export const navigation = [
    {
        'id': 'dashboards',
        'title': 'Dashboards',
        'translate': 'NAV.DASHBOARDS',
        'type': 'item',
        'icon': 'dashboard',
        'url': '/apps/dashboards/project'
    },
    {
        'id': 'placement',
        'title': 'Admin Activities',
        'translate': 'NAV.OFFER_PROVISIONING',
        'type': 'group',
        'icon': 'pages',
        'children': [
            {
                'id': 'faculty',
                'title': 'Faculty',
                'type': 'collapse',
                'icon': 'home_repair_service',
                'children': [
                    {
                        'id': 'list-faculty',
                        'title': 'List Faculty',
                        'type': 'item',
                        'url': '/pages/faculty/list'
                    },
                    {
                        'id': 'add-faculty',
                        'title': 'Add Faculty',
                        'type': 'item',
                        'url': '/pages/faculty/add'
                    },
                ]
            },
            {
                'id': 'classes',
                'title': 'Classes',
                'type': 'collapse',
                'icon': 'class',
                'children': [
                    {
                        'id': 'list-classes',
                        'title': 'List Classes',
                        'type': 'item',
                        'url': '/pages/classes/list'
                    },
                    {
                        'id': 'add-classes',
                        'title': 'Add Classes',
                        'type': 'item',
                        'url': '/pages/classes/add'
                    },
                ]
            },
            {
                'id': 'timetable',
                'title': 'Timetable',
                'type': 'collapse',
                'icon': 'more_time',
                'children': [
                    {
                        'id': 'list-timetable',
                        'title': 'Class Timetables',
                        'type': 'item',
                        'url': '/pages/timetable/list'
                    },
                    
                ]
            },
            {
                'id': 'survey',
                'title': 'Survey',
                'type': 'collapse',
                'icon': 'poll',
                'children': [
                    {
                        'id': 'list-survey',
                        'title': 'List Survey',
                        'type': 'item',
                        'url': '/pages/survey/list'
                    },
                    {
                        'id': 'add-survey',
                        'title': 'Add Survey',
                        'type': 'item',
                        'url': '/pages/survey/add'
                    },
                ]
            },
            {
                'id': 'key',
                'title': 'Progress Evaluation Keys',
                'type': 'collapse',
                'icon': 'vpn_key',
                'children': [
                    {
                        'id': 'list-keys',
                        'title': 'List Keys',
                        'type': 'item',
                        'url': '/pages/keys/list'
                    },
                    {
                        'id': 'add-keys',
                        'title': 'Add Keys',
                        'type': 'item',
                        'url': '/pages/keys/add'
                    },
                ]
            },
            {
                'id': 'events',
                'title': 'Events',
                'type': 'collapse',
                'icon': 'post_add',
                'children': [
                    {
                        'id': 'list-events',
                        'title': 'List Events',
                        'type': 'item',
                        'url': '/pages/events/list'
                    },
                    {
                        'id': 'add-events',
                        'title': 'Add Event',
                        'type': 'item',
                        'url': '/pages/events/add'
                    },
                ]
            },
            {
                'id': 'courses',
                'title': 'Courses',
                'type': 'collapse',
                'icon': 'menu',
                'children': [
                    {
                        'id': 'list-courses',
                        'title': 'List Courses',
                        'type': 'item',
                        'url': '/pages/courses/list'
                    },
                    {
                        'id': 'add-courses',
                        'title': 'Add Courses',
                        'type': 'item',
                        'url': '/pages/courses/add'
                    },
                ]
            },
            {
                'id': 'Fees',
                'title': 'Fees',
                'type': 'collapse',
                'icon': 'monetization_on',
                'children': [
                    {
                        'id': 'list-Fees',
                        'title': 'List Fees',
                        'type': 'item',
                        'url': '/pages/fees/list'
                    }
                ]
            },
            {
                'id': 'notice',
                'title': 'Admin Notice',
                'type': 'collapse',
                'icon': 'notification_important',
                'children': [
                    {
                        'id': 'list-notices',
                        'title': 'List Notices',
                        'type': 'item',
                        'url': '/pages/notice/list'
                    },
                    {
                        'id': 'add-notice',
                        'title': 'Add Notices',
                        'type': 'item',
                        'url': '/pages/notice/add'
                    },
                ]
            },
            {
                'id': 'student',
                'title': 'Student',
                'type': 'collapse',
                'icon': 'escalator_warning',
                'children': [
                    {
                        'id': 'list-student',
                        'title': 'List student',
                        'type': 'item',
                        'url': '/pages/student/list'
                    },
                    {
                        'id': 'add-student',
                        'title': 'Add student',
                        'type': 'item',
                        'url': '/pages/student/add'
                    },
                ]
            },
            {
                'id': 'diaries',
                'title': 'Diaries',
                'type': 'collapse',
                'icon': 'assignment',
                'children': [
                    {
                        'id': 'list-diaries',
                        'title': 'List Diaries',
                        'type': 'item',
                        'url': '/pages/lists/diaries'
                    }
                ]
            },
            {
                'id': 'attendance',
                'title': 'Attendance',
                'type': 'collapse',
                'icon': 'edit_attributes',
                'children': [
                    {
                        'id': 'list-attendance',
                        'title': 'List Attendance',
                        'type': 'item',
                        'url': '/pages/lists/attendances'
                    }
                ]
            },
            {
                'id': 'leaves',
                'title': 'Leaves',
                'type': 'collapse',
                'icon': 'sick',
                'children': [
                    {
                        'id': 'list-leaves',
                        'title': 'List Leaves',
                        'type': 'item',
                        'url': '/pages/lists/leaves'
                    }
                ]
            },
            {
                'id': 'ptm',
                'title': 'Parents Teacher Meetings ',
                'type': 'collapse',
                'icon': 'meeting_room',
                'children': [
                    {
                        'id': 'list-ptm',
                        'title': 'List PTM',
                        'type': 'item',
                        'url': '/pages/lists/ptm'
                    }
                ]
            },
            {
                'id': 'resources',
                'title': 'Resources',
                'type': 'collapse',
                'icon': 'insert_drive_file',
                'children': [
                    {
                        'id': 'list-resources',
                        'title': 'List Resources',
                        'type': 'item',
                        'url': '/pages/lists/resources'
                    }
                ]
            },
            {
                'id': 'results',
                'title': 'Results',
                'type': 'collapse',
                'icon': 'report',
                'children': [
                    {
                        'id': 'list-results',
                        'title': 'List Results',
                        'type': 'item',
                        'url': '/pages/lists/results'
                    }
                ]
            },

           
        ]
    },
    {
        'id': 'account',
        'title': 'Account',
        'translate': 'NAV.OFFER_PROVISIONING',
        'type': 'group',
        'icon': 'pages',
        'children': [
            // {
            //     'id'      : 'reset-password',
            //     'title'   : 'Reset Password',
            //     'type'    : 'item',
            //     'icon'    : 'lock',
            //     'url'  : '/pages/administration/reset-password'
            // },
            {
                'id'      : 'logout',
                'title'   : 'Logout',
                'type'    : 'item',
                'icon'    : 'lock',
                'click' : 'logout',
                'url'  : '/pages/account/logout'
            }
        ]
    }
];
