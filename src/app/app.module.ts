import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { TranslateModule } from '@ngx-translate/core';
import 'hammerjs';

import { FuseModule } from '@fuse/fuse.module';
import { FuseSharedModule } from '@fuse/shared.module';

import { fuseConfig } from './fuse-config';

import { AppComponent } from './app.component';
import { FuseFakeDbService } from './fuse-fake-db/fuse-fake-db.service';
import { FuseMainModule } from './main/main.module';
import { AppStoreModule } from './store/store.module';
import { AppService } from './app.service';
import { AuthService } from './auth/auth.service'
import { AuthGuard } from './auth/auth.guard'
import { JwtInterceptor } from './auth/jwt.interceptor'
import { fakeBackendProvider } from './auth/fake-backend'
import { FuseLoginComponent } from './auth/login/login.component'
import { MatButtonModule, MatSnackBarModule, MatCheckboxModule, MatFormFieldModule, MatInputModule,MatProgressBarModule } from '@angular/material';


const appRoutes: Routes = [
    {
        path        : 'apps',
        loadChildren: './main/content/apps/apps.module#FuseAppsModule',
    },
    {
        path        : 'pages',
        loadChildren: './main/content/pages/pages.module#FusePagesModule',
    },
    // {
    //     path        : 'ui',
    //     loadChildren: './main/content/ui/ui.module#FuseUIModule'
    // },
    // {
    //     path        : 'services',
    //     loadChildren: './main/content/services/services.module#FuseServicesModule'
    // },
    // {
    //     path        : 'components',
    //     loadChildren: './main/content/components/components.module#FuseComponentsModule'
    // },
    // {
    //     path        : 'components-third-party',
    //     loadChildren: './main/content/components-third-party/components-third-party.module#FuseComponentsThirdPartyModule'
    // },
    {
        path      : '**',
        redirectTo: 'apps/dashboards/project/'
    }
];

@NgModule({
    declarations: [
        AppComponent,
        FuseLoginComponent
    ],
    imports     : [
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        RouterModule.forRoot(appRoutes),

        TranslateModule.forRoot(),
        InMemoryWebApiModule.forRoot(FuseFakeDbService, {
            delay             : 0,
            passThruUnknownUrl: true
        }),

        // Fuse Main and Shared modules
        FuseModule.forRoot(fuseConfig),
        FuseSharedModule,
        AppStoreModule,
        FuseMainModule,

        MatButtonModule, 
        MatCheckboxModule, 
        MatFormFieldModule, 
        MatInputModule,
        MatProgressBarModule,
        MatSnackBarModule
    ],
    bootstrap   : [
        AppComponent
    ],
    providers   : [
        AppService,
        AuthGuard,
        AuthService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: JwtInterceptor,
            multi: true
        },
        fakeBackendProvider
    ]
})
export class AppModule
{
}
