import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { environment } from '../environments/environment';

@Injectable()
export class AppService implements Resolve<any>
{
    apiUrl: any = environment.apiUrl;
    cdnUrl: any = environment.cdnUrl;
    merchantService: any = this.apiUrl + 'merchantapi/';
    featuredService: any = this.apiUrl + 'couponapi/featured/details/';
    couponService: any = this.apiUrl + 'couponapi/';
    uploadService: any = this.apiUrl + 'uploadapi/';
    placementService: any = this.apiUrl + 'placementapi/';
    userService: any = this.apiUrl + 'userapi/';
    reportService: any = this.apiUrl + 'reportapi/';
    commissionService: any = this.apiUrl + 'commissionapi/';
    authService: any = this.apiUrl + 'api/v1/sms_users/sign_in.json';
    seasonalservice: any = this.apiUrl + 'couponapi/seasonal/';
    logService: any = this.apiUrl + 'logapi/';
    adminService: any = this.apiUrl + 'api/v1/admin/';
    uploadImageService: any = this.apiUrl + 'api/v1/';
    constructor(private http: HttpClient)
    {
    }

    /**
     * The File Manager App Main Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {
        debugger;
        route.params['id'] = 1;
        return new Promise((resolve, reject) => {
            resolve();
        });
    }
}
