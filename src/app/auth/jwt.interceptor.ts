import { Injectable, Component } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import { AuthService } from './auth.service';
import {
  MatSnackBar,
  MatSnackBarHorizontalPosition,
  MatSnackBarVerticalPosition,
} from '@angular/material/snack-bar';
export const InterceptorSkipHeader = 'X-Skip-Interceptor';
@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
    constructor(private authService: AuthService,private _snackBar: MatSnackBar){

    }
    openSnackBar() {
      this.authService.onshowLoadingBarChanged.next(false);
      this.authService.showLoadingBar = false;
      this._snackBar.open('Login Failed, Invalid Credentials', 'End now', {
        duration: 3000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
        
      });
      
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if(request.headers.has(InterceptorSkipHeader)){
          const headers = request.headers.delete(InterceptorSkipHeader);
          return next.handle(request.clone({ headers })).do((event: HttpEvent<any>) => {
            if (event instanceof HttpResponse) {
              console.log("IN RESPONSE EVENT");
            }
          }, (err: any) => {
            if (err instanceof HttpErrorResponse) {
              if (err.status === 401) {
                //alert("Login Failed! Invalid Credentials");
                this.authService.logout();
              }
            }
          });
        }
        else{
          // add authorization header with jwt token if available
          let currentUser = JSON.parse(localStorage.getItem('user'));
          console.log("Intercepter");
          console.log(currentUser);
          if (currentUser && currentUser.accessToken) {
            request = request.clone({
              setHeaders: {
                Authorization: `Bearer ${currentUser.accessToken}`
              }
            });
          }

          return next.handle(request).do((event: HttpEvent<any>) => {
            if (event instanceof HttpResponse) {
              console.log("IN RESPONSE EVENT");
            }
          }, (err: any) => {
            debugger;
            if (err instanceof HttpErrorResponse) {
              if (err.status === 401) {
                this.openSnackBar();
                this.authService.logout();
              }
            }
          });
        }

    }
}
