import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';
import { MatSnackBar } from '@angular/material';

import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../auth.service'


@Component({
    selector: 'fuse-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: fuseAnimations
})
export class FuseLoginComponent implements OnInit {
    loginForm: FormGroup;
    loginFormErrors: any;
    showLoadingBar: boolean = false;
    returnUrl: string;
    user: any = {}

    constructor(
        private fuseConfig: FuseConfigService,
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private authService: AuthService,
        private snackBar: MatSnackBar,
        
    ) {
        this.fuseConfig.setConfig({
            layout: {
                navigation: 'none',
                toolbar: 'none',
                footer: 'none'
            }
        });

        this.loginFormErrors = {
            email: {},
            password: {}
        };

        this.showLoadingBar = this.authService.showLoadingBar;
    }

    ngOnInit() {
        // reset login status
        debugger;
        this.authService.logout();
        
        // get return url from route parameters or default to '/'
        // this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
        this.loginForm = this.formBuilder.group({
            email: ['', [Validators.required, Validators.email]],
            password: ['', Validators.required]
        });

        this.loginForm.valueChanges.subscribe(() => {
            this.onLoginFormValuesChanged();
        });
    }

    onLoginFormValuesChanged() {
        for (const field in this.loginFormErrors) {
            if (!this.loginFormErrors.hasOwnProperty(field)) {
                continue;
            }

            // Clear previous errors
            this.loginFormErrors[field] = {};

            // Get the control
            const control = this.loginForm.get(field);

            if (control && control.dirty && !control.valid) {
                this.loginFormErrors[field] = control.errors;
            }
        }
    }

    login() {
        this.authService.onshowLoadingBarChanged.next(true);
        this.authService.showLoadingBar = true;
        this.showLoadingBar = true;
        let that = this;
        this.authService.login(this.user.email, this.user.password)
            .then((response) => {
                debugger;
                if (response == null) {
                  this.snackBar.open("Username and Password dose not match or user is inactive", "Done", {
                    duration: 2000,
                      
                });
                  
                }
                else {
                    debugger;
                  this.router.navigate(['/']);
                }
            })
    }


}
