export const AuthPermissions = [
    {
        "name": "dashboard",
        "url"  : "/apps/dashboards/analytics"
    }, 
    {
        "name": "dashboard_analytics",
        "url"  : "/apps/dashboards/analytics"
    }, 
    // merchants
    {
        "name": "merchant_list",
        "url"  : "/pages/merchant/list"
    }, 
    {
        "name": "merchant_add",
        "url": "/pages/merchant/add"
    }, 
    {
        "name": "merchant_edit",
        "url": "/pages/merchant/edit"
    }, 
    // monetized
    {
        "name": "merchant_monetized",
        "url": "/pages/merchant/monetized"
    }, 
    // merchant-category
    {
        "name": "merchant-category_list",
        "url": "/pages/merchant-category/list"
    }, 
    {
        "name": "merchant-category_add",
        "url": "/pages/merchant-category/add"
    }, 
    {
        "name": "merchant-category_edit",
        "url": "/pages/merchant-category/edit"
    }, 
    // coupon list
    {
        "name": "coupon_list",
        "url": "/pages/coupon/list"
    }, 
    {
        "name": "coupon_add",
        "url": "/pages/coupon/add"
    }, 
    {
        "name": "coupon_edit",
        "url": "/pages/coupon/edit"
    }, 
    // seasonal events
    {
        "name": "seasonal-events_list",
        "url": "/pages/seasonal-events/list"
    }, 
    {
        "name": "seasonal-events_add",
        "url": "/pages/seasonal-events/add"
    }, 
    {
        "name": "seasonal-events_edit",
        "url": "/pages/seasonal-events/edit"
    }, 
    // affiliate network
    {
        "name": "affiliate-network_list",
        "url": "/pages/affiliate-network/list"
    }, 
    {
        "name": "affiliate-network_add",
        "url": "/pages/affiliate-network/add"
    }, 
    {
        "name": "affiliate-network_edit",
        "url": "/pages/affiliate-network/edit"
    }, 
    // scheduled placements
    {
        "name": "scheduled-placements",
        // "url": "/pages/scheduled-placements"
        "url": ""
    },
    {
        "name": "scheduled-placements-pages",
        "url": "/pages/scheduled-placement-pages/list"
    },
    {
        "name": "scheduled-placements-seasonal",
        "url": "/pages/scheduled-placement-seasonal/list"
    },
    {
        "name": "scheduled-placements-upcoming-sales",
        "url": "/pages/scheduled-placement-upcoming-sales/list"
    },
    {
        "name": "scheduled-placements-home-event",
        "url": "/pages/scheduled-placement-home-event/list"
    },  
    {
        "name": "scheduled-placements-top-stores",
        "url": "/pages/scheduled-placement-top-stores/list"
    },  
    {
        "name": "scheduled-placements-popular-stores",
        "url": "/pages/scheduled-placement-popular-stores/list"
    },
    {
        "name": "scheduled-placements-search-merchant-links",
        "url": "/pages/scheduled-placement-search-merchant-links/list"
    },  
    // paid placements
    {
        "name": "paid-placements_list",
        "url": "/pages/paid-placements/list"
    }, 
    // carousel
    {
        "name": "carousel",
        "url": "/pages/carousel"
    }, 
    // featured merchants
    {
        "name": "featured-merchants",
        "url": "/pages/featured-merchants"
    }, 
    // featured coupon
    {
        "name": "featured-coupon",
        "url": "/pages/featured-coupon/list"
    }, 
    // administration
    {
        "name": "administration_list-user",
        "url": "/pages/administration/list-user"
    }, 
    {
        "name": "administration_add-user",
        "url": "/pages/administration/add-user"
    }, 
    {
        "name": "administration_edit-user",
        "url": "/pages/administration/edit-user"
    }, 
    {
        "name": "administration_list-role",
        "url": "/pages/administration/list-role"
    }, 
    {
        "name": "administration_add-role",
        "url": "/pages/administration/add-role"
    },
    {
        "name": "administration_edit-role",
        "url": "/pages/administration/edit-role"
    },
    // commission and cashbacks
    {
        "name": "commission_list",
        "url": "/pages/commission/list"
    }, 
    {
        "name": "customer-cashbacks_list",
        "url": "/pages/customer-cashbacks/list"
    }, 
    {
        "name": "payout-requests_list",
        "url": "/pages/payout-requests/list"
    },
    {
        "name": "sync-commission",
        "url": "/pages/sync-commission/"
    },
    {
        "name": "reset-password",
        "url": "/pages/administration/reset-password"
    },
    {
        "name": "list-user-activity",
        "url": "/pages/administration/list-user-activity"
    },
    {
        "name": "list-user-activity",
        "url": "/pages/administration/list-user-activity"
    },
    {
        "name": "logout",
        "url": "/pages/account/logout"
    },
    {
        "name": "merchant-review-list",
        "url": "/pages/merchant-review/list"
    },
    {
        "name": "merchant-review-assign-list",
        "url": "/pages/merchant-review/assign-list"
    },
    {
        "name": "search-merchant-coupons",
        "url": "/pages/merchant-coupons/list"
    }
]
