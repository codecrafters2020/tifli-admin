import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { AppService } from '../app.service';

import { AuthGroup } from './models/auth.types';
import { AuthPermissions } from './models/auth.permissions'
import * as _ from 'lodash'


@Injectable()
export class AuthService {

    user: any = {
        id: 0,
        email: '',
        password: '',
        roles: [],
        accessToken: null
    };
    token: any = '';
    urls: Array<string>; // Store the actions for which this user has permission
    permissions: Array<string>;
    showLoadingBar: boolean = false;
    onshowLoadingBarChanged: BehaviorSubject<any> = new BehaviorSubject<any>({});
    constructor(private http: HttpClient, private appService: AppService, private router: Router) {
        this.permissions = this.getPermissions();
    }

    hasPermission(url: string): boolean {
        let exists = _.find(this.permissions, o => {
            return _.includes(url, o);
        });
        return exists ? true : false;
    }

    login(email: string, password: string): Promise<any> {

      let accountInfo = {};
      let userInfo={};
      accountInfo['sms_user'] = {email: email, password: password}

        // var loginPayLoad = {
        //     username: email,
        //     password: password
        // }
        return new Promise((resolve, reject) => {
            this.http.post(this.appService.authService , accountInfo)
                .subscribe((response: any) => {
                    debugger;
                    if (response == null && response.sms_user) {
                      console.log("Wrong credentials");
                      // response.token = response.sms_user.auth_token;
                    }
                    else {
                        response.token = response.sms_user.auth_token;
                        // response.token = "temp";

                        if (response.token && (response.id != null || response.id != 0)) {
                          
                          // response.roles[0].permissions.push(logoutPermission);
                          // response.roles[0].permissions.push(dashboardPermission);
                          // response.roles[0].permissions.push(resetPasswordPermission);
                          // response.roles[0].permissions.push(listUserActivityPermission);
                          // response.roles[0].permissions.push(searchMerchantCoupons);
                          // response.roles[0].permissions.push(merchantReviewList);
                          // response.roles[0].permissions.push(merchantReviewAssignList);
                          // response.roles[0].permissions.push(featuredCouponPermission);
                          // response.roles[0].permissions.push(scheduledPlacementPages);
                          // response.roles[0].permissions.push(scheduledPlacementSeasonal);
                          // response.roles[0].permissions.push(scheduledPlacementUpcomingSales);
                          // response.roles[0].permissions.push(scheduledPlacementHomeEvent);
                          // response.roles[0].permissions.push(scheduledPlacementTopStores);
                          // response.roles[0].permissions.push(scheduledPlacementPopularStores);
                          // response.roles[0].permissions.push(scheduledPlacementSearchMerchants);
                          // response.roles[0].permissions.push(syncCommissionPermission);
                                    debugger;
                        //   this.permissions = this.getUrls(response.permissions);
                          this.user.id = response.sms_user;
                          this.user.email = response.sms_user.email;
                          // this.user.password = loginPayLoad.password;
                        //   this.user.roles = response.roles;
                          this.user.accessToken = response.token;

                            this.token = response.token;
                            localStorage.setItem('user', JSON.stringify(this.user));
                            localStorage.setItem('token', JSON.stringify(this.token));
                            // localStorage.setItem('permissions', JSON.stringify(this.permissions));
                            // localStorage.setItem('user', JSON.stringify(response.user));
                            // localStorage.setItem('token', JSON.stringify(response.user.token));
                            // localStorage.setItem('permissions', JSON.stringify(this.permissions));
                        }
                        resolve(response);
                    }
                }, error => {

                })
        })
    }

    getUrls(permissions) {
        let pp = [];
        let rolePermissions = [];
        let permurls = [];
        // roles ka union
        // _.map(roles, (role) => {
        //     pp = _.concat(role.permissions, pp)
        // });
      debugger;
      pp = _.concat(permissions, pp);
        rolePermissions = _.uniq(pp);

        // getting url of permission name from the global array
        _.map(rolePermissions, (permission) => {
            let perm = _.find(AuthPermissions, { 'name': permission });
            permurls.push(perm['url']);
        });

        return permurls;
    }

    logout() {
        // remove user from local storage to log user out
        debugger;
        localStorage.removeItem('user');
        localStorage.removeItem('token');
        // localStorage.removeItem('permissions');
    }

    getUser() {
        return JSON.parse(localStorage.getItem('user'));
    }

    getToken() {
        return JSON.parse(localStorage.getItem('token'));
    }

    getPermissions() {
        return JSON.parse(localStorage.getItem('permissions'));
        // return this.permissions
    }

    isLoggedIn() {
        return this.getToken() ? true : false;
    }
}
