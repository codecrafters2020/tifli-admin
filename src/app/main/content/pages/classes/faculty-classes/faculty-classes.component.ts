import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';

import { FacultyClassesService } from './faculty-classes.service';

import { Router } from '@angular/router';

@Component({
    selector     : 'fuse-file-manager',
    templateUrl  : './faculty-classes.component.html',
    styleUrls    : ['./faculty-classes.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class FacultyClassesComponent implements OnInit
{
    selected: any;
    pathArr: string[];

    constructor(private FacultyClassesService: FacultyClassesService, private router: Router)
    {
    }

    ngOnInit()
    {
        
    }

    openMerchantCategoryList(){
        
        this.router.navigateByUrl("pages/classes/list");
    }
}
