import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { AppService } from '../../../../../app.service';
import { FacultyModule } from '../../faculty/faculty.module';

@Injectable()
export class FacultyClassesService implements Resolve<any>
{
    classes:any;
    TempClasses:any;
    id:any;
    faculty_id:any;
    section_name:any;
    section_id:any;
    faculties:any[];
    courses: any[];
    obj = {
        campus_id: '',
        created_at:'',
        deleted_at:'',
        id : ''

    }
   
    payload = {
        class_name: '',
        id : '',
        sections: [{section_name : '',faculty_id: '', id: '' }]

    }
   
    onUsersChanged: BehaviorSubject<any> = new BehaviorSubject({});
    onUserSelected: BehaviorSubject<any> = new BehaviorSubject({});
    
    constructor(private http: HttpClient, private appService: AppService)
    {
    }

    /**
     * The File Manager App Main Resolver
     * 
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {
        this.id=route.params['id'];

         return new Promise((resolve, reject) => {
             debugger;
             Promise.all([
              this.getClasses(),
              this.getFaculty(),
             ]).then(
                 ([users]) => {
                     resolve();
                 },
                 reject);
         });
    }

    getClasses(): Promise<any>
    {
        let queryString = 'id=' + this.id ;

        return new Promise((resolve, reject) => {
            debugger;
            // this.http.get(this.appService.apiUrl + 'cms/api/merchants/categories/'+this.id)
            this.http.get(`${this.appService.adminService}section_courses/get_section_courses?${queryString}`) 
                .subscribe((response: any) => {
                    console.log(response);
                    debugger;
                    if(response)
                    {
                       
                        this.TempClasses = response;
                            let that = this;
                           
                            this.TempClasses.section_courses.forEach(function(value){
                            debugger;
                            if(value.course_teacher == null)
                            {
                                
                                debugger;
                                value.course_teacher={
                                    campus_id: '',
                                    created_at:'',
                                    deleted_at:'',
                                    id : ''
                            
                                } ;
                            }
                               
                           
                         } );
                        debugger;
                        this.classes = this.TempClasses;
                    }
                    resolve(response);
                }, reject);
        }).catch(err => this.handleError(err, () => this.http.get(`${this.appService.adminService}section_courses/get_section_courses?${queryString}`)));
    }
    protected handleError(error, continuation: () => Observable<any> ) {
        if (error.status == 404 || error.status == 400) {
            this.classes = [];
        }
     
    }


    updateClasses(classes): Promise<any>
    {
        // debugger;
        return new Promise((resolve, reject) => {
            debugger;
           // this.http.put(this.appService.merchantService + 'categories/'+classes.id, classes)
           this.http.patch(`${this.appService.adminService}section_courses`, classes, {observe : "response"})
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }
    getFaculty(): Promise<any> {
        return new Promise((resolve, reject) => {
            // this.http.get(this.appService.apiUrl + 'cms/api/users/')
            // this.http.get('http://104.42.179.33:8765/cms/api/coupons/coupons?page=0&size=20&sort=couponCode,asc&searchTerms=gag&couponCode=123&couponType=Deal')
            this.http.get(`${this.appService.adminService}faculties/show_all_faculties`)
                .subscribe((response: any) => {
                    debugger;
                    this.faculties = response.sms_users;
                    resolve(response);
                }, reject);
        });
    }
   

}