import { Component, OnInit,ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, FormArray, Validators, EmailValidator } from '@angular/forms';
import { FacultyClassesService } from '../faculty-classes.service';
import { Router } from '@angular/router';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { startWith } from 'rxjs/operators/startWith';
import { map } from 'rxjs/operators/map';
import { MatSnackBar } from '@angular/material';

import {
    MatAutocomplete,
    MatAutocompleteSelectedEvent,
    MatAutocompleteTrigger,
    VERSION,
  } from '@angular/material';
import { Subscription } from 'rxjs/Subscription';

@Component({
    selector   : 'fuse-file-list',
    templateUrl: './form-faculty-classes.component.html',
    styleUrls  : ['./form-faculty-classes.component.scss'],
})
export class FormFacultyClassesComponent implements OnInit
{
    classes: any;
    form: FormGroup;
    formErrors: any;
    test: any;
    teachers : any[];
    visible: boolean = true;
    selectable: boolean = true;
    removable: boolean = true;
    addOnBlur: boolean = true;
    removedSections: any[];
    sections: any=[];
    button_disabled :boolean = false;
   // sections: [{id:'',section_name : '',faculty_id: '' }];
   //sections: any[]; 
   //sections: any[];
   /*/
    classes = {
        class_name: '',
        

    }
    */
     course_teacher = {
        id: '3'
    }
   
    constructor(private formBuilder: FormBuilder, private FacultyClassesService: FacultyClassesService, private router: Router, private snackBar: MatSnackBar)
    {
        debugger
        
        //var thisOne = this;
        //this.classes.class_name = EditClassesService.classes.school_name.class_name;
       // this.classes.sections.section_name = EditClassesService.classes.school_name.class_name;
        this.teachers=this.FacultyClassesService.faculties;
        this.classes = FacultyClassesService.classes;
        debugger;
        if(this.classes == ""){
            this.snackBar.open("Selected Class has no courses", "Add course first", {
                duration: 2000,
            });
            
                this.redirect('pages/classes/list');
               
        }
        debugger;
        let that = this;
        
        
        
        this.formErrors = {
            className: {},
            sectionName: {},
           // ctName: {},
            
        };
        
    }

    ngOnInit()
    {
        // Reactive Form
        this.form = this.formBuilder.group({
            className: ['', Validators.required],
            sectionName: ['', Validators.required],
           // classTeacher: ['', Validators.required],
            sections: new FormArray([]),
           
        });

        this.form.valueChanges.subscribe(() => {
            this.onFormValuesChanged();
        });
        debugger;
        if(this.classes.section_courses != null){
            for(let i=0;i<this.classes.section_courses.length;i++){
                const control = <FormArray>this.form.controls['sections'];
                debugger;
                control.push(this.loadSections(this.classes.section_courses[i]));
            }
        }
        
        

    }
     loadSections(classes: any) {
        debugger;
        return this.formBuilder.group({
           course_name : [classes.course, Validators.required],
           faculty : [classes.course_teacher]

          //  className: ['', Validators.required]

        });
    }
     
   SearchData(input: any){
        // debugger;
    }

    onFormValuesChanged()
    {
        for ( const field in this.formErrors )
        {
            if ( !this.formErrors.hasOwnProperty(field) )
            {
                continue;
            }

            // Clear previous errors
            this.formErrors[field] = {};

            // Get the control
            const control = this.form.get(field);

            if ( control && control.dirty && !control.valid )
            {
                this.formErrors[field] = control.errors;
            }
        }
    }
    onSubmit() {
        debugger;
        this.button_disabled = true;
        let formValues = this.form.value;
        let that = this;
       // that.sections.push("abc");
       
        if (that.form.valid) {
            
            that.classes.section_courses.forEach(function(value) {
                debugger;
                if(value.course_teacher.id != null)
                {        let obj = {
                        'id' : value.id,
                        'faculty_id' : value.course_teacher.id
                       }
                       that.sections.push(obj);
                      // this.pushfunction(obj);

                 }
                 else{
                    let obj = {
                        'id' : value.id
                        }
                       that.sections.push(obj);
                   }
                    
            });
           let object = {
                 'id': this.classes.section.id,
                'section_courses': this.sections
                   
             }
             
           // let payload = this.classes;
            this.FacultyClassesService.updateClasses(object).then(response => {
                debugger;
                console.log("response: " + JSON.stringify(response));
            debugger;
            if(response.status !== "200" && !"204" ){
                this.snackBar.open("Something went wrong", "Error", {
                    duration: 2000,
                });
            }
            this.snackBar.open("Sucess", "Done", {
                duration: 2000,
            });
            
                this.redirect('pages/classes/list');
               
            }); 
        }
        
    }

    redirect(pagename: string) {
        this.router.navigate([pagename]);
    }

    log(val: any) { console.log(val); }
}
