import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';

import { AddClassesService } from './add-classes.service';

import { Router } from '@angular/router';

@Component({
    selector     : 'fuse-file-manager',
    templateUrl  : './add-classes.component.html',
    styleUrls    : ['./add-classes.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class AddClassesComponent implements OnInit
{
    selected: any;
    pathArr: string[];

    constructor(private AddClassesService: AddClassesService, private router: Router)
    {
    }

    ngOnInit()
    {
        
    }

    openClassesList(){
        
        this.router.navigateByUrl("pages/classes/list");
    }
}
