import { Component, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, FormArray, Validators, EmailValidator } from '@angular/forms';
import { AddClassesService } from '../add-classes.service';
import { Router } from '@angular/router';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { startWith } from 'rxjs/operators/startWith';
import { map } from 'rxjs/operators/map';
import { MatSnackBar } from '@angular/material';

import {
    MatAutocomplete,
    MatAutocompleteSelectedEvent,
    MatAutocompleteTrigger,
    VERSION,
} from '@angular/material';
import { Subscription } from 'rxjs/Subscription';

@Component({
    selector: 'fuse-file-list',
    templateUrl: './form-add-classes.component.html',
    styleUrls: ['./form-add-classes.component.scss'],
})
export class FormAddClassesComponent implements OnInit {
   // merchantCategoryStatus: boolean = false;
    form: FormGroup;
    formErrors: any;
    test: any;
    teachers: any[];
    courses:any[];
    visible: boolean = true;
    selectable: boolean = true;
    removable: boolean = true;
    addOnBlur: boolean = true;
    button_disabled: boolean = false;
    //classTeacher = getFaculty();
    classes = {
        class_name: '',
        sections: [{section_name : '',faculty_id: '' }],
        courses: [{id : ''}]

    }
    constructor(private formBuilder: FormBuilder, private AddClassesService: AddClassesService, private router: Router, private snackBar: MatSnackBar) {
        // Reactive form errors
        this.formErrors = {
            className: {},
            ctName: {},
            
        };
        debugger;
        this.teachers=this.AddClassesService.faculties;
        this.courses=this.AddClassesService.courses;

    }

    ngOnInit() {
        // Reactive Form
        this.form = this.formBuilder.group({
            class_name: ['', Validators.required],
           // classTeacher: ['', Validators.required],
            sections: new FormArray([]),
            courses: new FormArray([])

        
        });
        // not edit for courses

        const control = <FormArray>this.form.controls['sections']
        for (let i = 0; i < control.length; i++) {
            control[i]
                .subscribe(value => {
                    debugger;
                    let passwordConfirmValue = this.form.controls.passwordConfirm.value;
                    const control = this.form.controls.passwordConfirm;
                    if (value != passwordConfirmValue)
                        this.formErrors.passwordConfirm.passwordsNotMatch = true;
                    else {
                        this.form.controls['passwordConfirm'].setValue(value);
                    }
                });
        }

        this.form.valueChanges.subscribe(() => {

            this.onFormValuesChanged();
        });

        const courses_control = <FormArray>this.form.controls['courses']
        for (let i = 0; i < courses_control.length; i++) {
            courses_control[i]
                .subscribe(value => {
                    debugger;
                    let passwordConfirmValue = this.form.controls.passwordConfirm.value;
                    const control = this.form.controls.passwordConfirm;
                    if (value != passwordConfirmValue)
                        this.formErrors.passwordConfirm.passwordsNotMatch = true;
                    else {
                        this.form.controls['passwordConfirm'].setValue(value);
                    }
                });
        }

        this.form.valueChanges.subscribe(() => {

            this.onFormValuesChanged();
        });

    }
    get DynamicFormControls() {

        return <FormArray>this.form.get('courses');
      }
    get DynamicFormControlsSection() {

        return <FormArray>this.form.get('sections');
      }

    initCourse() {
        return this.formBuilder.group({
            course_id: ['', Validators.required]
          //  faculty_id: ['', Validators.required]
            
        });
    }

    addNewCourse() {
        const course_control = <FormArray>this.form.controls['courses'];
        debugger;
        console.log("qari")
        course_control.push(this.initCourse());

    }

    deleteCourse(index: number) {
        debugger;
        const course_control = <FormArray>this.form.controls['courses'];
        course_control.removeAt(index);
    }



    SearchData2(input: any) {
        // debugger;
    }

    initSection() {
        return this.formBuilder.group({
            section_name: ['', Validators.required],
            faculty_id: ['', Validators.required]
            
        });
    }

    addNewSection() {
        const control = <FormArray>this.form.controls['sections'];
        debugger;
        console.log("qari")
        control.push(this.initSection());

    }

    deleteSection(index: number) {
        debugger;
        const control = <FormArray>this.form.controls['sections'];
        control.removeAt(index);
    }



    SearchData(input: any) {
        // debugger;
    }

    onFormValuesChanged() {
        for (const field in this.formErrors) {
            if (!this.formErrors.hasOwnProperty(field)) {
                continue;
            }

            // Clear previous errors
            this.formErrors[field] = {};

            // Get the control
            const control = this.form.get(field);

            if (control && control.dirty && !control.valid) {
                this.formErrors[field] = control.errors;
            }
        }
    }

    
    
    onSubmit() {
        // debugger;
        this.button_disabled = true;
        let formValues = this.form.value;
        if (this.form.valid) {
            this.classes.class_name = formValues.class_name;
            this.classes.sections = formValues.sections;
            this.classes.courses = formValues.courses;
            let payload = {
                'class': this.classes
             
                }
            
            this.AddClassesService.addClasses(payload).then(response => {
                // debugger;
                console.log("response: " + JSON.stringify(response));
                debugger;
                if(response.status !== "200"){
                    this.snackBar.open("Something went wrong", "Error", {
                        duration: 2000,
                    });
                }
                this.snackBar.open("Sucess", "Done", {
                    duration: 2000,
                });
                
                    this.redirect('pages/classes/list');
            });
        }

    }
    redirect(pagename: string) {
        this.router.navigate([pagename]);
    }

    log(val: any) { console.log(val); }
}
