import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { of } from 'rxjs/observable/of';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { AppService } from '../../../../../app.service';

@Injectable()
export class AddClassesService implements Resolve<any>
{
    roles: any[];
    faculties: any[];
    courses: any[];
    constructor(private http: HttpClient, private appService: AppService)
    {
    }

    /**
     * The File Manager App Main Resolver
     * 
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {

           // return new Promise((resolve, reject) => {
        //     // debugger;
        //     resolve();
        // });
        return new Promise((resolve, reject) => {

            Promise.all([
                this.getFaculty(),
                this.getCourses()
            ]).then(
                ([faculty]) => {
                    resolve(Response);
                },
                reject
                );
        });
    }

    addClasses(classes): Promise<any>
    {
        // debugger;
        return new Promise((resolve, reject) => {
            this.http.post(`${this.appService.adminService}class_sections`, classes)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    //error handling 

      
      protected handleError(error, continuation: () => Observable<any>) {
          let that = this;
          //alert("Selected Class and Month Pair has no Attendance Record");
          if (error.status == 404 || error.status == 400) {
               this.faculties = [];
               this.courses = [];
            
            return of(false);
          }
          else if (error.status == 422){
            //this.is422 = true;
            debugger;
            return of(false);
          }
       
      }



    getFaculty(): Promise<any> {
        return new Promise((resolve, reject) => {
            // this.http.get(this.appService.apiUrl + 'cms/api/users/')
            // this.http.get('http://104.42.179.33:8765/cms/api/coupons/coupons?page=0&size=20&sort=couponCode,asc&searchTerms=gag&couponCode=123&couponType=Deal')
            this.http.get(`${this.appService.adminService}faculties/show_all_faculties`)
                .subscribe((response: any) => {
                    debugger;
                    this.faculties = response.sms_users;
                    resolve(response);
                }, reject);
        }).catch(err => this.handleError(err, () =>  this.http.get(`${this.appService.adminService}faculties/show_all_faculties`)

            ));
        
    }
    getCourses(): Promise<any> {
        return new Promise((resolve, reject) => {
            // this.http.get(this.appService.apiUrl + 'cms/api/users/')
            // this.http.get('http://104.42.179.33:8765/cms/api/coupons/coupons?page=0&size=20&sort=couponCode,asc&searchTerms=gag&couponCode=123&couponType=Deal')
            this.http.get(`${this.appService.adminService}courses/show_all_courses`)
                .subscribe((response: any) => {
                    debugger;
                    //this.coupons = response;
                    this.courses = response;
                    resolve(response);
                }, reject);
        }).catch(err => this.handleError(err, () =>  this.http.get(`${this.appService.adminService}courses/show_all_courses`)));;
    }



}