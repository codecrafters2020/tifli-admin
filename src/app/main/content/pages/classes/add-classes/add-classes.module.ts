import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CdkTableModule } from '@angular/cdk/table';

import { MatFormFieldModule, MatInputModule, MatSelectModule, MatStepperModule, MatButtonModule, MatIconModule, MatRippleModule, MatSidenavModule, MatSlideToggleModule, MatTableModule } from '@angular/material';
import {MatChipsModule} from '@angular/material/chips';
import {MatAutocompleteModule} from '@angular/material/autocomplete';

import { FuseSharedModule } from '@fuse/shared.module';

import { AddClassesComponent } from './add-classes.component';
import { AddClassesService } from './add-classes.service';
import { FormAddClassesComponent } from './form/form-add-classes.component';

const routes: Routes = [
    {
        path     : '**',
        component: AddClassesComponent,
        children : [],
        resolve  : {
            files: AddClassesService
        }
    }
];

@NgModule({
    declarations: [
        AddClassesComponent,
        FormAddClassesComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        CdkTableModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatButtonModule,
        MatIconModule,
        MatRippleModule,
        MatSlideToggleModule,
        MatSidenavModule,
        MatTableModule,
        MatChipsModule,
        MatAutocompleteModule,

        FuseSharedModule
    ],
    providers   : [
        AddClassesService
    ]
})
export class AddClassesModule
{
}
