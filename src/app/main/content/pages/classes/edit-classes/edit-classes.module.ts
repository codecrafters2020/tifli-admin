import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CdkTableModule } from '@angular/cdk/table';

import { MatFormFieldModule, MatInputModule, MatSelectModule, MatStepperModule, MatButtonModule, MatIconModule, MatRippleModule, MatSidenavModule, MatSlideToggleModule, MatTableModule } from '@angular/material';
import {MatChipsModule} from '@angular/material/chips';
import {MatAutocompleteModule} from '@angular/material/autocomplete';

import { FuseSharedModule } from '@fuse/shared.module';

import { EditClassesComponent } from './edit-classes.component';
import { EditClassesService } from './edit-classes.service';
import { FormEditClassesComponent } from './form/form-edit-classes.component';

const routes: Routes = [
    {
        path     : '**',
        component: EditClassesComponent,
        children : [],
        resolve  : {
            files: EditClassesService
        }
    }
];

@NgModule({
    declarations: [
        EditClassesComponent,
        FormEditClassesComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        CdkTableModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatButtonModule,
        MatIconModule,
        MatRippleModule,
        MatSlideToggleModule,
        MatSidenavModule,
        MatTableModule,
        MatChipsModule,
        MatAutocompleteModule,

        FuseSharedModule
    ],
    providers   : [
        EditClassesService
    ]
})
export class EditClassesModule
{
}
