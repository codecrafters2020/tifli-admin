import { Component, OnInit,ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, FormArray, Validators, EmailValidator } from '@angular/forms';
import { EditClassesService } from '../edit-classes.service';
import { Router } from '@angular/router';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { startWith } from 'rxjs/operators/startWith';
import { map } from 'rxjs/operators/map';
import { MatSnackBar } from '@angular/material';

import {
    MatAutocomplete,
    MatAutocompleteSelectedEvent,
    MatAutocompleteTrigger,
    VERSION,
  } from '@angular/material';
import { Subscription } from 'rxjs/Subscription';

@Component({
    selector   : 'fuse-file-list',
    templateUrl: './form-edit-classes.component.html',
    styleUrls  : ['./form-edit-classes.component.scss'],
})
export class FormEditClassesComponent implements OnInit
{
    classes: any;
    form: FormGroup;
    formErrors: any;
    test: any;
    teachers : any[];
    courses: any[];
    visible: boolean = true;
    selectable: boolean = true;
    removable: boolean = true;
    addOnBlur: boolean = true;
    removedSections: any[];
    removedCourses: any[]; 
    sections: any=[];
    coursesObj: any=[];
    button_disabled: boolean = false;
   // sections: [{id:'',section_name : '',faculty_id: '' }];
   //sections: any[]; 
   //sections: any[];
   /*/
    classes = {
        class_name: '',
        

    }
    */
     obj = {
    faculty_name: '',
    faculty_id: ''
    }
   
    constructor(private formBuilder: FormBuilder, private EditClassesService: EditClassesService, private router: Router, private snackBar: MatSnackBar)
    {
        debugger
        
        //var thisOne = this;
        //this.classes.class_name = EditClassesService.classes.school_name.class_name;
       // this.classes.sections.section_name = EditClassesService.classes.school_name.class_name;
        this.teachers=this.EditClassesService.faculties;
        this.classes = EditClassesService.classes;
        this.courses=this.EditClassesService.courses;
    
        debugger;
        let that = this;
        this.classes.class_sections.forEach(function(value){
            debugger;
            if(value.faculty == null)
            {
                
                value.faculty=that.obj;
            }
               // this.payload.sections.faculty_id.push(this.fac);
           
 } );
        
        this.formErrors = {
            className: {},
           // ctName: {},
            
        };
        
    }

    ngOnInit()
    {
        // Reactive Form
        this.form = this.formBuilder.group({
            className: ['', Validators.required],
           // classTeacher: ['', Validators.required],
            sections: new FormArray([]),
            coursesForm: new FormArray([])
        
        });

        this.form.valueChanges.subscribe(() => {
            this.onFormValuesChanged();
        });
        debugger;
        if(this.classes.class_sections != null){
            for(let i=0;i<this.classes.class_sections.length;i++){
                const control = <FormArray>this.form.controls['sections'];
                debugger;
                control.push(this.loadSections(this.classes.class_sections[i]));
            }
        }
        if(this.classes.courses != null){
            for(let i=0;i<this.classes.courses.length;i++){
                const control_courses = <FormArray>this.form.controls['coursesForm'];
                debugger;
                control_courses.push(this.loadCourses(this.classes.courses[i]));
            }
        }
        
        

    }

    loadCourses(classes: any) {
        debugger;
        return this.formBuilder.group({
           course_name : [classes.course, Validators.required]
          // faculty : [classes.faculty, Validators.required]

          //  className: ['', Validators.required]

        });
    }

    initCourses() {
        return this.formBuilder.group({
            course_name: ['', Validators.required],
           // faculty: ['', Validators.required]


        });
    }

    addNewCourses() {
        debugger;
        let newCourse = {
           // faculty: [{id: '' }],
            course: [{course_name : '', id:''}]


        }
        //this.classes.class_sections[1];
        //newSections.section.section_name = '';
       // newSections.faculty.id= '';
       // {section_name : '',faculty_id: '' };
        this.classes.courses.push(newCourse);
        const control = <FormArray>this.form.controls['coursesForm'];
        control.push(this.initCourses());
    }

    deleteCourses(index: number) {
        debugger;
        const control = <FormArray>this.form.controls['coursesForm'];
        control.removeAt(index);
        //this.removedCourses.push(this.classes.courses[index]);
        this.classes.courses.splice(index,1);
    }

    loadSections(classes: any) {
        debugger;
        return this.formBuilder.group({
           section_name : [classes.section, Validators.required],
           faculty : [classes.faculty, Validators.required]

          //  className: ['', Validators.required]

        });
    }

    initSections() {
        return this.formBuilder.group({
            section_name: ['', Validators.required],
            faculty: ['', Validators.required]


        });
    }

    addNewSections() {
        debugger;
        let newSections = {
            faculty: [{id: '' }],
            section: [{section_name : '',section_id: '' }]


        }
        //this.classes.class_sections[1];
        //newSections.section.section_name = '';
       // newSections.faculty.id= '';
       // {section_name : '',faculty_id: '' };
        this.classes.class_sections.push(newSections);
        const control = <FormArray>this.form.controls['sections'];
        control.push(this.initSections());
    }

    deleteSections(index: number) {
        debugger;
        const control = <FormArray>this.form.controls['sections'];
        control.removeAt(index);
        // this.removedSections.push(this.classes.class_sections[index]);
         this.classes.class_sections.splice(index,1);
    }
    


    SearchData(input: any){
        // debugger;
    }

    onFormValuesChanged()
    {
        for ( const field in this.formErrors )
        {
            if ( !this.formErrors.hasOwnProperty(field) )
            {
                continue;
            }

            // Clear previous errors
            this.formErrors[field] = {};

            // Get the control
            const control = this.form.get(field);

            if ( control && control.dirty && !control.valid )
            {
                this.formErrors[field] = control.errors;
            }
        }
    }
   

    onSubmit() {
        debugger;
        this.button_disabled = true;
        let formValues = this.form.value;
        let that = this;
       // that.sections.push("abc");
        if (that.form.valid) {
            
            that.classes.class_sections.forEach(function(value) {
                debugger;
                if(value.section.id != null)
                {
                   

                    let obj = {
                        'id' : value.section.id,
                        'section_name' : value.section.section_name,
                        'faculty_id' : value.faculty.id
                       }
                       that.sections.push(obj);
                      // this.pushfunction(obj);

                 }
                 else{
                    let obj = {
                        
                        //  'id' : value.section.id,
                          'section_name' : value.section.section_name,
                          'faculty_id' : value.faculty.id
                         }
                         
                          that.sections.push(obj);
                   }
                    
            });
            that.classes.courses.forEach(function(value) {
                
                    let obj = {
                        'course_id' : value.course.id,
                        }
                       that.coursesObj.push(obj);
                                    
            });
            debugger;
             let object = {
                 'class_name': this.classes.school_class.class_name,
                 'class_id': this.classes.school_class.id,
                'sections': this.sections,
                    'courses':this.coursesObj
             }
            let payload = this.classes;
            this.EditClassesService.updateClasses(object).then(response => {
                debugger;
                console.log("response: " + JSON.stringify(response));
            debugger;
            if(response.status !== "200"){
                this.snackBar.open("Something went wrong", "Error", {
                    duration: 2000,
                });
            }
            this.snackBar.open("Sucess", "Done", {
                duration: 2000,
            });
            
                this.redirect('pages/classes/list');
               
            }); 
        }
        
    }

    redirect(pagename: string) {
        this.router.navigate([pagename]);
    }

    log(val: any) { console.log(val); }
}
