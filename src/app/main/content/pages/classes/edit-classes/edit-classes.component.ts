import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';

import { EditClassesService } from './edit-classes.service';

import { Router } from '@angular/router';

@Component({
    selector     : 'fuse-file-manager',
    templateUrl  : './edit-classes.component.html',
    styleUrls    : ['./edit-classes.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class EditClassesComponent implements OnInit
{
    selected: any;
    pathArr: string[];

    constructor(private EditClassesService: EditClassesService, private router: Router)
    {
    }

    ngOnInit()
    {
        
    }

    openMerchantCategoryList(){
        
        this.router.navigateByUrl("pages/classes/list");
    }
}
