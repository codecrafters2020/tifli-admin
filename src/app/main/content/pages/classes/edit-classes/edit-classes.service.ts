import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { AppService } from '../../../../../app.service';
import { FacultyModule } from '../../faculty/faculty.module';

@Injectable()
export class EditClassesService implements Resolve<any>
{
    classes:any;
    id:any;
    faculty_id:any;
    section_name:any;
    section_id:any;
    faculties:any[];
    courses: any[];
    payload = {
        class_name: '',
        id : '',
        sections: [{section_name : '',faculty_id: '', id: '' }]

    }
   
    onUsersChanged: BehaviorSubject<any> = new BehaviorSubject({});
    onUserSelected: BehaviorSubject<any> = new BehaviorSubject({});
    
    constructor(private http: HttpClient, private appService: AppService)
    {
    }

    /**
     * The File Manager App Main Resolver
     * 
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {
        this.id=route.params['id'];

         return new Promise((resolve, reject) => {
             debugger;
             Promise.all([
              this.getClasses(),
              this.getFaculty(),
              this.getCourses()
             ]).then(
                 ([users]) => {
                     resolve();
                 },
                 reject);
         });
    }

    getClasses(): Promise<any>
    {
        let queryString = 'id=' + this.id ;

        return new Promise((resolve, reject) => {
            debugger;
            // this.http.get(this.appService.apiUrl + 'cms/api/merchants/categories/'+this.id)
            this.http.get(`${this.appService.adminService}class_sections/get_class_section?${queryString}`) 
                .subscribe((response: any) => {
                    console.log(response);
                    debugger;
                    if(response)
                    {
                        // for(let i=0;i<response.data.length;i++){
                        //     if(response.data[i].id == this.id){
                        //         this.category=response.data[i];
                        //         break;
                        //     }

                        // }
                        debugger;
                        this.classes = response;
                        this.payload.class_name = this.classes.school_class.class_name;
                        /*/
                        this.classes.class_sections.forEach(function(value){
                            debugger;
                           this.faculty_id = value.faculty.id;
                            this.section_name = value.sections.section_name;
                            this.section_id = value.sections.id;
                            debugger;
                            let obj ={
                                section_name : this.section_name,
                                faculty_id : this.faculty_id,
                                id : this.section_id 
                            

                            }
                            this.payload.sections.push(this.obj);
                           // this.payload.sections.faculty_id.push(this.fac);
                           
                 } );  
                 */
                    }
                    resolve(response);
                }, reject);
        });
    }

    updateClasses(classes): Promise<any>
    {
        // debugger;
        return new Promise((resolve, reject) => {
            debugger;
           // this.http.put(this.appService.merchantService + 'categories/'+classes.id, classes)
           this.http.patch(`${this.appService.adminService}class_sections`, classes)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }
    getFaculty(): Promise<any> {
        return new Promise((resolve, reject) => {
            // this.http.get(this.appService.apiUrl + 'cms/api/users/')
            // this.http.get('http://104.42.179.33:8765/cms/api/coupons/coupons?page=0&size=20&sort=couponCode,asc&searchTerms=gag&couponCode=123&couponType=Deal')
            this.http.get(`${this.appService.adminService}faculties/show_all_faculties`)
                .subscribe((response: any) => {
                    debugger;
                    this.faculties = response.sms_users;
                    resolve(response);
                }, reject);
        });
    }
    getCourses(): Promise<any> {
        return new Promise((resolve, reject) => {
            // this.http.get(this.appService.apiUrl + 'cms/api/users/')
            // this.http.get('http://104.42.179.33:8765/cms/api/coupons/coupons?page=0&size=20&sort=couponCode,asc&searchTerms=gag&couponCode=123&couponType=Deal')
            this.http.get(`${this.appService.adminService}courses/show_all_courses`)
                .subscribe((response: any) => {
                    debugger;
                    //this.coupons = response;
                    this.courses = response;
                    resolve(response);
                }, reject);
        });
    }

}