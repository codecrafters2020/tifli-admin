import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FuseAngularMaterialModule } from '../../components/angular-material/angular-material.module';

import { FuseSharedModule } from '@fuse/shared.module';

const routes = [
    {
        path        : 'list',
        loadChildren: './list-classes/list-classes.module#ListClassesModule'
    },
    {
        path        : 'add',
        loadChildren: './add-classes/add-classes.module#AddClassesModule'
    },
    {
        path        : 'edit/:id',
        loadChildren: './edit-classes/edit-classes.module#EditClassesModule'
    },
    {
        path        : 'faculty/:id',
        loadChildren: './faculty-classes/faculty-classes.module#FacultyClassesModule'
    }
];

@NgModule({
    imports     : [
        FuseSharedModule,
        RouterModule.forChild(routes),
        FuseAngularMaterialModule
    ],
    declarations: []
})
export class ClassesModule
{
}
