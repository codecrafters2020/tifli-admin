import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';

import { ListClassesService } from './list-classes.service';
import { Router } from '@angular/router';

@Component({
    selector     : 'fuse-file-manager',
    templateUrl  : './list-classes.component.html',
    styleUrls    : ['./list-classes.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class ListClassesComponent implements OnInit
{
    selected: any;
    pathArr: string[];

    constructor(private ListClassesService: ListClassesService, private router: Router)
    {
    }

    ngOnInit()
    {
        // this.ListMerchantCategoryService.onFileSelected.subscribe(selected => {
        //     this.selected = selected;
        //    // this.pathArr = selected.location.split('>');
        // });
    }

    openClassesAdd(){
        this.router.navigateByUrl("pages/classes/add");
    }
}
