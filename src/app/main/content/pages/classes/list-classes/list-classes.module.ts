import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CdkTableModule } from '@angular/cdk/table';

import { MatFormFieldModule, MatInputModule, MatSelectModule, MatStepperModule, MatButtonModule, MatIconModule, MatRippleModule, MatSidenavModule, MatSlideToggleModule, MatTableModule, MatMenuModule, MatPaginatorModule, MatSortModule } from '@angular/material';

import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatRadioModule} from '@angular/material/radio';
import { FuseSharedModule } from '@fuse/shared.module';

import { ListClassesComponent } from './list-classes.component';
import { ListClassesService } from './list-classes.service';
import { GridClassesComponent } from './grid/grid-classes.component';
import { SidenavClassesMainComponent } from './sidenavs/main/sidenav-classes-main.component';
import { SidenavClassesDetailsComponent } from './sidenavs/details/sidenav-classes-details.component';

const routes: Routes = [
    {
        path     : '**',
        component: ListClassesComponent,
        children : [],
        resolve  : {
            files: ListClassesService
        }
    }
];

@NgModule({
    declarations: [
        ListClassesComponent,
        GridClassesComponent,
        SidenavClassesMainComponent,
        SidenavClassesDetailsComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        CdkTableModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatButtonModule,
        MatIconModule,
        MatRippleModule,
        MatSlideToggleModule,
        MatSidenavModule,
        MatTableModule,
        MatMenuModule,
        MatPaginatorModule, 
        MatSortModule,

        FuseSharedModule,
        MatDatepickerModule,
        MatCheckboxModule,
        MatRadioModule
    ],
    providers   : [
        ListClassesService
    ]
})
export class ListClassesModule
{
}
