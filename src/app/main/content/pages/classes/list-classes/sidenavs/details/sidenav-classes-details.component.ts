import { Component, OnInit } from '@angular/core';

import { fuseAnimations } from '@fuse/animations/index';

import { ListClassesService } from '../../list-classes.service';

@Component({
    selector   : 'fuse-file-manager-details-sidenav',
    templateUrl: './sidenav-classes-details.component.html',
    styleUrls  : ['./sidenav-classes-details.component.scss'],
    animations : fuseAnimations
})
export class SidenavClassesDetailsComponent implements OnInit
{

    selected: any;

    constructor(private ListClassesService: ListClassesService)
    {

    }

    ngOnInit()
    {
        this.ListClassesService.onFileSelected.subscribe(selected => {
            this.selected = selected;
        });
    }

}
