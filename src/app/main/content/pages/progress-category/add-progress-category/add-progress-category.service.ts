import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { AppService } from '../../../../../app.service';

@Injectable()
export class AddProgressCategoryService implements Resolve<any>
{
    roles: any[];
    faculties: any[];
    constructor(private http: HttpClient, private appService: AppService)
    {
    }

    /**
     * The File Manager App Main Resolver
     * 
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {

           // return new Promise((resolve, reject) => {
        //     // debugger;
        //     resolve();
        // });
       /*/
        return new Promise((resolve, reject) => {

            Promise.all([
               // this.getFaculty(),
                //this.getclass()
            ]).then(
                ([faculty]) => {
                    resolve();
                },
                reject
                );
        });
        */
    }

    addProgressCategory(courses): Promise<any>
    {
        // debugger;
        let obj = {
            progress_category : {
                "category_name" : courses.category_name
            }
        }
        return new Promise((resolve, reject) => {
            this.http.post(`${this.appService.adminService}progress/create_progress_category`, obj)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }
    

}