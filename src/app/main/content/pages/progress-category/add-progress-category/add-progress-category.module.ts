import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CdkTableModule } from '@angular/cdk/table';

import { MatFormFieldModule, MatInputModule, MatSelectModule, MatStepperModule, MatButtonModule, MatIconModule, MatRippleModule, MatSidenavModule, MatSlideToggleModule, MatTableModule } from '@angular/material';
import {MatChipsModule} from '@angular/material/chips';
import {MatAutocompleteModule} from '@angular/material/autocomplete';

import { FuseSharedModule } from '@fuse/shared.module';

import { AddProgressCategoryComponent } from './add-progress-category.component';
import { AddProgressCategoryService } from './add-progress-category.service';
import {FormAddProgressCategoryComponent} from './form/form-add-progress-category.component';

const routes: Routes = [
    {
        path     : '**',
        component: AddProgressCategoryComponent,
        children : [],
        resolve  : {
            files: AddProgressCategoryService
        }
    }
];

@NgModule({
    declarations: [
        AddProgressCategoryComponent,
        FormAddProgressCategoryComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        CdkTableModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatButtonModule,
        MatIconModule,
        MatRippleModule,
        MatSlideToggleModule,
        MatSidenavModule,
        MatTableModule,
        MatChipsModule,
        MatAutocompleteModule,

        FuseSharedModule
    ],
    providers   : [
        AddProgressCategoryService
    ]
})
export class AddProgressCategoryModule
{
}
