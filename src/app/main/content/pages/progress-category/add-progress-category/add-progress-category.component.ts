import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';

import { AddProgressCategoryService } from './add-progress-category.service';

import { Router } from '@angular/router';

@Component({
    selector     : 'fuse-file-manager',
    templateUrl  : './add-progress-category.component.html',
    styleUrls    : ['./add-progress-category.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class AddProgressCategoryComponent implements OnInit
{
    selected: any;
    pathArr: string[];

    constructor(private AddProgressCategoryService: AddProgressCategoryService, private router: Router)
    {
    }

    ngOnInit()
    {
        
    }

    openProgressCategoryList(){
        
        this.router.navigateByUrl("pages/progress-category/list");
    }
}
