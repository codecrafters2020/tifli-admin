import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators, FormsModule, FormControl, RequiredValidator } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { EditProgressCategoryService } from '../edit-progress-category.service';
import { Router } from '@angular/router';
import { FancyImageUploaderOptions, UploadedFile } from 'ng2-fancy-image-uploader';
import { getLocaleDateTimeFormat } from '@angular/common';
import { AppService } from '../../../../../../app.service';

@Component({
    selector: 'fuse-file-list',
    templateUrl: './form-edit-progress-category.component.html',
    styleUrls: ['./form-edit-progress-category.component.scss'],
})
export class FormEditProgressCategoryComponent implements OnInit {
    
    //form: FormGroup;
    formErrors: any;
    test: any;
    step: number;

    // Horizontal Stepper
    horizontalStepperStep1: FormGroup;
    horizontalStepperStep2: FormGroup;
   
    button_disabled :boolean = false;
    horizontalStepperStep1Errors: any;
    horizontalStepperStep2Errors: any;
  
    default_choice={ course_name: "None" }
    progress_category={
        category_name:'' }


   // displayedColumns = ['enable', 'name', 'description'];
   // displayedColumnsPanelHeading = ['CategoryName', 'Description', 'isChecked'];


    constructor(private EditProgressCategoryService: EditProgressCategoryService, private formBuilder: FormBuilder, private router: Router, private snackBar: MatSnackBar, private appService: AppService) {

        this.horizontalStepperStep1Errors = {
           
        };
        this.progress_category = EditProgressCategoryService.progress_category;

    }

    ngOnInit() {

        // Horizontal Stepper form steps
        this.horizontalStepperStep1 = this.formBuilder.group({

            Course_Name: ['', [Validators.required]]
                   });

      

      



        this.horizontalStepperStep1.valueChanges.subscribe(() => {
            this.onFormValuesChanged(this.horizontalStepperStep1, this.horizontalStepperStep1Errors);
        });

    
    

    }

   
    onFormValuesChanged(horizontalStepperStep: FormGroup, horizontalStepperStepErrors: any) {
        for (const field in horizontalStepperStepErrors) {
            if (!horizontalStepperStepErrors.hasOwnProperty(field)) {
                continue;
            }
            horizontalStepperStepErrors[field] = {};

            const control = horizontalStepperStep.get(field);

            if (control && control.dirty && control.touched && !control.valid) {
                horizontalStepperStepErrors[field] = control.errors;
            }
        }
    }

    finishHorizontalStepper() {
        // alert('You have finished the horizontal stepper!');
        
        this.button_disabled = true;
        try{
        this.EditProgressCategoryService.updateProgressCategory(this.progress_category).then(response => {
            debugger;
            this.test = response;
            console.log("response: " + JSON.stringify(response));
            debugger;
            if(response.status !== "200"){
                this.snackBar.open("Something went wrong", "Error", {
                    duration: 2000,
                });
            }
            this.snackBar.open("Sucess", "Done", {
                duration: 2000,
            });
            
                this.redirect('pages/progress-category/list');
        });
        }
        catch(err){
            debugger;
        }
    }

    redirect(pagename: string) {
        this.router.navigate([pagename]);
    }

    

   
}
