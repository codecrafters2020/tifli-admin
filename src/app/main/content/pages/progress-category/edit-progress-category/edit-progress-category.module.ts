import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CdkTableModule } from '@angular/cdk/table';

import { MatFormFieldModule, MatInputModule, MatSelectModule, MatStepperModule, MatButtonModule, MatIconModule, MatRippleModule, MatSidenavModule, MatSlideToggleModule, MatTableModule, MatCheckboxModule} from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';

import { EditProgressCategoryComponent } from './edit-progress-category.component';
import { EditProgressCategoryService } from './edit-progress-category.service';
import { FormEditProgressCategoryComponent } from './form/form-edit-progress-categorycomponent';
import { FancyImageUploaderModule } from 'ng2-fancy-image-uploader';
import {MatExpansionModule} from '@angular/material/expansion';


const routes: Routes = [
    {
        path     : '**',
        component:EditProgressCategoryComponent,
        children : [],
        resolve  : {
            merchantById : EditProgressCategoryService
        }
    }
];

@NgModule({
    declarations: [
        EditProgressCategoryComponent,
        FormEditProgressCategoryComponent,
    ],
    imports     : [
        RouterModule.forChild(routes),

        CdkTableModule,
        MatFormFieldModule,
        MatExpansionModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatButtonModule,
        MatIconModule,
        MatRippleModule,
        MatSlideToggleModule,
        MatSidenavModule,
        MatTableModule,
        FancyImageUploaderModule,
        MatCheckboxModule,

        FuseSharedModule
    ],
    providers   : [
        EditProgressCategoryService
    ]
})
export class EditProgressCategoryModule
{
}
