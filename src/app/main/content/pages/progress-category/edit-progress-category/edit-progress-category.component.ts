import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';

import { EditProgressCategoryService } from './edit-progress-category.service';

import { Router } from '@angular/router';

@Component({
    selector     : 'fuse-file-manager',
    templateUrl  : './edit-progress-category.component.html',
    styleUrls    : ['./edit-progress-category.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class EditProgressCategoryComponent implements OnInit
{
    selected: any;
    pathArr: string[];

    constructor(private EditProgressCategoryService: EditProgressCategoryService, private router: Router)
    {
    }

    ngOnInit()
    {
        // this.AddMerchantService.onFileSelected.subscribe(selected => {
        //     this.selected = selected;
        //     this.pathArr = selected.location.split('>');
        // });
    }

    openProgressCategoryList(){
        
        this.router.navigateByUrl("pages/progress-category/list/");
    }
}
