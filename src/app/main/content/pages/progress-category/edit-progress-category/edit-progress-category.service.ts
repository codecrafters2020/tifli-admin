import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { AppService } from '../../../../../app.service';

@Injectable()
export class EditProgressCategoryService implements Resolve<any>
{

    
    id:any;
    progress_category:any;

    constructor(private http: HttpClient, private appService: AppService)
    {
    }

    /**
     * The File Manager App Main Resolver
     * 
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {

       this.id=route.params['id'];
        return new Promise((resolve, reject) => {

            Promise.all([
                this.getProgressCategoryById(),
            ]).then(
                ([]) => {
                    resolve();
                },
                reject
            );
        });
    }


    getProgressCategoryById(): Promise<any>
    {
        let queryString = 'id=' + this.id ;

        return new Promise((resolve, reject) => {
            this.http.get(`${this.appService.adminService}courses/get_course?${queryString}`)
            .subscribe((response: any) => {
                      debugger;
                    console.log(response);
                    if(response)
                    {
                        debugger;
                        this.progress_category=response;
                    }
                    resolve(response);
                }, reject);
        });
    }  


    updateProgressCategory(courses): Promise<any>
    {
        return new Promise((resolve, reject) => {
            // this.http.get(this.appService.apiUrl + 'cms/api/users/')
            // this.http.get('http://104.42.179.33:8765/cms/api/coupons/coupons?page=0&size=20&sort=couponCode,asc&searchTerms=gag&couponCode=123&couponType=Deal')
            this.http.patch(`${this.appService.adminService}courses`,courses)
                .subscribe((response: any) => {
                    debugger;
                    this.progress_category = response;
                   
                    resolve(response);
                }, reject);
        });
}
}