import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FuseAngularMaterialModule } from '../../components/angular-material/angular-material.module';

import { FuseSharedModule } from '@fuse/shared.module';

const routes = [
    {
        path        : 'list',
        loadChildren: './list-progress-category/list-progress-category.module#ListProgressCategoryModule'
    },
    {
        path        : 'add',
        loadChildren: './add-progress-category/add-progress-category.module#AddProgressCategoryModule'
    },
    {
        path        : 'edit/:id',
        loadChildren: './edit-progress-category/edit-progress-category.module#EditProgressCategoryModule'
    }
];

@NgModule({
    imports     : [
        FuseSharedModule,
        RouterModule.forChild(routes),
        FuseAngularMaterialModule
    ],
    declarations: []
})
export class ProgressCategoryModule
{
}
