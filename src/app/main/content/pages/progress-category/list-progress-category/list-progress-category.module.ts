import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CdkTableModule } from '@angular/cdk/table';

import { MatAutocompleteModule, MatFormFieldModule, MatInputModule, MatSelectModule, MatStepperModule, MatButtonModule, MatIconModule, MatRippleModule, MatSidenavModule, MatSlideToggleModule, MatTableModule, MatMenuModule, MatPaginatorModule, MatSortModule } from '@angular/material';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatRadioModule} from '@angular/material/radio';
import { FuseSharedModule } from '@fuse/shared.module';

import { ListProgressCategoryComponent } from './list-progress-category.component';
import { ListProgressCategoryService } from './list-progress-category.service';
import { GridProgressCategoryComponent } from './grid/grid-progress-category.component';
import { SidenavProgressCategoryMainComponent } from './sidenavs/main/sidenav-progress-category-main.component';
import { SidenavProgressCategoryDetailsComponent } from './sidenavs/details/sidenav-progress-category-details.component';

const routes: Routes = [
    {
        path     : '**',
        component: ListProgressCategoryComponent,
        children : [],
        resolve  : {
            files: ListProgressCategoryService
        }
    }
];

@NgModule({
    declarations: [
        ListProgressCategoryComponent,
        GridProgressCategoryComponent,
        SidenavProgressCategoryMainComponent,
        SidenavProgressCategoryDetailsComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        CdkTableModule,
        MatAutocompleteModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatButtonModule,
        MatIconModule,
        MatRippleModule,
        MatSlideToggleModule,
        MatSidenavModule,
        MatTableModule,
        MatMenuModule,
        MatPaginatorModule, 
        MatSortModule,
        MatProgressSpinnerModule,

        FuseSharedModule,
        MatDatepickerModule,
        MatCheckboxModule,
        MatRadioModule
    ],
    providers   : [
        ListProgressCategoryService
    ]
})
export class ListProgressCategoryModule
{
}
