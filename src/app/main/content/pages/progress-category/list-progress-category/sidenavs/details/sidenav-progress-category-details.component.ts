import { Component, OnInit } from '@angular/core';

import { fuseAnimations } from '@fuse/animations/index';

import { ListProgressCategoryService } from '../../list-progress-category.service';

@Component({
    selector   : 'fuse-file-manager-details-sidenav',
    templateUrl: './sidenav-progress-category-details.component.html',
    styleUrls  : ['./sidenav-progress-category-details.component.scss'],
    animations : fuseAnimations
})
export class SidenavProgressCategoryDetailsComponent implements OnInit
{

    selected: any;

    constructor(private ListProgressCategoryService: ListProgressCategoryService)
    {

    }

    ngOnInit()
    {
        this.ListProgressCategoryService.onFileSelected.subscribe(selected => {
            this.selected = selected;
        });
    }

}
