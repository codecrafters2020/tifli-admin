import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CdkTableModule } from '@angular/cdk/table';

import { MatAutocompleteModule, MatFormFieldModule, MatInputModule, MatSelectModule, MatStepperModule, MatButtonModule, MatIconModule, MatRippleModule, MatSidenavModule, MatSlideToggleModule, MatTableModule, MatMenuModule, MatPaginatorModule, MatSortModule } from '@angular/material';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatRadioModule} from '@angular/material/radio';
import { FuseSharedModule } from '@fuse/shared.module';

import { ListKeysComponent } from './list-keys.component';
import { ListKeysService } from './list-keys.service';
import { GridKeysComponent } from './grid/grid-keys.component';
import { SidenavKeysMainComponent } from './sidenavs/main/sidenav-keys-main.component';
import { SidenavKeysDetailsComponent } from './sidenavs/details/sidenav-keys-details.component';

const routes: Routes = [
    {
        path     : '**',
        component: ListKeysComponent,
        children : [],
        resolve  : {
            files: ListKeysService
        }
    }
];

@NgModule({
    declarations: [
        ListKeysComponent,
        GridKeysComponent,
        SidenavKeysMainComponent,
        SidenavKeysDetailsComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        CdkTableModule,
        MatAutocompleteModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatButtonModule,
        MatIconModule,
        MatRippleModule,
        MatSlideToggleModule,
        MatSidenavModule,
        MatTableModule,
        MatMenuModule,
        MatPaginatorModule, 
        MatSortModule,
        MatProgressSpinnerModule,

        FuseSharedModule,
        MatDatepickerModule,
        MatCheckboxModule,
        MatRadioModule
    ],
    providers   : [
        ListKeysService
    ]
})
export class ListKeysModule
{
}
