import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { AbstractControl, FormBuilder, FormGroup, FormArray, Validators, EmailValidator } from '@angular/forms';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRadioModule } from '@angular/material/radio';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { FormControl } from '@angular/forms';
import { Observable, BehaviorSubject} from 'rxjs';
import { of } from 'rxjs/observable/of';
import { map, startWith } from 'rxjs/operators';
import { FuseAngularMaterialModule } from '../../../../../components/angular-material/angular-material.module';

import { ListKeysService } from '../../list-keys.service';


@Component({
    selector   : 'fuse-file-manager-main-sidenav',
    templateUrl: './sidenav-keys-main.component.html',
    styleUrls  : ['./sidenav-keys-main.component.scss']
})

export class SidenavKeysMainComponent
{
    @Input() leftSideNav: any;

    form: FormGroup;
    formErrors: any;

    
    
    btnDisabled: boolean = false;
  eventResults: any[];
  event_name: any;
  event_date: any;
  course_name : any;
  ClassSection: any;
  filterClass: "";
    constructor(private ListKeysService: ListKeysService, 
                private router: Router,
                private formBuilder: FormBuilder)
    {
      // Reactive Form
      this.form = this.formBuilder.group({
        event : ['', ],
        event_date : ['', ],
        ClassSelector : ['']
    });

    // Reactive form errors
    this.formErrors = {
   
      event  : {},
      event_date : {}
    };
    this.ClassSection = this.ListKeysService.classes;
      
    }
    filter() 
    {
      this.btnDisabled = true;
      debugger;
     // this.event_name =  this.form.controls.event;
      //this.event_name=  this.event_name.value;
      this.ListKeysService.getFilteredKeys(
                                                this.filterClass                                
                                              )
        .then((response) => {
          this.btnDisabled = false;
          this.leftSideNav.toggle()
        })
    }
    onChange()
    {
      
    }
    ngOnInit() 
    {
      this.form.valueChanges.subscribe(() => {
        // console.log(this.form.value);
      });
    
      // this.form.controls.merchantName.valueChanges
      //   .debounceTime(400)
      //   .subscribe(searchTerm => {
    	// 		this.ListFacultyService.searchMerchant(searchTerm)
    	// 			.subscribe(merchantNames => {
    	// 				this.merchantSearchResult = merchantNames;
    	// 			});
      //   });

      // this.form.controls.seasonalEvent.valueChanges
      //   .debounceTime(400)
      //   .subscribe(searchTerm => {
    	// 		this.ListFacultyService.searchSeasonalEvent(searchTerm)
    	// 			.subscribe(response => {
    	// 				this.seasonalEventSearchResult = response;
    	// 			});
        // });

    }

    displaySelectedName(item: any) 
    {
      return item.event_name;
    }
}  
