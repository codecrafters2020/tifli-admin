import { Component, OnInit } from '@angular/core';

import { fuseAnimations } from '@fuse/animations/index';

import { ListKeysService } from '../../list-keys.service';

@Component({
    selector   : 'fuse-file-manager-details-sidenav',
    templateUrl: './sidenav-keys-details.component.html',
    styleUrls  : ['./sidenav-keys-details.component.scss'],
    animations : fuseAnimations
})
export class SidenavKeysDetailsComponent implements OnInit
{

    selected: any;

    constructor(private ListKeysService: ListKeysService)
    {

    }

    ngOnInit()
    {
        this.ListKeysService.onFileSelected.subscribe(selected => {
            this.selected = selected;
        });
    }

}
