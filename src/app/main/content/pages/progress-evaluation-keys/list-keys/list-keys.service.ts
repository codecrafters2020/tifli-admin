import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of'
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { AppService } from '../../../../../app.service';

import * as moment from 'moment';
import * as _ from 'lodash';
import { TableHttpExample } from 'assets/angular-material-examples/table-http/table-http-example';

@Injectable()
export class ListKeysService implements Resolve<any>
{
    coupons: any[];
    course: any[];
    couponTypes: any[];
    onFilesChanged: BehaviorSubject<any> = new BehaviorSubject({});
    onFileSelected: BehaviorSubject<any> = new BehaviorSubject({});
    classes: any;

    constructor(private http: HttpClient, private appService: AppService) {
    }

    /**
     * The File Manager App Main Resolver
     * 
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getClasses()
            ]).then(
                ([courses]) => {
                    resolve(Response);
                },
                reject
                );
        });
    }

    
    deleteKeys(course): Promise<any>
    {
        return new Promise((resolve, reject) => {
            // this.http.get(this.appService.apiUrl + 'cms/api/users/')
            // this.http.get('http://104.42.179.33:8765/cms/api/coupons/coupons?page=0&size=20&sort=couponCode,asc&searchTerms=gag&couponCode=123&couponType=Deal')
            this.http.patch(`${this.appService.adminService}courses/delete_course`,course)
                .subscribe((response: any) => {
                    debugger;
                    resolve(response);
                }, reject);
        });
}

    getKeys(): Promise<any> {

        return new Promise((resolve, reject) => {
            this.http.get(`${this.appService.adminService}courses/show_all_courses`)
                .subscribe((response: any) => {
                    debugger;
                    this.coupons = response;
                    this.course = response;
                    resolve(response);
                }, reject);
        }).catch(err => this.handleError(err, () =>  this.http.get(`${this.appService.adminService}courses/show_all_courses`)));;
    }
    protected handleError(error, continuation: () => Observable<any>) {
        let that = this;
        debugger;
        alert("No Record Available");
        if (error.status == 404 || error.status == 400) {
           that.coupons= [];
           that.course= [];  
          return of(false);
        }
     
    }


   

    getFilteredKeys(
        name: ''
        
    ): Promise<any> {
        debugger;

        let queryString = 'class_section_id=' + name;
        let that = this;
        return new Promise((resolve, reject) => {
            // this.http.get('api/coupon-list')
            // this.http.get(`http://104.42.179.33:8765/cms/api/coupons?page=0&size=20&sort=couponCode,asc&searchTerms=gag&couponCode=123&couponType=Deal`)
            this.http.get(`${this.appService.adminService}progress_evaluation_key/get_progress_evaluation_keys?${queryString}`)
                .subscribe((response: any) => {
                    debugger;
                    if (response.message=="progress evaluation keys not found"){
                        alert("No Record Available");
                        that.coupons = [];
                        this.onFilesChanged.next(that.coupons);
                        this.onFileSelected.next(that.coupons);
                    }
                    else{
                        this.coupons = response;
                        this.course = response;
                        this.onFilesChanged.next(response);
                        this.onFileSelected.next(response);
                        
                    }
                    resolve(response);
                }, reject);
        }).catch(err => this.http.get(`${this.appService.adminService}progress_evaluation_key/get_progress_evaluation_keys?${queryString}`));
    }

    getClasses(): Promise<any>
    {
        let that = this;
        return new Promise((resolve, reject) => {
           // this.http.get(this.appService.merchantService + 'categories/')
           this.http.get(`${this.appService.adminService}class_sections/show_all_classes`)
           .subscribe((response: any) => {
                    debugger;
                    that.coupons= [];
                    let obj= {
                        key_name: "Not Available",
                        key_description: "Please Select Class Section from Filters"

                    }; 
                    that.coupons.push(obj);
                   // this.course.push(obj);
                    that.classes = response.class_sections;

                    //  this.onFilesChanged.next(response.data);
                    //  this.onFileSelected.next(response[0]);
                     resolve(response);
                }, reject);
        });
    }




}
