import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';

import { EditCoursesService } from './edit-keys.service';

import { Router } from '@angular/router';

@Component({
    selector     : 'fuse-file-manager',
    templateUrl  : './edit-courses.component.html',
    styleUrls    : ['./edit-courses.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class EditCoursesComponent implements OnInit
{
    selected: any;
    pathArr: string[];

    constructor(private EditCoursesService: EditCoursesService, private router: Router)
    {
    }

    ngOnInit()
    {
        // this.AddMerchantService.onFileSelected.subscribe(selected => {
        //     this.selected = selected;
        //     this.pathArr = selected.location.split('>');
        // });
    }

    openCoursesList(){
        
        this.router.navigateByUrl("pages/courses/list/");
    }
}
