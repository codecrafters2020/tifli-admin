import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CdkTableModule } from '@angular/cdk/table';

import { MatFormFieldModule, MatInputModule, MatSelectModule, MatStepperModule, MatButtonModule, MatIconModule, MatRippleModule, MatSidenavModule, MatSlideToggleModule, MatTableModule } from '@angular/material';
import {MatChipsModule} from '@angular/material/chips';
import {MatAutocompleteModule} from '@angular/material/autocomplete';

import { FuseSharedModule } from '@fuse/shared.module';

import { AddKeysComponent } from './add-keys.component';
import { AddKeysService } from './add-keys.service';
import {FormAddKeysComponent} from './form/form-add-keys.component';

const routes: Routes = [
    {
        path     : '**',
        component: AddKeysComponent,
        children : [],
        resolve  : {
            files: AddKeysService
        }
    }
];

@NgModule({
    declarations: [
        AddKeysComponent,
        FormAddKeysComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        CdkTableModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatButtonModule,
        MatIconModule,
        MatRippleModule,
        MatSlideToggleModule,
        MatSidenavModule,
        MatTableModule,
        MatChipsModule,
        MatAutocompleteModule,

        FuseSharedModule
    ],
    providers   : [
        AddKeysService
    ]
})
export class AddKeysModule
{
}
