import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';

import { AddKeysService } from './add-keys.service';

import { Router } from '@angular/router';

@Component({
    selector     : 'fuse-file-manager',
    templateUrl  : './add-keys.component.html',
    styleUrls    : ['./add-keys.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class AddKeysComponent implements OnInit
{
    selected: any;
    pathArr: string[];

    constructor(private AddKeysService: AddKeysService, private router: Router)
    {
    }

    ngOnInit()
    {
        
    }

    openKeysList(){
        
        this.router.navigateByUrl("pages/keys/list");
    }
}
