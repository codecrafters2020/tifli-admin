import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { AppService } from '../../../../app.service';

import * as moment from 'moment';
import * as _ from 'lodash';

@Injectable()
export class ClassSectionService implements Resolve<any>
{
    coupons: any[];
    classes: any[];
    couponTypes: any[];
    onFilesChanged: BehaviorSubject<any> = new BehaviorSubject({});
    onFileSelected: BehaviorSubject<any> = new BehaviorSubject({});

    constructor(private http: HttpClient, private appService: AppService) {
    }

    /**
     * The File Manager App Main Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {

        return new Promise((resolve, reject) => {

            Promise.all([
                // this.getFaculty(),
                this.getClass()
            ]).then(
                ([classes]) => {
                    resolve();
                },
                reject
                );
        });
    }

    //
    // deleteFaculty(faculty): Promise<any>
    // {
    //     return new Promise((resolve, reject) => {
    //         // this.http.get(this.appService.apiUrl + 'cms/api/users/')
    //         // this.http.get('http://104.42.179.33:8765/cms/api/coupons/coupons?page=0&size=20&sort=couponCode,asc&searchTerms=gag&couponCode=123&couponType=Deal')
    //         this.http.patch(`${this.appService.adminService}faculties/delete_faculty`,faculty)
    //             .subscribe((response: any) => {
    //                 resolve(response);
    //             }, reject);
    //     });
    // }


    getClass(): Promise<any> {
        return new Promise((resolve, reject) => {
          this.http.get(`${this.appService.adminService}class_sections/show_all_classes`)
                .subscribe((response: any) => {
                    this.classes = response.class_sections;
                    resolve(response);
                }, reject);
        });
    }



    // getFilteredClassSection(
    //     class_section: number
    //
    //
    // ): Promise<any> {
    //     // debugger;
    //     let params = {
    //         class_section
    //     };
    //
    //     let queryString = '';
    //     _.forEach(params, (val, key) => {
    //         if (val || typeof val === 'number') {
    //             queryString = queryString + key + '=' + val + '&';
    //         }
    //     });
    //
    //     return new Promise((resolve, reject) => {
    //         // this.http.get('api/coupon-list')
    //         // this.http.get(`http://104.42.179.33:8765/cms/api/coupons?page=0&size=20&sort=couponCode,asc&searchTerms=gag&couponCode=123&couponType=Deal`)
    //         this.http.get(`${this.appService.adminService}faculties/get_filtered_faculty?${queryString}`)
    //             .subscribe((response: any) => {
    //                 this.coupons = response.sms_users;
    //                 this.onFilesChanged.next(response.sms_users);
    //                 this.onFileSelected.next(response[0]);
    //                 resolve(response);
    //             }, reject);
    //     });
    // }


}
