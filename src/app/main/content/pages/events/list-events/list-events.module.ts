import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CdkTableModule } from '@angular/cdk/table';

import { MatAutocompleteModule, MatFormFieldModule, MatInputModule, MatSelectModule, MatStepperModule, MatButtonModule, MatIconModule, MatRippleModule, MatSidenavModule, MatSlideToggleModule, MatTableModule, MatMenuModule, MatPaginatorModule, MatSortModule } from '@angular/material';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatRadioModule} from '@angular/material/radio';
import { FuseSharedModule } from '@fuse/shared.module';

import { ListEventsComponent } from './list-events.component';
import { ListEventsService } from './list-events.service';
import { GridEventsComponent } from './grid/grid-events.component';
import { SidenavEventsMainComponent } from './sidenavs/main/sidenav-events-main.component';
import { SidenavEventsDetailsComponent } from './sidenavs/details/sidenav-events-details.component';

const routes: Routes = [
    {
        path     : '**',
        component: ListEventsComponent,
        children : [],
        resolve  : {
            files: ListEventsService
        }
    }
];

@NgModule({
    declarations: [
        ListEventsComponent,
        GridEventsComponent,
        SidenavEventsMainComponent,
        SidenavEventsDetailsComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        CdkTableModule,
        MatAutocompleteModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatButtonModule,
        MatIconModule,
        MatRippleModule,
        MatSlideToggleModule,
        MatSidenavModule,
        MatTableModule,
        MatMenuModule,
        MatPaginatorModule, 
        MatSortModule,
        MatProgressSpinnerModule,

        FuseSharedModule,
        MatDatepickerModule,
        MatCheckboxModule,
        MatRadioModule
    ],
    providers   : [
        ListEventsService
    ]
})
export class ListEventsModule
{
}
