import { Component, OnInit } from '@angular/core';

import { fuseAnimations } from '@fuse/animations/index';

import { ListEventsService } from '../../list-events.service';

@Component({
    selector   : 'fuse-file-manager-details-sidenav',
    templateUrl: './sidenav-events-details.component.html',
    styleUrls  : ['./sidenav-events-details.component.scss'],
    animations : fuseAnimations
})
export class SidenavEventsDetailsComponent implements OnInit
{

    selected: any;

    constructor(private ListEventsService: ListEventsService)
    {

    }

    ngOnInit()
    {
        this.ListEventsService.onFileSelected.subscribe(selected => {
            this.selected = selected;
        });
    }

}
