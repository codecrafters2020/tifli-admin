import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of'
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { AppService } from '../../../../../app.service';

import * as moment from 'moment';
import * as _ from 'lodash';

@Injectable()
export class ListEventsService implements Resolve<any>
{
    coupons: any[];
    events: any[];
    couponTypes: any[];
    onFilesChanged: BehaviorSubject<any> = new BehaviorSubject({});
    onFileSelected: BehaviorSubject<any> = new BehaviorSubject({});

    constructor(private http: HttpClient, private appService: AppService) {
    }

    /**
     * The File Manager App Main Resolver
     * 
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getevents(),
                //this.getclass()
            ]).then(
                ([events]) => {
                    resolve();
                },
                reject
                );
        });
    }

    
    deleteEvents(events): Promise<any>
    {
        return new Promise((resolve, reject) => {
            // this.http.get(this.appService.apiUrl + 'cms/api/users/')
            // this.http.get('http://104.42.179.33:8765/cms/api/coupons/coupons?page=0&size=20&sort=couponCode,asc&searchTerms=gag&couponCode=123&couponType=Deal')
            this.http.patch(`${this.appService.adminService}events/delete_event`,events)
                .subscribe((response: any) => {
                    debugger;
                    resolve(response);
                }, reject);
        });
}

    getevents(): Promise<any> {
        return new Promise((resolve, reject) => {
            // this.http.get(this.appService.apiUrl + 'cms/api/users/')
            // this.http.get('http://104.42.179.33:8765/cms/api/coupons/coupons?page=0&size=20&sort=couponCode,asc&searchTerms=gag&couponCode=123&couponType=Deal')
            this.http.get(`${this.appService.adminService}events/show_all_events`)
                .subscribe((response: any) => {
                    debugger;
                    this.coupons = response;
                    this.events = response;
                    resolve(response);
                }, reject);
        });
    }

    getclass(): Promise<any> {
        return new Promise((resolve, reject) => {
          this.http.get(`${this.appService.adminService}events/show_all_events`)
                .subscribe((response: any) => {
                    debugger;
                    
                    resolve(response);
                }, reject);
        });
    }
    


    getFilteredEvents(
        name: '',
        date: '' 
    
        
    ): Promise<any> {
        debugger;

        let queryString = 'name=' + name;
        //+ name + ' date=' + date;

        let params = {
         
            name : 'name='+name,
            date : 'date='+date
        
        };
        /*/
        let queryString = '';
        _.forEach(params, (val, key) => {
            if (val || typeof val === 'number') {
                queryString = queryString + key + '=' + val + '&';
            }
           else if (val || typeof val === 'string') {
                queryString = queryString + key + '=' + val + '&';
            }
            
        });
        */
        return new Promise((resolve, reject) => {
            // this.http.get('api/coupon-list')
            // this.http.get(`http://104.42.179.33:8765/cms/api/coupons?page=0&size=20&sort=couponCode,asc&searchTerms=gag&couponCode=123&couponType=Deal`)
            this.http.get(`${this.appService.adminService}events/get_filtered_events?${queryString}`)
                .subscribe((response: any) => {
                    this.coupons = response;
                    this.onFilesChanged.next(response.sms_users);
                    this.onFileSelected.next(response[0]);
                    resolve(response);
                }, reject);
        });
    }


}
