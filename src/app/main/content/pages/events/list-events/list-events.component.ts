import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, FormArray, Validators, EmailValidator } from '@angular/forms';
import { fuseAnimations } from '@fuse/animations';

import { ListEventsService } from './list-events.service';
import { Router } from '@angular/router';

@Component({
    selector: 'fuse-file-manager',
    templateUrl: './list-events.component.html',
    styleUrls: ['./list-events.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class ListEventsComponent implements OnInit {
    selected: any;
    pathArr: string[];
    isSearch: boolean = false;
    form: FormGroup;
    merchantSearchResult: any[];


    constructor(private ListEventsService: ListEventsService, private router: Router, private formBuilder: FormBuilder) {
      //  this.ListCouponService.getCoupons();
        this.form = this.formBuilder.group({
            merchantName : ['', ]})

    }

    ngOnInit() {
        this.ListEventsService.onFileSelected.subscribe(selected => {
            this.selected = selected;
            // this.pathArr = selected.location.split('>');
        });

    }

    openCouponAdd() {
        this.router.navigateByUrl("pages/events/add");

    }

    resetGrid() {
        this.ListEventsService.getevents();
    }
    
    searching() {

        if(this.isSearch == false){
            this.isSearch = true;
        }

        else{
            this.isSearch = false;
            console.log("close it")
        }
    }
}
