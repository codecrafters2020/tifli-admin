import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';

import { AddEventsService } from './add-events.service';

import { Router } from '@angular/router';

@Component({
    selector     : 'fuse-file-manager',
    templateUrl  : './add-events.component.html',
    styleUrls    : ['./add-events.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class AddEventsComponent implements OnInit
{
    selected: any;
    pathArr: string[];

    constructor(private AddEventsService: AddEventsService, private router: Router)
    {
    }

    ngOnInit()
    {
        // this.AddMerchantService.onFileSelected.subscribe(selected => {
        //     this.selected = selected;
        //     this.pathArr = selected.location.split('>');
        // });
    }

    openEventsList(){
        
        this.router.navigateByUrl("pages/events/list/");
    }
}
