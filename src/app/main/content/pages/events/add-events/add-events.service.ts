import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { AppService } from '../../../../../app.service';
import { AbstractControl } from '@angular/forms';
import { HttpClient,HttpHeaders, } from '@angular/common/http';
import { Headers } from '@angular/http';
import { RequestOptions } from '@angular/http';
import {AuthService} from '../../../../../auth/auth.service'
import {InterceptorSkipHeader} from '../../../../../auth/jwt.interceptor';


@Injectable()
export class AddEventsService implements Resolve<any>
{

    categoriesList: any[];
    usersList: any[];
    barcodesList: any[];
    affiliateNetworksList: any[];
    merchantAffiliateStatusList: any[];
    merchantsList: any[];
    merchantFound: boolean = false;
    merchantTierList: any[];
    headers: any;

    constructor(private http: HttpClient, private appService: AppService ,public globalVar: AuthService) {
    }

    /**
     * The File Manager App Main Resolver
     * 
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {

      
    }
    getSignedUploadRequest(name,ext, type) {
        this.setHeader()
        // return this.http.get(`${this.apiUrl}/companies/presign_upload?filename=${name}&filetype=${type}&contentType=jpeg`, {headers: this.headers});

        return this.http.get(`${this.appService.uploadImageService}/signed_urls.json?filename=${name}&filetype=${type}&contentType=jpg`,
        {headers: this.headers});


      }
      putFileToS3(body: File, presignedUrl: string){
        type bodyType = 'body';

        // upload file to the pre-signed url
        const httpOptions = {
          headers: new HttpHeaders({
            'Content-Type': 'image/jpeg'
              }),
         observe: <bodyType>'response'
        };
        const headers = new HttpHeaders().set(InterceptorSkipHeader, '');

        return this.http.put(presignedUrl, body, { headers });
        }

        uploadFile(url, file) {

          return this.http.put(url, file);
      }
      setHeader(){
        this.headers = new HttpHeaders({
           Authorization: this.globalVar.user.accessToken|| '',
           "Access-Control-Allow-Origin": "*"

        });
      }

    addevents(SmsUser): Promise<any> {
        return new Promise((resolve, reject) => {
            debugger;
            this.http.post(`${this.appService.adminService}events`, SmsUser)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }



}
