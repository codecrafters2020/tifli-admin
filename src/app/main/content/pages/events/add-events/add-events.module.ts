import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CdkTableModule } from '@angular/cdk/table';

import { MatFormFieldModule, MatInputModule, MatSelectModule, MatStepperModule, MatButtonModule, MatIconModule, MatRippleModule, MatSidenavModule, MatSlideToggleModule, MatTableModule, MatCheckboxModule} from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';

import { AddEventsComponent } from './add-events.component';
import { FormAddEventsComponent } from './form/form-add-events.component';
import { AddEventsService } from './add-events.service';
import { FancyImageUploaderModule } from 'ng2-fancy-image-uploader';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatProgressBarModule} from '@angular/material/progress-bar';

const routes: Routes = [
    {
        path     : '**',
        component:AddEventsComponent,
        children : [],
        resolve  : {
            merchantById : AddEventsService
        }
    }
];

@NgModule({
    declarations: [
        AddEventsComponent,
        FormAddEventsComponent,
    ],
    imports     : [
        RouterModule.forChild(routes),
        MatProgressBarModule,
        CdkTableModule,
        MatFormFieldModule,
        MatExpansionModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatButtonModule,
        MatIconModule,
        MatRippleModule,
        MatSlideToggleModule,
        MatSidenavModule,
        MatTableModule,
        FancyImageUploaderModule,
        MatCheckboxModule,

        FuseSharedModule
    ],
    providers   : [
        AddEventsService
    ]
})
export class AddEventsModule
{
}
