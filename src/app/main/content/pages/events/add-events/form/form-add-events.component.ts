import { Component, OnInit,NgZone } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators, FormsModule, FormControl, RequiredValidator } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { AddEventsService } from '../add-events.service';
import { Router } from '@angular/router';
import { FancyImageUploaderOptions, UploadedFile } from 'ng2-fancy-image-uploader';
import { getLocaleDateTimeFormat } from '@angular/common';
import { AppService } from '../../../../../../app.service';

@Component({
    selector: 'fuse-file-list',
    templateUrl: './form-add-events.component.html',
    styleUrls: ['./form-add-events.component.scss'],
})
export class FormAddEventsComponent implements OnInit {
    
    //form: FormGroup;
    formErrors: any;
    test: any;
    step: number;

    // Horizontal Stepper
    horizontalStepperStep1: FormGroup;
    horizontalStepperStep2: FormGroup;
    button_disabled :boolean = false;
    showLoadingBar: boolean = false;
    photoUpload: boolean = false;
    horizontalStepperStep1Errors: any;
    horizontalStepperStep2Errors: any;
    imagePreview : string;
    avatar: any;
    default_choice={id: "-1", event_name: "None" ,description: "none"}
    events={
        event_name:'',
        description:'',
        event_date:'',
        image_url:'',
        location:'',
        images :[],     
      }


    displayedColumns = ['enable', 'name', 'description'];
    displayedColumnsPanelHeading = ['CategoryName', 'Description', 'isChecked'];


    constructor(private AddEventsService: AddEventsService, private formBuilder: FormBuilder, private router: Router, private snackBar: MatSnackBar, private appService: AppService) {

        this.horizontalStepperStep1Errors = {
            name: {},
            date: {},
            
           
        };

    }

    ngOnInit() {

        // Horizontal Stepper form steps
        this.horizontalStepperStep1 = this.formBuilder.group({

            Event_Name: ['', [Validators.required]],
            Event_Date: ['', [Validators.required]],
            Event_Description: ['', [Validators.required]],
            Event_Location: ['', [Validators.required]]
        });

        this.horizontalStepperStep2 = this.formBuilder.group({
            image : new FormControl(null, {})
            // merchantLargeLogo: ['', Validators.required],
            // merchantSmallLogo: ['', Validators.required]
        });

      

      



        this.horizontalStepperStep1.valueChanges.subscribe(() => {
            this.onFormValuesChanged(this.horizontalStepperStep1, this.horizontalStepperStep1Errors);
        });

        this.horizontalStepperStep2.valueChanges.subscribe(() => {
            this.onFormValuesChanged(this.horizontalStepperStep2, this.horizontalStepperStep2Errors);
        });

    

    }
    randomString = function (length) {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (var i = 0; i < length; i++) {
          text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return text;
      }
     uploadToS3(event){
        this.showLoadingBar = true;
        this.photoUpload = true;
        if (event.target.files){
          var reader = new FileReader();
          var blob;
          reader.readAsDataURL(event.target.files[0]);
          // tslint:disable-next-line:no-shadowed-variable
          const file = (event.target as HTMLInputElement).files[0];
          console.log('file',file);
          reader.onload = (event: any ) => {
            blob = event.target.result;
            this.imagePreview = event.target.result;
            // const file = (event.target as HTMLInputElement).files[0];
            this.horizontalStepperStep2.patchValue({image : blob});
            this.horizontalStepperStep2.get('image').updateValueAndValidity();
            let ext = 'jpg';
            let type = 'image/jpeg';
            let newName = this.randomString(6) + new Date().getTime() + '.' + ext;
            let that = this;
            // method 2

            // method 2
            this.AddEventsService.getSignedUploadRequest(newName, ext,type).subscribe(data => {
              console.log('Success in getSignedUploadRequest',data);
              that.avatar =  data['public_url'];
              console.log('i am in public url',data['public_url']);
              console.log('b64',blob);
              console.log(this.horizontalStepperStep2);
              let form = this.horizontalStepperStep2.value;
              blob =blob.replace(/^data:image\/\w+;base64,/, '');
              console.log('blob',blob);
              blob = this.base64toBlob(blob, 'jpg');
              this.events.image_url = data['public_url'];
              debugger;
              console.log('blob',blob);
              this.AddEventsService.putFileToS3(blob, data["presigned_url"])
              .subscribe(
                 
                response => {
                this.showLoadingBar = false;
                this.photoUpload = false;
                }
                );
            }, (err) => {

              console.log('getSignedUploadRequest timeout error: ', err);
            });

          };
        }
        else{
            this.showLoadingBar = false;
            this.photoUpload = false;
        }
      }
      base64toBlob(base64Data, contentType) {
        contentType = contentType || '';
        var sliceSize = 1024;
        var byteCharacters = atob(base64Data);
        var bytesLength = byteCharacters.length;
        var slicesCount = Math.ceil(bytesLength / sliceSize);
        var byteArrays = new Array(slicesCount);
    
        for (var sliceIndex = 0; sliceIndex < slicesCount; ++sliceIndex) {
          var begin = sliceIndex * sliceSize;
          var end = Math.min(begin + sliceSize, bytesLength);
    
          var bytes = new Array(end - begin);
          for (var offset = begin, i = 0; offset < end; ++i, ++offset) {
            bytes[i] = byteCharacters[offset].charCodeAt(0);
          }
          byteArrays[sliceIndex] = new Uint8Array(bytes);
        }
        return new Blob(byteArrays, { type: contentType });
      }

   
    onFormValuesChanged(horizontalStepperStep: FormGroup, horizontalStepperStepErrors: any) {
        for (const field in horizontalStepperStepErrors) {
            if (!horizontalStepperStepErrors.hasOwnProperty(field)) {
                continue;
            }
            horizontalStepperStepErrors[field] = {};

            const control = horizontalStepperStep.get(field);

            if (control && control.dirty && control.touched && !control.valid) {
                horizontalStepperStepErrors[field] = control.errors;
            }
        }
    }

    finishHorizontalStepper() {
        this.button_disabled = true;
        try{
        this.AddEventsService.addevents(this.events).then(response => {
            debugger;
            this.test = response;
            console.log("response: " + JSON.stringify(response));
            debugger;
            if(response.status !== "200"){
                this.snackBar.open("Something went wrong", "Error", {
                    duration: 2000,
                });
            }
            this.snackBar.open("Sucess", "Done", {
                duration: 2000,
            });
            
                this.redirect('pages/events/list');
        });
        }
        catch(err){
            debugger;
        }
    }

    redirect(pagename: string) {
        this.router.navigate([pagename]);
    }

    onUploadLargeImage(file: UploadedFile) {
        debugger;
        var response: any = {};
        response = JSON.parse(file.response)
        if (response.meta.code == 200) {
            if (this.events.images.length >= 1) {
                for (let i = 0; i < this.events.images.length; i++) {
                    if(this.events.images[i].suffix=="large")
                    {
                        this.events.images.splice(i, 1);
                    }
                }
            }
            var largeImage: any = {};
            largeImage.fileName = response.data.fileName;
            largeImage.suffix = "large";
            this.events.images.push(largeImage);
        }
    }

   
}
