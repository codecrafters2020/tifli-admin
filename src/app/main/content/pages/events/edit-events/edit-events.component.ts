import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';

import { EditEventsService } from './edit-events.service';

import { Router } from '@angular/router';

@Component({
    selector     : 'fuse-file-manager',
    templateUrl  : './edit-events.component.html',
    styleUrls    : ['./edit-events.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class EditEventsComponent implements OnInit
{
    selected: any;
    pathArr: string[];

    constructor(private EditEventsService: EditEventsService, private router: Router)
    {
    }

    ngOnInit()
    {
        // this.AddMerchantService.onFileSelected.subscribe(selected => {
        //     this.selected = selected;
        //     this.pathArr = selected.location.split('>');
        // });
    }

    openEventsList(){
        
        this.router.navigateByUrl("pages/events/list/");
    }
}
