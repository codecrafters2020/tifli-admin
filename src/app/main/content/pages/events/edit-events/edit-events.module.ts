import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CdkTableModule } from '@angular/cdk/table';

import { MatFormFieldModule, MatInputModule, MatSelectModule, MatStepperModule, MatButtonModule, MatIconModule, MatRippleModule, MatSidenavModule, MatSlideToggleModule, MatTableModule, MatCheckboxModule} from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';

import { EditEventsComponent } from './edit-events.component';
import { EditEventsService } from './edit-events.service';
import { FormEditEventsComponent } from './form/form-edit-events.component';
import { FancyImageUploaderModule } from 'ng2-fancy-image-uploader';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatProgressBarModule} from '@angular/material/progress-bar';

const routes: Routes = [
    {
        path     : '**',
        component:EditEventsComponent,
        children : [],
        resolve  : {
            merchantById : EditEventsService
        }
    }
];

@NgModule({
    declarations: [
        EditEventsComponent,
        FormEditEventsComponent,
    ],
    imports     : [
        RouterModule.forChild(routes),
        MatProgressBarModule,
        CdkTableModule,
        MatFormFieldModule,
        MatExpansionModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatButtonModule,
        MatIconModule,
        MatRippleModule,
        MatSlideToggleModule,
        MatSidenavModule,
        MatTableModule,
        FancyImageUploaderModule,
        MatCheckboxModule,

        FuseSharedModule
    ],
    providers   : [
        EditEventsService
    ]
})
export class EditEventsModule
{
}
