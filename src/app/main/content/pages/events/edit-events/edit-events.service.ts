import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { AppService } from '../../../../../app.service';
import { HttpHeaders, } from '@angular/common/http';
import { Headers } from '@angular/http';
import { RequestOptions } from '@angular/http';
import {AuthService} from '../../../../../auth/auth.service'
import {InterceptorSkipHeader} from '../../../../../auth/jwt.interceptor';

@Injectable()
export class EditEventsService implements Resolve<any>
{

    
    id:any;
    events:any;
    headers: any;

    constructor(private http: HttpClient, private appService: AppService,public globalVar: AuthService)
    {
    }

    /**
     * The File Manager App Main Resolver
     * 
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {

       this.id=route.params['id'];
        return new Promise((resolve, reject) => {

            Promise.all([
                this.getEventsById(),
            ]).then(
                ([]) => {
                    resolve();
                },
                reject
            );
        });
    }
    getSignedUploadRequest(name,ext, type) {
        this.setHeader()
        // return this.http.get(`${this.apiUrl}/companies/presign_upload?filename=${name}&filetype=${type}&contentType=jpeg`, {headers: this.headers});

        return this.http.get(`${this.appService.uploadImageService}/signed_urls.json?filename=${name}&filetype=${type}&contentType=jpg`,
        {headers: this.headers});


      }
      putFileToS3(body: File, presignedUrl: string){
        type bodyType = 'body';

        // upload file to the pre-signed url
        const httpOptions = {
          headers: new HttpHeaders({
            'Content-Type': 'image/jpeg'
              }),
         observe: <bodyType>'response'
        };
        const headers = new HttpHeaders().set(InterceptorSkipHeader, '');

        return this.http.put(presignedUrl, body, { headers });
        }

        uploadFile(url, file) {

          return this.http.put(url, file);
      }
      setHeader(){
        this.headers = new HttpHeaders({
           Authorization: this.globalVar.user.accessToken|| '',
           "Access-Control-Allow-Origin": "*"

        });
      }

    getEventsById(): Promise<any>
    {
        let queryString = 'id=' + this.id ;

        return new Promise((resolve, reject) => {
            this.http.get(`${this.appService.adminService}events/get_event?${queryString}`)
            .subscribe((response: any) => {
                      debugger;
                    console.log(response);
                    if(response)
                    {
                        debugger;
                        this.events=response;
                    }
                    resolve(response);
                }, reject);
        });
    }  


    updateEvents(event): Promise<any>
    {
        return new Promise((resolve, reject) => {
            // this.http.get(this.appService.apiUrl + 'cms/api/users/')
            // this.http.get('http://104.42.179.33:8765/cms/api/coupons/coupons?page=0&size=20&sort=couponCode,asc&searchTerms=gag&couponCode=123&couponType=Deal')
            this.http.patch(`${this.appService.adminService}events`,event)
                .subscribe((response: any) => {
                    debugger;
                    this.events = response.events;
                   
                    resolve(response);
                }, reject);
        });
}
}