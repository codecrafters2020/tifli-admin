import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FuseAngularMaterialModule } from '../../components/angular-material/angular-material.module';

import { FuseSharedModule } from '@fuse/shared.module';

const routes = [
    {
        path        : 'list',
        loadChildren: './list-events/list-events.module#ListEventsModule'
    },
    {
        path        : 'add',
        loadChildren: './add-events/add-events.module#AddEventsModule'
    },
    {
        path        : 'edit/:id',
        loadChildren: './edit-events/edit-events.module#EditEventsModule'
    }
];

@NgModule({
    imports     : [
        FuseSharedModule,
        RouterModule.forChild(routes),
        FuseAngularMaterialModule
    ],
    declarations: []
})
export class EventsModule
{
}
