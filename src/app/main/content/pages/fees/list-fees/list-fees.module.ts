import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CdkTableModule } from '@angular/cdk/table';

import { MatAutocompleteModule, MatFormFieldModule, MatInputModule, MatSelectModule, MatStepperModule, MatButtonModule, MatIconModule, MatRippleModule, MatSidenavModule, MatSlideToggleModule, MatTableModule, MatMenuModule, MatPaginatorModule, MatSortModule } from '@angular/material';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatRadioModule} from '@angular/material/radio';
import { FuseSharedModule } from '@fuse/shared.module';

import { ListFeesComponent } from './list-fees.component';
import { ListFeesService } from './list-fees.service';
import { GridFeesComponent } from './grid/grid-fees.component';
import { SidenavFeesMainComponent } from './sidenavs/main/sidenav-fees-main.component';
import { SidenavFeesDetailsComponent } from './sidenavs/details/sidenav-fees-details.component';

const routes: Routes = [
    {
        path     : '**',
        component: ListFeesComponent,
        children : [],
        resolve  : {
            files: ListFeesService
        }
    }
];

@NgModule({
    declarations: [
        ListFeesComponent,
        GridFeesComponent,
        SidenavFeesMainComponent,
        SidenavFeesDetailsComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        CdkTableModule,
        MatAutocompleteModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatButtonModule,
        MatIconModule,
        MatRippleModule,
        MatSlideToggleModule,
        MatSidenavModule,
        MatTableModule,
        MatMenuModule,
        MatPaginatorModule, 
        MatSortModule,
        MatProgressSpinnerModule,

        FuseSharedModule,
        MatDatepickerModule,
        MatCheckboxModule,
        MatRadioModule
    ],
    providers   : [
        ListFeesService
    ]
})
export class ListFeesModule
{
}
