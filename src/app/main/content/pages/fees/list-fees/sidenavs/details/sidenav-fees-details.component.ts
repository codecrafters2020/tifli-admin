import { Component, OnInit } from '@angular/core';

import { fuseAnimations } from '@fuse/animations/index';

import { ListFeesService } from '../../list-fees.service';

@Component({
    selector   : 'fuse-file-manager-details-sidenav',
    templateUrl: './sidenav-fees-details.component.html',
    styleUrls  : ['./sidenav-fees-details.component.scss'],
    animations : fuseAnimations
})
export class SidenavFeesDetailsComponent implements OnInit
{

    selected: any;

    constructor(private ListFeesService: ListFeesService)
    {

    }

    ngOnInit()
    {
        this.ListFeesService.onFileSelected.subscribe(selected => {
            this.selected = selected;
        });
    }

}
