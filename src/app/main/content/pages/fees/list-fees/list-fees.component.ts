import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, FormArray, Validators, EmailValidator } from '@angular/forms';
import { fuseAnimations } from '@fuse/animations';

import { ListFeesService } from './list-fees.service';
import { Router } from '@angular/router';

@Component({
    selector: 'fuse-file-manager',
    templateUrl: './list-fees.component.html',
    styleUrls: ['./list-fees.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class ListFeesComponent implements OnInit {
    selected: any;
    pathArr: string[];
    isSearch: boolean = false;
    form: FormGroup;
    merchantSearchResult: any[];


    constructor(private ListFeesService: ListFeesService, private router: Router, private formBuilder: FormBuilder) {
      //  this.ListCouponService.getCoupons();
        this.form = this.formBuilder.group({
            merchantName : ['', ]})

    }

    ngOnInit() {
        this.ListFeesService.onFileSelected.subscribe(selected => {
            this.selected = selected;
            // this.pathArr = selected.location.split('>');
        });

    }

    openCouponAdd() {
        this.router.navigateByUrl("pages/fees/add");

    }

    resetGrid() {
        this.ListFeesService.getFees();
    }
    
    searching() {

        if(this.isSearch == false){
            this.isSearch = true;
        }

        else{
            this.isSearch = false;
            console.log("close it")
        }
    }
}
