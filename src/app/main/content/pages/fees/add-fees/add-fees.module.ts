import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CdkTableModule } from '@angular/cdk/table';
import {MatChipsModule} from '@angular/material/chips';
import { MatFormFieldModule, MatInputModule, MatSelectModule, MatStepperModule, MatButtonModule, MatIconModule, MatRippleModule, MatSidenavModule, MatSlideToggleModule, MatTableModule, MatCheckboxModule} from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';

import { AddFeesComponent } from './add-fees.component';
import { FormAddFeesComponent } from './form/form-add-fees.component';
import { AddFeesService } from './add-fees.service';
import { FancyImageUploaderModule } from 'ng2-fancy-image-uploader';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatAutocompleteModule} from '@angular/material/autocomplete';

const routes: Routes = [
    {
        path     : '**',
        component:AddFeesComponent,
        children : [],
        resolve  : {
            merchantById : AddFeesService
        }
    }
];

@NgModule({
    declarations: [
        AddFeesComponent,
        FormAddFeesComponent,
    ],
    imports     : [
        RouterModule.forChild(routes),

        CdkTableModule,
        MatFormFieldModule,
        MatExpansionModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatButtonModule,
        MatIconModule,
        MatRippleModule,
        MatSlideToggleModule,
        MatSidenavModule,
        MatTableModule,
        FancyImageUploaderModule,
        MatCheckboxModule,
        MatChipsModule,
        MatDatepickerModule,
        MatAutocompleteModule,
        FuseSharedModule
    ],
    providers   : [
        AddFeesService
    ]
})
export class AddFeesModule
{
}
