import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';

import { AddFeesService } from './add-fees.service';

import { Router } from '@angular/router';

@Component({
    selector     : 'fuse-file-manager',
    templateUrl  : './add-fees.component.html',
    styleUrls    : ['./add-fees.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class AddFeesComponent implements OnInit
{
    selected: any;
    pathArr: string[];

    constructor(private AddFeesService: AddFeesService, private router: Router)
    {
    }

    ngOnInit()
    {
        // this.AddMerchantService.onFileSelected.subscribe(selected => {
        //     this.selected = selected;
        //     this.pathArr = selected.location.split('>');
        // });
    }

    openFeesList(){
        
        this.router.navigateByUrl("pages/Fees/list/");
    }
}
