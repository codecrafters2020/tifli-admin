import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators, FormsModule, FormControl, RequiredValidator } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { AddFeesService } from '../add-fees.service';
import { Router } from '@angular/router';
import { FancyImageUploaderOptions, UploadedFile } from 'ng2-fancy-image-uploader';
import { getLocaleDateTimeFormat } from '@angular/common';
import { AppService } from '../../../../../../app.service';
import {ElementRef, ViewChild} from '@angular/core';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { MatAutocompleteSelectedEvent, MatChipInputEvent } from '@angular/material';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
@Component({
    selector: 'fuse-file-list',
    templateUrl: './form-add-fees.component.html',
    styleUrls: ['./form-add-fees.component.scss'],
})
export class FormAddFeesComponent implements OnInit {
    
    //form: FormGroup;
    formErrors: any;
    test: any;
    step: number;
    studentsFromService: any[];
    studentArray: any=[];
    selectedStudents: any=[];
    // Horizontal Stepper
    horizontalStepperStep1: FormGroup;

    visible = true;
    selectable = true;
    removable = true;
    addOnBlur = false;
    separatorKeysCodes: number[] = [ENTER, COMMA];
    studentCtrl = new FormControl();
    filteredStudents: Observable<string[]>;
    students: any = [];
    startDate: any;
    @ViewChild('studentInput') studentInput: ElementRef;
    horizontalStepperStep1Errors: any;
  
    default_choice={id: "-1", event_name: "None" ,description: "none"}
    events={
        event_name:'',
        description:'',
        event_date:'',
        image_url:'',
        location:'',
        images :[],     
      }
      PaymentStatuses = [
        { "name": "Paid", "value":  1 },
        { "name": "Pending", "value":  2 },
        { "name": "Defaulter", "value":  3 }
        
      ];
      registrationByMonths = [
        { "name": "January", "value":  '01' , "datePicker" :1 },
        { "name": "Feburary", "value":  '02' , "datePicker" :2},
        { "name": "March", "value":  '03' , "datePicker" :3},
        { "name": "April", "value":  '04' , "datePicker" :4},
        { "name": "May", "value":  '05' , "datePicker" :5},
        { "name": "June", "value":  '06' , "datePicker" :6},
        { "name": "July", "value":  '07' , "datePicker" :7},
        { "name": "August", "value":  '08' , "datePicker" :8},
        { "name": "September", "value":  '09' , "datePicker" :9},
        { "name": "October", "value":  '10' , "datePicker" :10},
        { "name": "November", "value":  '11' , "datePicker" :11},
        { "name": "December", "value":  '12' , "datePicker" :12}
    ];  
    selectedMonth  : any;

    displayedColumns = ['enable', 'name', 'description'];
    displayedColumnsPanelHeading = ['CategoryName', 'Description', 'isChecked'];


    constructor(private AddFeesService: AddFeesService, private formBuilder: FormBuilder, private router: Router, private snackBar: MatSnackBar, private appService: AppService) {

        this.horizontalStepperStep1Errors = {
            date: {}
            
           
        };
        this.startDate = new Date(1990, 0, 1);
        let that = this;
        this.studentsFromService = AddFeesService.students;
        this.studentsFromService.forEach((element, index, array) => {
            let Obj = {
                id : element.id,
                name : element.first_name.concat(' ',element.last_name)
            }
            that.studentArray.push(Obj);
        });
        that.studentArray;
        debugger;
        this.filteredStudents = this.studentCtrl.valueChanges.pipe(
            startWith(null),
            map((student: string | null) => student ? this._filter(student) : this.studentArray.slice()));
    }
    add(event: MatChipInputEvent): void {
    debugger
    const input = event.input;
    const value = event.value;
    
    if ((value || '').trim()) {
      this.students.push({
        id:Math.random(),
        name:value.trim()
      });
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }

    this.studentCtrl.setValue(null);
  }

  remove(student, indx): void {
    this.students.splice(indx, 1);
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    this.students.push(event.option.value);
    this.studentInput.nativeElement.value = '';
    this.studentCtrl.setValue(null);
  }

  private _filter(value: any): any[] {
    return this.studentArray.filter(student => student.name.toLocaleLowerCase().includes(value.toString().toLocaleLowerCase()));
  }
  
monthChanged(event){
    this.startDate = new Date(2020,parseInt(event.value,10),0);
  }
ngOnInit() {

        // Horizontal Stepper form steps
        this.horizontalStepperStep1 = this.formBuilder.group({
            Selected_Students: ['', []],
            Due_Date: ['', []],
            Amount: ['', []],
            Payment_Status: ['', []],
            month: ['', []]
        });

      

      

      



        this.horizontalStepperStep1.valueChanges.subscribe(() => {
            this.onFormValuesChanged(this.horizontalStepperStep1, this.horizontalStepperStep1Errors);
        });

       

    

    }

   
    onFormValuesChanged(horizontalStepperStep: FormGroup, horizontalStepperStepErrors: any) {
        for (const field in horizontalStepperStepErrors) {
            if (!horizontalStepperStepErrors.hasOwnProperty(field)) {
                continue;
            }
            horizontalStepperStepErrors[field] = {};

            const control = horizontalStepperStep.get(field);

            if (control && control.dirty && control.touched && !control.valid) {
                horizontalStepperStepErrors[field] = control.errors;
            }
        }
    }

    finishHorizontalStepper() {
        let formValues = this.horizontalStepperStep1.value;
        formValues.Selected_Students;
        let that = this;
        this.students.forEach((element, index, array) => {
            let Obj = {
                'id' : element.id,
            }
           debugger;
           that.selectedStudents = that.selectedStudents || [];
            that.selectedStudents.push(Obj);
        });
        let student_fees = {
            'fees_month' : formValues.month,
            'due_date' : formValues.Due_Date,
            'amount' : formValues.Amount,
            'payment_status' : formValues.Payment_Status
        }
        let obj ={
            'student_list' : that.selectedStudents,
            'student_fees' : student_fees
        }
        debugger;
        try{
        this.AddFeesService.addFees(obj).then(response => {
            debugger;
            this.test = response;
            console.log("response: " + JSON.stringify(response));
            debugger;
            if(response.status !== "200"){
                this.snackBar.open("Something went wrong", "Error", {
                    duration: 2000,
                });
            }
            this.snackBar.open("Sucess", "Done", {
                duration: 2000,
            });
            
                this.redirect('pages/fees/list');
        });
        }
        catch(err){
            
        }
    }

    redirect(pagename: string) {
        this.router.navigate([pagename]);
    }

    onUploadLargeImage(file: UploadedFile) {
     
        var response: any = {};
        response = JSON.parse(file.response)
        if (response.meta.code == 200) {
            if (this.events.images.length >= 1) {
                for (let i = 0; i < this.events.images.length; i++) {
                    if(this.events.images[i].suffix=="large")
                    {
                        this.events.images.splice(i, 1);
                    }
                }
            }
            var largeImage: any = {};
            largeImage.fileName = response.data.fileName;
            largeImage.suffix = "large";
            this.events.images.push(largeImage);
        }
    }

   
}
