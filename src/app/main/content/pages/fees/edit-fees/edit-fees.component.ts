import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';

import { EditFeesService } from './edit-fees.service';

import { Router } from '@angular/router';

@Component({
    selector     : 'fuse-file-manager',
    templateUrl  : './edit-fees.component.html',
    styleUrls    : ['./edit-fees.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class EditFeesComponent implements OnInit
{
    selected: any;
    pathArr: string[];

    constructor(private EditFeesService: EditFeesService, private router: Router)
    {
    }

    ngOnInit()
    {
        // this.AddMerchantService.onFileSelected.subscribe(selected => {
        //     this.selected = selected;
        //     this.pathArr = selected.location.split('>');
        // });
    }

    openFeesList(){
        
        this.router.navigateByUrl("pages/fees/list/");
    }
}
