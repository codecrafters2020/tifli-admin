import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CdkTableModule } from '@angular/cdk/table';
import {MatChipsModule} from '@angular/material/chips';


import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatAutocompleteModule} from '@angular/material/autocomplete';

import { MatFormFieldModule, MatInputModule, MatSelectModule, MatStepperModule, MatButtonModule, MatIconModule, MatRippleModule, MatSidenavModule, MatSlideToggleModule, MatTableModule, MatCheckboxModule} from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';

import { EditFeesComponent } from './edit-fees.component';
import { EditFeesService } from './edit-fees.service';
import { FormEditFeesComponent } from './form/form-edit-fees.component';
import { FancyImageUploaderModule } from 'ng2-fancy-image-uploader';
import {MatExpansionModule} from '@angular/material/expansion';


const routes: Routes = [
    {
        path     : '**',
        component:EditFeesComponent,
        children : [],
        resolve  : {
            merchantById : EditFeesService
        }
    }
];

@NgModule({
    declarations: [
        EditFeesComponent,
        FormEditFeesComponent,
    ],
    imports     : [
        RouterModule.forChild(routes),
        MatChipsModule,
        MatDatepickerModule,
        MatAutocompleteModule,
        CdkTableModule,
        MatFormFieldModule,
        MatExpansionModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatButtonModule,
        MatIconModule,
        MatRippleModule,
        MatSlideToggleModule,
        MatSidenavModule,
        MatTableModule,
        FancyImageUploaderModule,
        MatCheckboxModule,

        FuseSharedModule
    ],
    providers   : [
        EditFeesService
    ]
})
export class EditFeesModule
{
}
