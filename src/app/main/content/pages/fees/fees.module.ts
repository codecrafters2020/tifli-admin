import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FuseAngularMaterialModule } from '../../components/angular-material/angular-material.module';

import { FuseSharedModule } from '@fuse/shared.module';

const routes = [
    {
        path        : 'list',
        loadChildren: './list-fees/list-fees.module#ListFeesModule'
    },
    {
        path        : 'add',
        loadChildren: './add-fees/add-fees.module#AddFeesModule'
    },
    {
        path        : 'edit/:id',
        loadChildren: './edit-fees/edit-fees.module#EditFeesModule'
    }
];

@NgModule({
    imports     : [
        FuseSharedModule,
        RouterModule.forChild(routes),
        FuseAngularMaterialModule
    ],
    declarations: []
})
export class FeesModule
{
}
