import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FuseAngularMaterialModule } from '../../components/angular-material/angular-material.module';

import { FuseSharedModule } from '@fuse/shared.module';

const routes = [
    {
        path        : 'list',
        loadChildren: './list-survey/list-survey.module#ListSurveyModule'
    },
    {
        path        : 'add',
        loadChildren: './add-survey/add-survey.module#AddSurveyModule'
    },
    {
        path        : 'edit/:id',
        loadChildren: './edit-survey/edit-survey.module#EditSurveyModule'
    },
    {
        path        : 'response/:id',
        loadChildren: './response-survey/edit-survey.module#EditSurveyModule'
    },
    {
        path        : 'list-response/:id',
        loadChildren: './list-responses-survey/list-survey.module#ListSurveyModule'
    }
    
];

@NgModule({
    imports     : [
        FuseSharedModule,
        RouterModule.forChild(routes),
        FuseAngularMaterialModule
    ],
    declarations: []
})
export class SurveyModule
{
}
