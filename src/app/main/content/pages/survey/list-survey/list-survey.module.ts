import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CdkTableModule } from '@angular/cdk/table';

import { MatFormFieldModule, MatInputModule, MatSelectModule, MatStepperModule, MatButtonModule, MatIconModule, MatRippleModule, MatSidenavModule, MatSlideToggleModule, MatTableModule, MatMenuModule, MatPaginatorModule, MatSortModule } from '@angular/material';

import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatRadioModule} from '@angular/material/radio';
import { FuseSharedModule } from '@fuse/shared.module';

import { ListSurveyComponent } from './list-survey.component';
import { ListSurveyService } from './list-survey.service';
import { GridSurveyComponent } from './grid/grid-survey.component';
import { SidenavSurveyMainComponent } from './sidenavs/main/sidenav-survey-main.component';
import { SidenavSurveyDetailsComponent } from './sidenavs/details/sidenav-survey-details.component';

const routes: Routes = [
    {
        path     : '**',
        component: ListSurveyComponent,
        children : [],
        resolve  : {
            files: ListSurveyService
        }
    }
];

@NgModule({
    declarations: [
        ListSurveyComponent,
        GridSurveyComponent,
        SidenavSurveyMainComponent,
        SidenavSurveyDetailsComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        CdkTableModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatButtonModule,
        MatIconModule,
        MatRippleModule,
        MatSlideToggleModule,
        MatSidenavModule,
        MatTableModule,
        MatMenuModule,
        MatPaginatorModule, 
        MatSortModule,

        FuseSharedModule,
        MatDatepickerModule,
        MatCheckboxModule,
        MatRadioModule
    ],
    providers   : [
        ListSurveyService
    ]
})
export class ListSurveyModule
{
    
}
