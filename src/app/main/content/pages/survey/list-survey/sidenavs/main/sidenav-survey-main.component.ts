import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatRadioModule} from '@angular/material/radio';
import { FuseAngularMaterialModule } from '../../../../../components/angular-material/angular-material.module';
@Component({
    selector   : 'fuse-file-manager-main-sidenav',
    templateUrl: './sidenav-survey-main.component.html',
    styleUrls  : ['./sidenav-survey-main.component.scss']
})
export class SidenavSurveyMainComponent
{
    selected: any;

    constructor()
    {
    }
}
