import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { AppService } from '../../../../../app.service';

@Injectable()
export class ListSurveyService implements Resolve<any>
{
    merchantCategories: any[];
    onFilesChanged: BehaviorSubject<any> = new BehaviorSubject({});
    onFileSelected: BehaviorSubject<any> = new BehaviorSubject({});

    constructor(private http: HttpClient,private appService: AppService)
    {
        
    }

    /**
     * The File Manager App Main Resolver
     * 
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getFiles()
                //this.getclass()
            ]).then(
                ([files]) => {
                    resolve();
                },
                reject
                );
        });
        // return new Promise((resolve, reject) => {

        //     Promise.all([
        //         this.getFiles()
        //     ]).then(
        //         ([files]) => {
        //             resolve();
        //         },
        //         reject
        //     );
        // });
    }

    getFiles(): Promise<any>
    {
        return new Promise((resolve, reject) => {
           // this.http.get(this.appService.merchantService + 'categories/')
           this.http.get(`${this.appService.adminService}survey/get_all_surveys`)  
           .subscribe((response: any) => {
                    debugger;
                    this.merchantCategories = response.surveys;
                    this.onFilesChanged.next(response.surveys);
                    this.onFileSelected.next(response[0]);
                    resolve(response);
                }, reject);
        });
    }

}
