import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { DataSource } from '@angular/cdk/collections';
import { FormBuilder, FormControl, Validators, FormGroup } from '@angular/forms';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import {ChartsModule} from 'ng2-charts';
import { ListSurveyService } from '../list-survey.service';
import { fuseAnimations } from '@fuse/animations/index';
import { FuseUtils } from '@fuse/utils';
import { MatSnackBar } from '@angular/material';
import { ChartType, ChartOptions } from 'chart.js';
@Component({
    selector   : 'fuse-file-list',
    templateUrl: './grid-survey.component.html',
    styleUrls  : ['./grid-survey.component.scss'],
    animations : fuseAnimations
})
export class GridSurveyComponent implements OnInit
{
    files: any;
    dataSource: GridUserDataSource | null;
    displayedColumns = ['parent_id', 'q1', 'q2', 'q3', 'q4', 'q5'];
    selected: any;
    public barChartOptions = {
        scaleShowVerticalLines : true,
         responsive : true,
        title: {
            display: true,
            text: 'Qno1'
        }
    };
    public pieChartColors = [
        {
          backgroundColor: ['#0097dc', '#e91e63', '#89b8e2', '#3a5a98', '#f9a825'],
        },
      ];
    public pieChartOptions: ChartOptions = {
        responsive: true,
        legend: {
          position: 'left',
        },
        plugins: {
          datalabels: {
            formatter: (value, ctx) => {
              const label = ctx.chart.data.labels[ctx.dataIndex];
              return label;
            },
          },
        }
      };
    public barChartOptions1 = {
        scaleShowVerticalLines : true,
         responsive : true,
        title: {
            display: true,
            text: 'Qno2'
        }
    };
    
    public barChartOptions2 = {
        scaleShowVerticalLines : true,
         responsive : true,
        title: {
            display: true,
            text: 'Qno3'
        }
    };
    public barChartOptions3 = {
        scaleShowVerticalLines : true,
         responsive : true,
        title: {
            display: true,
            text: 'Qno4'
        }
    };
    public barChartOptions4 = {
        scaleShowVerticalLines : true,
         responsive : true,
        title: {
            display: true,
            text: 'Qno5'
        }
    };
    
    question1 = [0,0,0,0,0];
    question2 = [0,0,0,0,0];
    question3 = [0,0,0,0,0];
    question4= [0,0,0,0,0];
    question5 = [0,0,0,0,0];

    detailQ1 : any =[];
    detailQ2 : any =[];
    detailQ3 : any =[];
    detailQ4 : any =[];
    detailQ5 : any =[];
    public barChartLabels = ["Rating 1","Rating 2","Rating 3","Rating 4","Rating 5"];
    public barChartType = "pie";
    public barChartLegend = true;
    public barChartData = [
       
        {data: this.question1}
    ];
    public barChartData1 = [
       
        {data: this.question2}
    ];
    public barChartData2 = [
       
        {data: this.question3}
    ];
    public barChartData3 = [
       
        {data: this.question4}
    ];
    public barChartData4= [
       
        {data: this.question5}
    ];
    public 
    survey: any = {};
    form: FormGroup;
    visible: boolean = true;
    selectable: boolean = true;
    removable: boolean = true;
    addOnBlur: boolean = true;
    removedSubcategories: any[];
    questionsArray : any=[];
    surveyAnswers : any=[];
    questionsPresent : boolean = false;
    surveyID =[-1,-1,-1,-1,-1];
    surveyResponseQ1 : any=[];
    surveyResponseQ2 : any=[];
    surveyResponseQ3 : any=[];
    surveyResponseQ4 : any=[];
    surveyResponseQ5 : any=[];
    
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild('filter') filter: ElementRef;
    @ViewChild(MatSort) sort: MatSort;

    constructor(private ListSurveyService: ListSurveyService, private router: Router)
    {
        this.survey = ListSurveyService.category;
        let that = this;
        
       // let temp = this.survey.survey_questions_responses;
       if(this.survey.survey_question_responses)
       {
           that.questionsPresent = true;
       }
        
        //Inputs from dataset 
        for(let i=0; i<this.survey.survey_question_responses.length;i++){
            this.surveyAnswers[i] = this.survey.survey_question_responses[i].survey_answers;
            this.surveyID[i] = this.survey.survey_question_responses[i].survey_question.id;
            
        } 
      
     // work Below here is to populate arrays for pie-chart datasets
     
    for(let i = 0; i < this.survey.survey_question_responses.length; i++){
               
                if(i == 0){
                    if(this.surveyAnswers[i][1]){
                        that.question1[0] = this.surveyAnswers[i][1];}
                    if(this.surveyAnswers[i][2]){    
                        that.question1[1] = this.surveyAnswers[i][2];}
                    if(this.surveyAnswers[i][3]){    
                        that.question1[2] = this.surveyAnswers[i][3];}
                    if(this.surveyAnswers[i][4]){    
                        that.question1[3] = this.surveyAnswers[i][4];}
                    if(this.surveyAnswers[i][5]){    
                        that.question1[4] = this.surveyAnswers[i][5];}}

                if(i == 1){
                    if(this.surveyAnswers[i][1]){
                        that.question2[0] = this.surveyAnswers[i][1];}
                    if(this.surveyAnswers[i][2]){    
                            that.question2[1] = this.surveyAnswers[i][2];}
                    if(this.surveyAnswers[i][3]){    
                            that.question2[2] = this.surveyAnswers[i][3];}
                    if(this.surveyAnswers[i][4]){    
                            that.question2[3] = this.surveyAnswers[i][4];}
                    if(this.surveyAnswers[i][5]){    
                            that.question2[4] = this.surveyAnswers[i][5];}}
                if(i == 2){
                        if(this.surveyAnswers[i][1]){
                            that.question3[0] = this.surveyAnswers[i][1];}
                        if(this.surveyAnswers[i][2]){    
                            that.question3[1] = this.surveyAnswers[i][2];}
                        if(this.surveyAnswers[i][3]){    
                            that.question3[2] = this.surveyAnswers[i][3];}
                        if(this.surveyAnswers[i][4]){    
                            that.question3[3] = this.surveyAnswers[i][4];}
                        if(this.surveyAnswers[i][5]){    
                            that.question3[4] = this.surveyAnswers[i][5];}}
                if(i == 3){
                        if(this.surveyAnswers[i][1]){
                            that.question4[0] = this.surveyAnswers[i][1];}
                        if(this.surveyAnswers[i][2]){    
                                that.question4[1] = this.surveyAnswers[i][2];}
                        if(this.surveyAnswers[i][3]){    
                                that.question4[2] = this.surveyAnswers[i][3];}
                        if(this.surveyAnswers[i][4]){    
                                that.question4[3] = this.surveyAnswers[i][4];}
                        if(this.surveyAnswers[i][5]){    
                                that.question4[4] = this.surveyAnswers[i][5];}}
                if(i == 4){
                        if(this.surveyAnswers[i][1]){
                            that.question5[0] = this.surveyAnswers[i][1];}
                        if(this.surveyAnswers[i][2]){    
                            that.question5[1] = this.surveyAnswers[i][2];}
                        if(this.surveyAnswers[i][3]){    
                            that.question5[2] = this.surveyAnswers[i][3];}
                        if(this.surveyAnswers[i][4]){    
                            that.question5[3] = this.surveyAnswers[i][4];}
                        if(this.surveyAnswers[i][5]){    
                            that.question5[4] = this.surveyAnswers[i][5];}}}    
        
        for(let i = 0; i < this.survey.survey_responses.length; i++){

            var parent_name = this.survey.survey_responses[i].parent.first_name.concat(
               ' ', this.survey.survey_responses[i].parent.last_name
            );
            for(let j = 0; j < that.survey.survey_responses[i].answers.length; j++)
            {
                 
                for(let k = 0; k < 5; k++){

                    if(k==0){
                        if(that.survey.survey_responses[i].answers[j].question.id == that.surveyID[k]){
                           let obj = { 
                               parent_name : parent_name,
                               answer : that.survey.survey_responses[i].answers[j].answer.answer,
                               question: that.survey.survey_responses[i].answers[j].question.question
                            }
                            that.detailQ1.push(obj);
                        }      
                    }
                    else if(k==1){
                        if(that.survey.survey_responses[i].answers[j].question.id == that.surveyID[k]){
                            let obj = { 
                                parent_name : parent_name,
                                answer : that.survey.survey_responses[i].answers[j].answer.answer,
                                question: that.survey.survey_responses[i].answers[j].question.question
                            }
                            that.detailQ2.push(obj);
                        }      
                    }
                    else if(k==2){
                        if(that.survey.survey_responses[i].answers[j].question.id == that.surveyID[k]){
                            let obj = { 
                                parent_name : parent_name,
                                answer : that.survey.survey_responses[i].answers[j].answer.answer,
                                question: that.survey.survey_responses[i].answers[j].question.question
                            }
                            that.detailQ3.push(obj);
                        }      
                    }
                    else if(k==3){
                        if(that.survey.survey_responses[i].answers[j].question.id == that.surveyID[k]){
                            let obj = { 
                                parent_name : parent_name,
                                answer : that.survey.survey_responses[i].answers[j].answer.answer,
                                question: that.survey.survey_responses[i].answers[j].question.question
                            }
                            that.detailQ4.push(obj);
                        }      
                    }
                    else if(k==4){
                        if(that.survey.survey_responses[i].answers[j].question.id == that.surveyID[k]){
                            let obj = { 
                                parent_name : parent_name,
                                answer : that.survey.survey_responses[i].answers[j].answer.answer,
                                question: that.survey.survey_responses[i].answers[j].question.question
                            }
                            that.detailQ5.push(obj);
                        }      
                    }
                }
                 
            }
        }
                    
           // console.log(this.detailQ3);
    }
    

    ngOnInit()
    {
        
        // debugger;
        // this.dataSource = new GridUserDataSource(this.ListMerchantCategoryService, this.paginator, this.sort);
        this.dataSource = new GridUserDataSource(this.ListSurveyService, this.paginator, this.sort);
        // Observable.fromEvent(this.filter.nativeElement, 'keyup')
        //           .debounceTime(150)
        //           .distinctUntilChanged()
        //           .subscribe(() => {
        //               if ( !this.dataSource )
        //               {
        //                   return;
        //               }
        //               this.dataSource.filter = this.filter.nativeElement.value;
        //           });
    }
    onSubmit(){
        
    }
    onSelect(category) {
        // this.ListMerchantService.onFileSelected.next(selected);
      //  this.router.navigateByUrl("pages/survey/edit/" + category.id);
    }

    openUserDashboard(event,userid){
        this.router.navigateByUrl("apps/dashboards/project/" + userid);
    }

    openCategoryEdit(event,userid){
        this.router.navigateByUrl("pages/survey/edit/" + userid);
    }
    openSurveyResponses(event,userid){
        this.router.navigateByUrl("pages/survey/response/" + userid);
    }
}

export class GridUserDataSource extends DataSource<any>
{
    _filterChange = new BehaviorSubject('');
    _filteredDataChange = new BehaviorSubject('');

    get filteredData(): any
    {
        return this._filteredDataChange.value;
    }

    set filteredData(value: any)
    {
        this._filteredDataChange.next(value);
    }

    get filter(): string
    {
        return this._filterChange.value;
    }

    set filter(filter: string)
    {
        this._filterChange.next(filter);
    }
    constructor(private ListSurveyService: ListSurveyService, private _paginator: MatPaginator, private _sort: MatSort)
    {
        super();
        this.filteredData = this.ListSurveyService.merchantCategories;
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    // connect(): Observable<any[]>
    // {
    //     return this.ListMerchantCategoryService.onFilesChanged;
    // }
    connect(): Observable<any[]>
    {
        const displayDataChanges = [
            this.ListSurveyService.onFilesChanged,
            this._paginator.page,
            this._filterChange,
            // this._sort.sortChange
        ];

        return Observable.merge(...displayDataChanges).map(() => {
            let data = this.ListSurveyService.merchantCategories.slice();

            data = this.filterData(data);

            this.filteredData = [...data];

            //data = this.sortData(data);

            // Grab the page's slice of data.
            const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
            return data.splice(startIndex, this._paginator.pageSize);
        });
    }

    filterData(data)
    {
        if ( !this.filter )
        {
            return data;
        }
        return FuseUtils.filterArrayByString(data, this.filter);
    }

    // sortData(data): any[]
    // {
    //     if ( !this._sort.active || this._sort.direction === '' )
    //     {
    //         return data;
    //     }

    //     return data.sort((a, b) => {
    //         let propertyA: number | string = '';
    //         let propertyB: number | string = '';

    //         switch ( this._sort.active )
    //         {
    //             case 'id':
    //                 [propertyA, propertyB] = [a.id, b.id];
    //                 break;
    //             case 'name':
    //                 [propertyA, propertyB] = [a.name, b.name];
    //                 break;
    //             case 'categories':
    //                 [propertyA, propertyB] = [a.categories[0], b.categories[0]];
    //                 break;
    //             case 'price':
    //                 [propertyA, propertyB] = [a.priceTaxIncl, b.priceTaxIncl];
    //                 break;
    //             case 'quantity':
    //                 [propertyA, propertyB] = [a.quantity, b.quantity];
    //                 break;
    //             case 'active':
    //                 [propertyA, propertyB] = [a.active, b.active];
    //                 break;
    //         }

    //         const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
    //         const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

    //         return (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1);
    //     });
    // }

    disconnect()
    {
    }
}
