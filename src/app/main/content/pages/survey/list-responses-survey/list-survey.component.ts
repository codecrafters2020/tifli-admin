import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';

import { ListSurveyService } from './list-survey.service';
import { Router } from '@angular/router';

@Component({
    selector     : 'fuse-file-manager',
    templateUrl  : './list-survey.component.html',
    styleUrls    : ['./list-survey.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class ListSurveyComponent implements OnInit
{
    selected: any;
    pathArr: string[];

    constructor(private ListSurveyService: ListSurveyService, private router: Router)
    {
    }

    ngOnInit()
    {
        // this.ListMerchantCategoryService.onFileSelected.subscribe(selected => {
        //     this.selected = selected;
        //    // this.pathArr = selected.location.split('>');
        // });
    }

    openSurveyAdd(){
        this.router.navigateByUrl("pages/survey/add");
    }
}
