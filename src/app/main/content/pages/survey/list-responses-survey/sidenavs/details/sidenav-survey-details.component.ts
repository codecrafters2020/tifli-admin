import { Component, OnInit } from '@angular/core';

import { fuseAnimations } from '@fuse/animations/index';

import { ListSurveyService } from '../../list-survey.service';

@Component({
    selector   : 'fuse-file-manager-details-sidenav',
    templateUrl: './sidenav-survey-details.component.html',
    styleUrls  : ['./sidenav-survey-details.component.scss'],
    animations : fuseAnimations
})
export class SidenavSurveyDetailsComponent implements OnInit
{

    selected: any;

    constructor(private ListSurveyService: ListSurveyService)
    {

    }

    ngOnInit()
    {
        this.ListSurveyService.onFileSelected.subscribe(selected => {
            this.selected = selected;
        });
    }

}
