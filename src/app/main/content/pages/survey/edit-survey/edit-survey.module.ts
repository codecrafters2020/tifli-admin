import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CdkTableModule } from '@angular/cdk/table';

import { MatFormFieldModule, MatInputModule, MatSelectModule, MatStepperModule, MatButtonModule, MatIconModule, MatRippleModule, MatSidenavModule, MatSlideToggleModule, MatTableModule } from '@angular/material';
import {MatChipsModule} from '@angular/material/chips';
import {MatAutocompleteModule} from '@angular/material/autocomplete';

import { FuseSharedModule } from '@fuse/shared.module';

import { EditSurveyComponent } from './edit-survey.component';
import { EditSurveyService } from './edit-survey.service';
import { FormEditSurveyComponent } from './form/form-edit-survey.component';

const routes: Routes = [
    {
        path     : '**',
        component: EditSurveyComponent,
        children : [],
        resolve  : {
            files: EditSurveyService
        }
    }
];

@NgModule({
    declarations: [
        EditSurveyComponent,
        FormEditSurveyComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        CdkTableModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatButtonModule,
        MatIconModule,
        MatRippleModule,
        MatSlideToggleModule,
        MatSidenavModule,
        MatTableModule,
        MatChipsModule,
        MatAutocompleteModule,

        FuseSharedModule
    ],
    providers   : [
        EditSurveyService
    ]
})
export class EditSurveyModule
{
}
