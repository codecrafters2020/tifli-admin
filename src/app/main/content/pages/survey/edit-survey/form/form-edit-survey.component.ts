import { Component, OnInit,ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, FormArray, Validators, EmailValidator } from '@angular/forms';
import { EditSurveyService } from '../edit-survey.service';
import { Router } from '@angular/router';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { startWith } from 'rxjs/operators/startWith';
import { map } from 'rxjs/operators/map';
import { MatSnackBar } from '@angular/material';

import {
    MatAutocomplete,
    MatAutocompleteSelectedEvent,
    MatAutocompleteTrigger,
    VERSION,
  } from '@angular/material';
import { Subscription } from 'rxjs/Subscription';

@Component({
    selector   : 'fuse-file-list',
    templateUrl: './form-edit-survey.component.html',
    styleUrls  : ['./form-edit-survey.component.scss'],
})
export class FormEditSurveyComponent implements OnInit
{
    survey: any = {};
    isPublic: boolean = false;
    form: FormGroup;
    formErrors: any;
    test: any;
    class_section : any=[];
    class_section_temp : any=[];
    visible: boolean = true;
    button_disabled :boolean = false;
    selectable: boolean = true;
    removable: boolean = true;
    addOnBlur: boolean = true;
    removedSubcategories: any[];
    ClassSectionResult : any[];
    questionsArray : any=[];
    constructor(private formBuilder: FormBuilder, private EditSurveyService: EditSurveyService, private router: Router, private snackBar: MatSnackBar)
    {
        debugger;
        this.survey = EditSurveyService.category;
        this.ClassSectionResult=this.EditSurveyService.classes;
        let that = this;
       // this.class_section.push(1);
        if(this.survey.is_public == false)
        {
            if(that.survey.survey_privacies){
                that.survey.survey_privacies.forEach(function(value) {
                    let object = value.class_section_id
                    that.class_section.push(object)
                });
            }
           
        }
      
        // Reactive form errors
        this.formErrors = {
           surveyName : {},
            description  : {},
            fruitCtrl :{}
        };
    }

    ngOnInit()
    {
        // Reactive Form
        this.form = this.formBuilder.group({
            surveyName : ['', Validators.required],
            description  : ['', Validators.required],
            fruitCtrl   : [''],
            status : [''],
            questions : new FormArray([])
        });

        this.form.valueChanges.subscribe(() => {
            this.onFormValuesChanged();
        });
        debugger;
        if(this.survey.survey_questions != null){
            for(let i=0;i<this.survey.survey_questions.length;i++){
                const control = <FormArray>this.form.controls['questions'];
                control.push(this.loadSubCategory(this.survey.survey_questions[i]));
            }
        }

    }

    loadSubCategory(survey: any) {
        return this.formBuilder.group({
            question : [survey.survey_questions, Validators.required],
           
        });
    }

    initSubCategory() {
        return this.formBuilder.group({
            question : ['', Validators.required],
             });
    }

    addNewSubCategory() {
        debugger;
        let newSubCategory = {id:'',question:''};
        this.survey.survey_questions.push(newSubCategory);
        const control = <FormArray>this.form.controls['questions'];
        control.push(this.initSubCategory());
    }

    deleteSubCategory(index: number) {
        debugger;
        const control = <FormArray>this.form.controls['questions'];
        control.removeAt(index);
        this.removedSubcategories.push(this.survey.survey_questions[index]);
        this.survey.questions.splice(index,1);
    }
    


    SearchData(input: any){
        // debugger;
    }

    onFormValuesChanged()
    {
        for ( const field in this.formErrors )
        {
            if ( !this.formErrors.hasOwnProperty(field) )
            {
                continue;
            }

            // Clear previous errors
            this.formErrors[field] = {};

            // Get the control
            const control = this.form.get(field);

            if ( control && control.dirty && !control.valid )
            {
                this.formErrors[field] = control.errors;
            }
        }
    }

    onSubmit() {
        debugger;
        let that = this;
        that.button_disabled = true;
        let formValues = this.form.value;
        if (this.form.valid) {
           
            if(this.survey.is_public == true){
                 this.isPublicTrue();
            }
            else{
               this.isPublicFalse();
            }
               
            
           
        }
        
    }
    isPublicTrue()
    {
        let that = this;
            this.survey.survey_questions.forEach(function(value) {
            if(value.id != null)
            {
                let obj = {
                    'question' : value.question,
                    'id' : value.id
                }
                that.questionsArray.push(obj);
            }
            else{
                let obj = value.question;
                that.questionsArray.push(obj);
            }
           
        });
            
        let payload = {
            'survey_name' : this.survey.survey_name,
            'description' : this.survey.description,
            'survey_type' : this.survey.survey_type,
            'is_public'    : this.survey.is_public,
            'survey_questions': this.questionsArray,
            'id' : this.survey.id
        }
        let Object = {
            'survey' : payload
        }
        debugger;
        this.EditSurveyService.updateSurvey(Object).then(response => {
            debugger;
            
            console.log("response: " + JSON.stringify(response));
            debugger;
            if(response.status !== "200"){
                this.snackBar.open("Something went wrong", "Error", {
                    duration: 2000,
                });
            }
            this.snackBar.open("Sucess", "Done", {
                duration: 2000,
            });
            
                this.redirect('pages/survey/list');
        });
    }
    isPublicFalse()
    {
        let that = this;
        this.survey.survey_questions.forEach(function(value) {
            if(value.id != null)
            {
                let obj = {
                    'question' : value.question,
                    'id' : value.id
                }
                that.questionsArray.push(obj);
            }
            else{
                let obj = value.question;
                that.questionsArray.push(obj);
            }
        });
        that.class_section.forEach(function(value) {
            debugger;
            let obj = {
                'class_section_id' : value
            }
            that.class_section_temp.push(obj);
        });
        let payload = {
            'survey_name' : this.survey.survey_name,
            'description' : this.survey.description,
            'survey_type' : this.survey.survey_type,
            'is_public'    : this.survey.is_public,
            'survey_questions': this.questionsArray,
            'class_section' : this.class_section_temp,
            'id' : this.survey.id
        }
        let Object = {
            'survey' : payload
        }
        this.EditSurveyService.updateSurvey(Object).then(response => {
            debugger;
            
            console.log("response: " + JSON.stringify(response));
            debugger;
            if(response.status !== "200"){
                this.snackBar.open("Something went wrong", "Error", {
                    duration: 2000,
                });
            }
            this.snackBar.open("Sucess", "Done", {
                duration: 2000,
            });
            
                this.redirect('pages/survey/list');
        });
    }

    redirect(pagename: string) {
        this.router.navigate([pagename]);
    }

    log(val: any) { console.log(val); }
    displaySelectedName(item: any) 
    {
      return item.school_class.class_name+"-"+item.section.section_name;
    }
}
