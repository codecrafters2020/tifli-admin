import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { AppService } from '../../../../../app.service';

@Injectable()
export class EditSurveyService implements Resolve<any>
{
    category:any;
    id:any;
    onUsersChanged: BehaviorSubject<any> = new BehaviorSubject({});
    onUserSelected: BehaviorSubject<any> = new BehaviorSubject({});
    classes : any[];
    constructor(private http: HttpClient, private appService: AppService)
    {
    }

    /**
     * The File Manager App Main Resolver
     * 
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {
        this.id=route.params['id'];
        return new Promise((resolve, reject) => {
            debugger;
            Promise.all([
             this.getSurveys(),
             this.getClasses()
           //  this.getCourses()
            ]).then(
                ([users]) => {
                    resolve();
                },
                reject);
        });
        // return new Promise((resolve, reject) => {
        //     debugger;
        //     Promise.all([
        //      this.getCategory()
        //     ]).then(
        //         ([users]) => {
        //             resolve();
        //         },
        //         reject);
        // });
    }

    getSurveys(): Promise<any>
    {
        let queryString = 'id=' + this.id ;


        return new Promise((resolve, reject) => {
            debugger;
            // this.http.get(this.appService.apiUrl + 'cms/api/merchants/categories/'+this.id)
            this.http.get(`${this.appService.adminService}survey/get_survey?${queryString}`) 
            .subscribe((response: any) => {
                    console.log(response);
                    debugger;
                    if(response)
                    {
                        // for(let i=0;i<response.data.length;i++){
                        //     if(response.data[i].id == this.id){
                        //         this.category=response.data[i];
                        //         break;
                        //     }

                        // }
                        this.category = response.survey;  
                    }
                    resolve(response);
                }, reject);
        });
    }

    updateSurvey(survey): Promise<any>
    {
        // debugger;
        return new Promise((resolve, reject) => {
            debugger;
            this.http.patch(`${this.appService.adminService}survey`, survey)
              .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }
    getClasses(): Promise<any> {
        return new Promise((resolve, reject) => {
          this.http.get(`${this.appService.adminService}faculties/show_all_classes`)
                .subscribe((response: any) => {
                    debugger;
                    this.classes = response.class_sections;
                    resolve(response);
                }, reject);
        });
    }

}