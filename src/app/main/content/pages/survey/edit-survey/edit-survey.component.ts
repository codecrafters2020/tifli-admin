import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';

import { EditSurveyService } from './edit-survey.service';

import { Router } from '@angular/router';

@Component({
    selector     : 'fuse-file-manager',
    templateUrl  : './edit-survey.component.html',
    styleUrls    : ['./edit-survey.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class EditSurveyComponent implements OnInit
{
    selected: any;
    pathArr: string[];

    constructor(private EditSurveyService: EditSurveyService, private router: Router)
    {
    }

    ngOnInit()
    {
        
    }

    openSurveyList(){
        
        this.router.navigateByUrl("pages/survey/list");
    }
}
