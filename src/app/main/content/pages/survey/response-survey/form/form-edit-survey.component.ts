import { Component, OnInit,ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, FormArray, Validators, EmailValidator } from '@angular/forms';
import { EditSurveyService } from '../edit-survey.service';
import { Router } from '@angular/router';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { startWith } from 'rxjs/operators/startWith';
import { map } from 'rxjs/operators/map';
import { MatSnackBar } from '@angular/material';
import {ChartsModule} from 'ng2-charts'

import {
    MatAutocomplete,
    MatAutocompleteSelectedEvent,
    MatAutocompleteTrigger,
    VERSION,
  } from '@angular/material';
import { Subscription } from 'rxjs/Subscription';

@Component({
    selector   : 'fuse-file-list',
    templateUrl: './form-edit-survey.component.html',
    styleUrls  : ['./form-edit-survey.component.scss'],
})
export class FormEditSurveyComponent implements OnInit
{
    public barChartOptions = {
        scaleShowVerticalLines : true,
         responsive : true,
        title: {
            display: true,
            text: 'Qno1'
        }
    };
    public barChartOptions1 = {
        scaleShowVerticalLines : true,
         responsive : true,
        title: {
            display: true,
            text: 'Qno2'
        }
    };
    
    public barChartOptions2 = {
        scaleShowVerticalLines : true,
         responsive : true,
        title: {
            display: true,
            text: 'Qno3'
        }
    };
    public barChartOptions3 = {
        scaleShowVerticalLines : true,
         responsive : true,
        title: {
            display: true,
            text: 'Qno4'
        }
    };
    public barChartOptions4 = {
        scaleShowVerticalLines : true,
         responsive : true,
        title: {
            display: true,
            text: 'Qno5'
        }
    };
    
    question1 = [0,0,0,0,0];
    question2 = [0,0,0,0,0];
    question3 = [0,0,0,0,0];
    question4= [0,0,0,0,0];
    question5 = [0,0,0,0,0];
    public barChartLabels = ["Rating 1","Rating 2","Rating 3","Rating 4","Rating 5"];
    public barChartType = "pie";
    public barChartLegend = true;
    public barChartData = [
       
        {data: this.question1}
    ];
    public barChartData1 = [
       
        {data: this.question2}
    ];
    public barChartData2 = [
       
        {data: this.question3}
    ];
    public barChartData3 = [
       
        {data: this.question4}
    ];
    public barChartData4= [
       
        {data: this.question5}
    ];
    
    public 
    survey: any = {};
    form: FormGroup;
    formErrors: any;
    test: any;
    visible: boolean = true;
    selectable: boolean = true;
    removable: boolean = true;
    addOnBlur: boolean = true;
    removedSubcategories: any[];
    questionsArray : any=[];
    surveyAnswers : any=[];
    questionsPresent : boolean = false;
    surveyID =[-1,-1,-1,-1,-1];
    surveyResponseQ1 : any=[];
    surveyResponseQ2 : any=[];
    surveyResponseQ3 : any=[];
    surveyResponseQ4 : any=[];
    surveyResponseQ5 : any=[];
    
    constructor(private formBuilder: FormBuilder, private EditSurveyService: EditSurveyService, private router: Router, private snackBar: MatSnackBar)
    {
       
        this.survey = EditSurveyService.category;
        let that = this;
        
       // let temp = this.survey.survey_questions_responses;
       if(this.survey.survey_question_responses)
       {
           that.questionsPresent = true;
       }
        //Inputs from dataset 
        for(let i=0; i<this.survey.survey_question_responses.length;i++){
            this.surveyAnswers[i] = this.survey.survey_question_responses[i].survey_answers;
            this.surveyID[i] = this.survey.survey_question_responses[i].survey_question.id;
        } 
      
     // work Below here is to populate arrays for pie-chart datasets
       for(let i = 0; i < 5; i++){
                if(i == 0){
                    if(this.surveyAnswers[i][1]){
                        that.question1[0] = this.surveyAnswers[i][1];}
                    if(this.surveyAnswers[i][2]){    
                        that.question1[1] = this.surveyAnswers[i][2];}
                    if(this.surveyAnswers[i][3]){    
                        that.question1[2] = this.surveyAnswers[i][3];}
                    if(this.surveyAnswers[i][4]){    
                        that.question1[3] = this.surveyAnswers[i][4];}
                    if(this.surveyAnswers[i][5]){    
                        that.question1[4] = this.surveyAnswers[i][5];}}

                if(i == 1){
                    if(this.surveyAnswers[i][1]){
                        that.question2[0] = this.surveyAnswers[i][1];}
                    if(this.surveyAnswers[i][2]){    
                            that.question2[1] = this.surveyAnswers[i][2];}
                    if(this.surveyAnswers[i][3]){    
                            that.question2[2] = this.surveyAnswers[i][3];}
                    if(this.surveyAnswers[i][4]){    
                            that.question2[3] = this.surveyAnswers[i][4];}
                    if(this.surveyAnswers[i][5]){    
                            that.question2[4] = this.surveyAnswers[i][5];}}
                if(i == 2){
                        if(this.surveyAnswers[i][1]){
                            that.question3[0] = this.surveyAnswers[i][1];}
                        if(this.surveyAnswers[i][2]){    
                            that.question3[1] = this.surveyAnswers[i][2];}
                        if(this.surveyAnswers[i][3]){    
                            that.question3[2] = this.surveyAnswers[i][3];}
                        if(this.surveyAnswers[i][4]){    
                            that.question3[3] = this.surveyAnswers[i][4];}
                        if(this.surveyAnswers[i][5]){    
                            that.question3[4] = this.surveyAnswers[i][5];}}
                if(i == 3){
                        if(this.surveyAnswers[i][1]){
                            that.question4[0] = this.surveyAnswers[i][1];}
                        if(this.surveyAnswers[i][2]){    
                                that.question4[1] = this.surveyAnswers[i][2];}
                        if(this.surveyAnswers[i][3]){    
                                that.question4[2] = this.surveyAnswers[i][3];}
                        if(this.surveyAnswers[i][4]){    
                                that.question4[3] = this.surveyAnswers[i][4];}
                        if(this.surveyAnswers[i][5]){    
                                that.question4[4] = this.surveyAnswers[i][5];}}
                if(i == 4){
                        if(this.surveyAnswers[i][1]){
                            that.question5[0] = this.surveyAnswers[i][1];}
                        if(this.surveyAnswers[i][2]){    
                            that.question5[1] = this.surveyAnswers[i][2];}
                        if(this.surveyAnswers[i][3]){    
                            that.question5[2] = this.surveyAnswers[i][3];}
                        if(this.surveyAnswers[i][4]){    
                            that.question5[3] = this.surveyAnswers[i][4];}
                        if(this.surveyAnswers[i][5]){    
                            that.question5[4] = this.surveyAnswers[i][5];}}}
       /*
        for(let i = 0; i < this.survey.survey_responses.length; i++){
            if(this.survey.survey_responses[i].survey_question.id == this.surveyID[0]){
                that.surveyResponseQ1.push(this.survey.survey_responses[i]);
            }
            else if(this.survey.survey_responses[i].survey_question.id == this.surveyID[1]){
                that.surveyResponseQ2.push(this.survey.survey_responses[i]);
            }
            else if(this.survey.survey_responses[i].survey_question.id == this.surveyID[2]){
                that.surveyResponseQ3.push(this.survey.survey_responses[i]);
            }
            else if(this.survey.survey_responses[i].survey_question.id == this.surveyID[3]){
                that.surveyResponseQ4.push(this.survey.survey_responses[i]);
            }
            else if(this.survey.survey_responses[i].survey_question.id == this.surveyID[4]){
                that.surveyResponseQ5.push(this.survey.survey_responses[i]);
            }

        }
        */
         // Reactive form errors
        this.formErrors = {
          
        };
    }
    onSubmit(){
        
    }
    ngOnInit()
    {
        // Reactive Form
        this.form = this.formBuilder.group({
            question1 : new FormArray([]),
            question2 : new FormArray([]),
            question3 : new FormArray([]),
            question4 : new FormArray([]),
            question5 : new FormArray([])

        });

        
        this.form.valueChanges.subscribe(() => {
            this.onFormValuesChanged();
        });
        debugger;
        if(this.surveyResponseQ1 != null){
            for(let i=0;i<this.surveyResponseQ1.length;i++){
                const control = <FormArray>this.form.controls['question1'];
                control.push(this.loadSubCategory(this.surveyResponseQ1[i]));
            }
        }
        if(this.surveyResponseQ2 != null){
            for(let i=0;i<this.surveyResponseQ2.length;i++){
                const control = <FormArray>this.form.controls['question2'];
                control.push(this.loadSubCategory(this.surveyResponseQ2[i]));
            }
        }
        if(this.surveyResponseQ3 != null){
            for(let i=0;i<this.surveyResponseQ3.length;i++){
                const control = <FormArray>this.form.controls['question3'];
                control.push(this.loadSubCategory(this.surveyResponseQ3[i]));
            }
        }
        if(this.surveyResponseQ4 != null){
            for(let i=0;i<this.surveyResponseQ4.length;i++){
                const control = <FormArray>this.form.controls['question4'];
                control.push(this.loadSubCategory(this.surveyResponseQ4[i]));
            }
        }
        if(this.surveyResponseQ5 != null){
            for(let i=0;i<this.surveyResponseQ5.length;i++){
                const control = <FormArray>this.form.controls['question5'];
                control.push(this.loadSubCategory(this.surveyResponseQ5[i]));
            }
        }

    }

    loadSubCategory(survey: any) {
        return this.formBuilder.group({
            parent_id : [survey],
            answer : [survey]
           
        });
    }

    initSubCategory() {
        return this.formBuilder.group({
            parent_id : [''],
            answer : ['']
             });
    }
    seeResponseList(event,userid){
        this.router.navigateByUrl("pages/survey/listOfResponse/" + userid);
    }

    
  
    


    SearchData(input: any){
        // debugger;
    }

    onFormValuesChanged()
    {
        for ( const field in this.formErrors )
        {
            if ( !this.formErrors.hasOwnProperty(field) )
            {
                continue;
            }

            // Clear previous errors
            this.formErrors[field] = {};

            // Get the control
            const control = this.form.get(field);

            if ( control && control.dirty && !control.valid )
            {
                this.formErrors[field] = control.errors;
            }
        }
    }

    redirect(pagename: string) {
        this.router.navigate([pagename]);
    }

    log(val: any) { console.log(val); }
    
}
