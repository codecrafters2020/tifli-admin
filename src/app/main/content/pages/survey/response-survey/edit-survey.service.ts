import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { AppService } from '../../../../../app.service';

@Injectable()
export class EditSurveyService implements Resolve<any>
{
    category:any;
    id:any;
    onUsersChanged: BehaviorSubject<any> = new BehaviorSubject({});
    onUserSelected: BehaviorSubject<any> = new BehaviorSubject({});
    classes : any[];
    constructor(private http: HttpClient, private appService: AppService)
    {
    }

    /**
     * The File Manager App Main Resolver
     * 
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {
        this.id=route.params['id'];
        return new Promise((resolve, reject) => {
           
            Promise.all([
             this.getSurveys()
           //  this.getCourses()
            ]).then(
                ([users]) => {
                    resolve();
                },
                reject);
        });
        // return new Promise((resolve, reject) => {
        //    
        //     Promise.all([
        //      this.getCategory()
        //     ]).then(
        //         ([users]) => {
        //             resolve();
        //         },
        //         reject);
        // });
    }

    getSurveys(): Promise<any>
    {
        let queryString = 'id=' + this.id ;


        return new Promise((resolve, reject) => {
         
            // this.http.get(this.appService.apiUrl + 'cms/api/merchants/categories/'+this.id)
            this.http.get(`${this.appService.adminService}survey/get_survey_results?${queryString}`) 
            .subscribe((response: any) => {
                    console.log(response);
                   
                    if(response)
                    {
                        // for(let i=0;i<response.data.length;i++){
                        //     if(response.data[i].id == this.id){
                        //         this.category=response.data[i];
                        //         break;
                        //     }

                        // }
                  
                        this.category = response;  
                    }
                    resolve(response);
                }, reject);
        });
    }

   

}