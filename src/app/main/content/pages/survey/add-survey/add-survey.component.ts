import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';

import { AddSurveyService } from './add-survey.service';

import { Router } from '@angular/router';

@Component({
    selector     : 'fuse-file-manager',
    templateUrl  : './add-survey.component.html',
    styleUrls    : ['./add-survey.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class AddSurveyComponent implements OnInit
{
    selected: any;
    pathArr: string[];

    constructor(private AddSurveyService: AddSurveyService, private router: Router)
    {
    }

    ngOnInit()
    {
        
    }

    openSurveyList(){
        
        this.router.navigateByUrl("pages/survey/list");
    }
}
