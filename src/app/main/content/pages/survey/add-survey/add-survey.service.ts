import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { AppService } from '../../../../../app.service';

@Injectable()
export class AddSurveyService implements Resolve<any>
{
    roles: any[];
    faculties: any[];
    classes: any[];
    constructor(private http: HttpClient, private appService: AppService)
    {
    }

    /**
     * The File Manager App Main Resolver
     * 
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {

        // return new Promise((resolve, reject) => {
        //     // debugger;
        //     resolve();
        // });
        return new Promise((resolve, reject) => {

            Promise.all([
                this.getClasses(),
                //this.getCourses()
            ]).then(
                ([faculty]) => {
                    resolve();
                },
                reject
                );
        });
    }

    addSurvey(survey): Promise<any>
    {
        // debugger;
        return new Promise((resolve, reject) => {
            this.http.post(`${this.appService.adminService}survey`, survey)
            .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }
    getClasses(): Promise<any> {
        return new Promise((resolve, reject) => {
          this.http.get(`${this.appService.adminService}faculties/show_all_classes`)
                .subscribe((response: any) => {
                    debugger;
                    this.classes = response.class_sections;
                    resolve(response);
                }, reject);
        });
    }

}