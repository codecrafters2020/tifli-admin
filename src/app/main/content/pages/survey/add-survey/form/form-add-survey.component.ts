import { Component, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, FormArray, Validators, EmailValidator } from '@angular/forms';
import { AddSurveyService } from '../add-survey.service';
import { Router } from '@angular/router';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { startWith } from 'rxjs/operators/startWith';
import { map } from 'rxjs/operators/map';
import { MatSnackBar } from '@angular/material';

import {
    MatAutocomplete,
    MatAutocompleteSelectedEvent,
    MatAutocompleteTrigger,
    VERSION,
} from '@angular/material';
import { Subscription } from 'rxjs/Subscription';

@Component({
    selector: 'fuse-file-list',
    templateUrl: './form-add-survey.component.html',
    styleUrls: ['./form-add-survey.component.scss'],
})
export class FormAddSurveyComponent implements OnInit {
    surveyStatus: boolean = true;
    form: FormGroup;
    formErrors: any;
    test=0;
    teachers: any[];
    button_disabled :boolean = false;
    visible: boolean = true;
    selectable: boolean = true;
    removable: boolean = true;
    addOnBlur: boolean = true;
    ClassSectionResult: any[];
    class_section : any=[];
    questionCount : number=0;
    surveyTypes = [
        { "name": "Survey1", "value": "1" },
        { "name": "Survey2", "value": "2" },
        { "name": "Survey3", "value": "3" }
    ];
    constructor(private formBuilder: FormBuilder, private AddSurveyService: AddSurveyService, private router: Router, private snackBar: MatSnackBar) {
        // Reactive form errors
        this.formErrors = {
            surveyName: {},
            description: {},
            fruitCtrl : {},
            type: {}
        };
        debugger;
        this.ClassSectionResult=this.AddSurveyService.classes;
      //  this.teachers=this.AddSurveyService.faculties;
    }

    ngOnInit() {
        // Reactive Form
        this.form = this.formBuilder.group({
            surveyName: ['', Validators.required],
            description: ['', Validators.required],
            status: [''],
            fruitCtrl: [''],
            type: ['', Validators.required],
            questions: new FormArray([])
        });

        const control = <FormArray>this.form.controls['questions']
        for (let i = 0; i < control.length; i++) {
            control[i]
                .subscribe(value => {
                    debugger;
                    let passwordConfirmValue = this.form.controls.passwordConfirm.value;
                    const control = this.form.controls.passwordConfirm;
                    if (value != passwordConfirmValue)
                        this.formErrors.passwordConfirm.passwordsNotMatch = true;
                    else {
                        this.form.controls['passwordConfirm'].setValue(value);
                    }
                });
        }

        this.form.valueChanges.subscribe(() => {

            this.onFormValuesChanged();
        });



    }

    initSubCategory() {
        return this.formBuilder.group({
            question: ['', Validators.required],
          //  description: ['', Validators.required],
           // status: [this.surveyStatus ? 'ACTIVE' : 'INACTIVE', Validators.required]
        });
    }

    addNewSubCategory() {
        ++this.questionCount;
        const control = <FormArray>this.form.controls['questions'];
        ++this.test;
        control.push(this.initSubCategory());

    }
    get DynamicFormControls() {

        return <FormArray>this.form.get('questions');
      }
    deleteSubCategory(index: number) {
        --this.questionCount;
        const control = <FormArray>this.form.controls['questions'];
        --this.test;
        control.removeAt(index);
    }



    SearchData(input: any) {
        // debugger;
    }

    onFormValuesChanged() {
        for (const field in this.formErrors) {
            if (!this.formErrors.hasOwnProperty(field)) {
                continue;
            }

            // Clear previous errors
            this.formErrors[field] = {};

            // Get the control
            const control = this.form.get(field);

            if (control && control.dirty && !control.valid) {
                this.formErrors[field] = control.errors;
            }
        }
    }

    onSubmit() {
        // debugger;
       this.button_disabled = true;
        let that = this;
       debugger;
  
       if(this.test > 0 && this.test <= 5 ){
            let formValues = this.form.value;
            if (this.form.valid) {
                if(this.surveyStatus == true){
                    debugger;
                    let temp1 = {
                        'survey_name': formValues.surveyName,
                        'description': formValues.description,
                        'is_public': true,
                        'survey_type': formValues.type,
                        'survey_questions': formValues.questions
                    }
                    let temp2 = formValues.questions
                    let payload = {
                            'survey' : temp1,
                            //'survey_questions' : temp2
                    }
                    let object = {
                        'object' : payload
                    }
                    this.AddSurveyService.addSurvey(payload).then(response => {
                        // debugger;
                        console.log("response: " + JSON.stringify(response));
                        debugger;
                        if(response.status !== "200"){
                            this.snackBar.open("Something went wrong", "Error", {
                                duration: 2000,
                            });
                        }
                        this.snackBar.open("Sucess", "Done", {
                            duration: 2000,
                        });
                        
                            this.redirect('pages/survey/list');
                    });
                }
                else{
                    debugger;
                    let temp3 = formValues.fruitCtrl;
                    temp3.forEach(function(value) {
                        debugger;
                        let obj = {
                            'class_section_id' : value
                        }
                        that.class_section.push(obj);
                    });
                 
                    let temp1 = {
                        'survey_name': formValues.surveyName,
                        'description': formValues.description,
                        'is_public': false,
                        'survey_type': formValues.type,
                        'survey_questions': formValues.questions,
                        'class_sections' : that.class_section
                 
                    }
                   // let temp2 = formValues.questions;
                       let payload = {
                            'survey' : temp1,
                           // 'survey_questions' : temp2,
                              }
                    this.AddSurveyService.addSurvey(payload).then(response => {
                        // debugger;
                        console.log("response: " + JSON.stringify(response));
                        debugger;
                        if(response.status !== "200"){
                            this.snackBar.open("Something went wrong", "Error", {
                                duration: 2000,
                            });
                        }
                        this.snackBar.open("Sucess", "Done", {
                            duration: 2000,
                        });
                        
                            this.redirect('pages/survey/list');
                    });
                }
                
            }
        }
        else if(this.test > 5){
            window.alert("Survey Should Not Contain More than 5 Questions");
        }
        else {
            window.alert("Survey Should Contain Questions");
        }
        

    }

    redirect(pagename: string) {
        this.router.navigate([pagename]);
    }

    log(val: any) { console.log(val); }
    displaySelectedName(item: any) 
    {
      return item.school_class.class_name+"-"+item.section.section_name;
    }
}
