import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CdkTableModule } from '@angular/cdk/table';

import { MatFormFieldModule, MatInputModule, MatSelectModule, MatStepperModule, MatButtonModule, MatIconModule, MatRippleModule, MatSidenavModule, MatSlideToggleModule, MatTableModule } from '@angular/material';
import {MatChipsModule} from '@angular/material/chips';
import {MatAutocompleteModule} from '@angular/material/autocomplete';

import { FuseSharedModule } from '@fuse/shared.module';

import { AddSurveyComponent } from './add-survey.component';
import { AddSurveyService } from './add-survey.service';
import { FormAddSurveyComponent } from './form/form-add-survey.component';

const routes: Routes = [
    {
        path     : '**',
        component: AddSurveyComponent,
        children : [],
        resolve  : {
            files: AddSurveyService
        }
    }
];

@NgModule({
    declarations: [
        AddSurveyComponent,
        FormAddSurveyComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        CdkTableModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatButtonModule,
        MatIconModule,
        MatRippleModule,
        MatSlideToggleModule,
        MatSidenavModule,
        MatTableModule,
        MatChipsModule,
        MatAutocompleteModule,

        FuseSharedModule
    ],
    providers   : [
        AddSurveyService
    ]
})
export class AddSurveyModule
{
}
