import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FuseAngularMaterialModule } from '../../components/angular-material/angular-material.module';

import { FuseSharedModule } from '@fuse/shared.module';

const routes = [
    {
        path        : 'list',
        loadChildren: './list-courses/list-courses.module#ListCoursesModule'
    },
    {
        path        : 'add',
        loadChildren: './add-courses/add-courses.module#AddCoursesModule'
    },
    {
        path        : 'edit/:id',
        loadChildren: './edit-courses/edit-courses.module#EditCoursesModule'
    }
];

@NgModule({
    imports     : [
        FuseSharedModule,
        RouterModule.forChild(routes),
        FuseAngularMaterialModule
    ],
    declarations: []
})
export class CoursesModule
{
}
