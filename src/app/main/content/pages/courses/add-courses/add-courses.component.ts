import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';

import { AddCoursesService } from './add-courses.service';

import { Router } from '@angular/router';

@Component({
    selector     : 'fuse-file-manager',
    templateUrl  : './add-courses.component.html',
    styleUrls    : ['./add-courses.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class AddCoursesComponent implements OnInit
{
    selected: any;
    pathArr: string[];

    constructor(private AddCoursesService: AddCoursesService, private router: Router)
    {
    }

    ngOnInit()
    {
        
    }

    openCoursesList(){
        
        this.router.navigateByUrl("pages/courses/list");
    }
}
