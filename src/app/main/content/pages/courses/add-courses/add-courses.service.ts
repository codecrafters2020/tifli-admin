import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { AppService } from '../../../../../app.service';

@Injectable()
export class AddCoursesService implements Resolve<any>
{
    roles: any[];
    faculties: any[];
    constructor(private http: HttpClient, private appService: AppService)
    {
    }

    /**
     * The File Manager App Main Resolver
     * 
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {

           // return new Promise((resolve, reject) => {
        //     // debugger;
        //     resolve();
        // });
       /*/
        return new Promise((resolve, reject) => {

            Promise.all([
               // this.getFaculty(),
                //this.getclass()
            ]).then(
                ([faculty]) => {
                    resolve();
                },
                reject
                );
        });
        */
    }

    addCourses(courses): Promise<any>
    {
        // debugger;
        return new Promise((resolve, reject) => {
            this.http.post(`${this.appService.adminService}courses`, courses)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }
    

}