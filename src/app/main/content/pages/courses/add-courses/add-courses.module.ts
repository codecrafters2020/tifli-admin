import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CdkTableModule } from '@angular/cdk/table';

import { MatFormFieldModule, MatInputModule, MatSelectModule, MatStepperModule, MatButtonModule, MatIconModule, MatRippleModule, MatSidenavModule, MatSlideToggleModule, MatTableModule } from '@angular/material';
import {MatChipsModule} from '@angular/material/chips';
import {MatAutocompleteModule} from '@angular/material/autocomplete';

import { FuseSharedModule } from '@fuse/shared.module';

import { AddCoursesComponent } from './add-courses.component';
import { AddCoursesService } from './add-courses.service';
import {FormAddCoursesComponent} from './form/form-add-courses.component';

const routes: Routes = [
    {
        path     : '**',
        component: AddCoursesComponent,
        children : [],
        resolve  : {
            files: AddCoursesService
        }
    }
];

@NgModule({
    declarations: [
        AddCoursesComponent,
        FormAddCoursesComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        CdkTableModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatButtonModule,
        MatIconModule,
        MatRippleModule,
        MatSlideToggleModule,
        MatSidenavModule,
        MatTableModule,
        MatChipsModule,
        MatAutocompleteModule,

        FuseSharedModule
    ],
    providers   : [
        AddCoursesService
    ]
})
export class AddCoursesModule
{
}
