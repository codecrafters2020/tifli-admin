import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, FormArray, Validators, EmailValidator } from '@angular/forms';
import { fuseAnimations } from '@fuse/animations';

import { ListNoticeService } from './list-notice.service';
import { Router } from '@angular/router';

@Component({
    selector: 'fuse-file-manager',
    templateUrl: './list-notice.component.html',
    styleUrls: ['./list-notice.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class ListNoticeComponent implements OnInit {
    selected: any;
    pathArr: string[];
    isSearch: boolean = false;
    form: FormGroup;
    merchantSearchResult: any[];


    constructor(private ListNoticeService: ListNoticeService, private router: Router, private formBuilder: FormBuilder) {
      //  this.ListCouponService.getCoupons();
        this.form = this.formBuilder.group({
            merchantName : ['', ]})

    }

    ngOnInit() {
        this.ListNoticeService.onFileSelected.subscribe(selected => {
            this.selected = selected;
            // this.pathArr = selected.location.split('>');
        });

    }

    openCouponAdd() {
        this.router.navigateByUrl("pages/notice/add");

    }

    resetGrid() {
        this.ListNoticeService.getNotice();
    }
    
    searching() {

        if(this.isSearch == false){
            this.isSearch = true;
        }

        else{
            this.isSearch = false;
            console.log("close it")
        }
    }
}
