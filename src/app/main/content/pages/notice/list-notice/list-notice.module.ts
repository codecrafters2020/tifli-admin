import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CdkTableModule } from '@angular/cdk/table';

import { MatAutocompleteModule, MatFormFieldModule, MatInputModule, MatSelectModule, MatStepperModule, MatButtonModule, MatIconModule, MatRippleModule, MatSidenavModule, MatSlideToggleModule, MatTableModule, MatMenuModule, MatPaginatorModule, MatSortModule } from '@angular/material';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatRadioModule} from '@angular/material/radio';
import { FuseSharedModule } from '@fuse/shared.module';

import { ListNoticeComponent } from './list-notice.component';
import { ListNoticeService } from './list-notice.service';
import { GridNoticeComponent } from './grid/grid-notice.component';
import { SidenavNoticeMainComponent } from './sidenavs/main/sidenav-notice-main.component';
import { SidenavNoticeDetailsComponent } from './sidenavs/details/sidenav-notice-details.component';

const routes: Routes = [
    {
        path     : '**',
        component: ListNoticeComponent,
        children : [],
        resolve  : {
            files: ListNoticeService
        }
    }
];

@NgModule({
    declarations: [
        ListNoticeComponent,
        GridNoticeComponent,
        SidenavNoticeMainComponent,
        SidenavNoticeDetailsComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        CdkTableModule,
        MatAutocompleteModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatButtonModule,
        MatIconModule,
        MatRippleModule,
        MatSlideToggleModule,
        MatSidenavModule,
        MatTableModule,
        MatMenuModule,
        MatPaginatorModule, 
        MatSortModule,
        MatProgressSpinnerModule,

        FuseSharedModule,
        MatDatepickerModule,
        MatCheckboxModule,
        MatRadioModule
    ],
    providers   : [
        ListNoticeService
    ]
})
export class ListNoticeModule
{
}
