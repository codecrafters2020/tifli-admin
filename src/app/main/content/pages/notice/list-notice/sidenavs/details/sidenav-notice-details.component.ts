import { Component, OnInit } from '@angular/core';

import { fuseAnimations } from '@fuse/animations/index';

import { ListNoticeService } from '../../list-notice.service';

@Component({
    selector   : 'fuse-file-manager-details-sidenav',
    templateUrl: './sidenav-notice-details.component.html',
    styleUrls  : ['./sidenav-notice-details.component.scss'],
    animations : fuseAnimations
})
export class SidenavNoticeDetailsComponent implements OnInit
{

    selected: any;

    constructor(private ListNoticeService: ListNoticeService)
    {

    }

    ngOnInit()
    {
        this.ListNoticeService.onFileSelected.subscribe(selected => {
            this.selected = selected;
        });
    }

}
