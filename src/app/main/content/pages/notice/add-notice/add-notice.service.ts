import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { AppService } from '../../../../../app.service';
import { AbstractControl } from '@angular/forms';
import { HttpHeaders, } from '@angular/common/http';
import { Headers } from '@angular/http';
import { RequestOptions } from '@angular/http';
import {AuthService} from '../../../../../auth/auth.service'
import {InterceptorSkipHeader} from '../../../../../auth/jwt.interceptor';
import { of } from 'rxjs/observable/of'


@Injectable()
export class AddNoticeService implements Resolve<any>
{

    categoriesList: any[];
    usersList: any[];
    barcodesList: any[];
    affiliateNetworksList: any[];
    merchantAffiliateStatusList: any[];
    merchantsList: any[];
    merchantFound: boolean = false;
    merchantTierList: any[];
    classes: any;
    headers: any;
    students: any;
    constructor(private http: HttpClient, private appService: AppService, public globalVar: AuthService) {
    }

    /**
     * The File Manager App Main Resolver
     * 
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return new Promise((resolve, reject) => {

            Promise.all([
                this.getClasses(),
                this.getStudents()
                //this.getCourses()
            ]).then(
                ([faculty]) => {
                    resolve(Response);
                },
                reject
                );
        });
      

    }
    getSignedUploadRequest(name,ext, type) {
        this.setHeader()
        // return this.http.get(`${this.apiUrl}/companies/presign_upload?filename=${name}&filetype=${type}&contentType=jpeg`, {headers: this.headers});

        return this.http.get(`${this.appService.uploadImageService}/signed_urls.json?filename=${name}&filetype=${type}&contentType=jpg`,
        {headers: this.headers});


      }
      putFileToS3(body: File, presignedUrl: string){
        type bodyType = 'body';

        // upload file to the pre-signed url
        const httpOptions = {
          headers: new HttpHeaders({
            'Content-Type': 'image/jpeg'
              }),
         observe: <bodyType>'response'
        };
        const headers = new HttpHeaders().set(InterceptorSkipHeader, '');

        return this.http.put(presignedUrl, body, { headers });
        }

        uploadFile(url, file) {

          return this.http.put(url, file);
      }
      setHeader(){
        this.headers = new HttpHeaders({
           Authorization: this.globalVar.user.accessToken|| '',
           "Access-Control-Allow-Origin": "*"

        });
      }


    addNotice(SmsUser): Promise<any> {
        return new Promise((resolve, reject) => {
            debugger;
            this.http.post(`${this.appService.adminService}notices`, SmsUser)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }
    getClasses(): Promise<any> {
        return new Promise((resolve, reject) => {
          this.http.get(`${this.appService.adminService}faculties/show_all_classes`)
                .subscribe((response: any) => {
                    debugger;
                    this.classes = response.class_sections;
                    resolve(response);
                }, reject);
        });
    }
    getStudents(): Promise<any> {
        return new Promise((resolve, reject) => {
          this.http.get(`${this.appService.adminService}students/show_all_students`)
            .subscribe((response: any) => {
              // debugger;
              this.students = response.students;
              resolve(response);
            }, reject);
        }).catch(err => this.handleError(err, () =>  this.http.get(`${this.appService.adminService}students/show_all_students`),1));
      }
      protected handleError(error, continuation: () => Observable<any> ,object) {
        let that = this;
        if (error.status == 404 || error.status == 400) {
            if(object==1){
              that.students = [];
            }
            else if (object == 2){
              that.classes = [];
            }
          return of(false);
        }
     
    }



}
