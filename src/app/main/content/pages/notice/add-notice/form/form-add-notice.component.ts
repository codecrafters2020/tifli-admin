import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators, FormsModule, FormControl, RequiredValidator } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { AddNoticeService } from '../add-notice.service';
import { Router } from '@angular/router';
import { FancyImageUploaderOptions, UploadedFile } from 'ng2-fancy-image-uploader';
import { getLocaleDateTimeFormat } from '@angular/common';
import { AppService } from '../../../../../../app.service';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { ChangeEvent } from '@ckeditor/ckeditor5-angular/ckeditor.component';
@Component({
    selector: 'fuse-file-list',
    templateUrl: './form-add-notice.component.html',
    styleUrls: ['./form-add-notice.component.scss'],
})
export class FormAddNoticeComponent implements OnInit {
  public Editor = ClassicEditor;    
    //form: FormGroup;
    formErrors: any;
    test: any;
    step: number;
    imagePreview : string;
    avatar: any;
    students: any;
    classSections: any;
    id :0; 
    showLoadingBar: boolean = false;
    photoUpload: boolean = false;
    info:any;
    selectedPrivacy :number = 1;
    privacy2 :number = 2;
    privacy3 :number = 3;
    button_disabled :boolean = false;
    public onChangeR( { editor }: ChangeEvent ) {
      this.info = editor.getData();

      
  }
    privacies: any[] = [
      {value: 1, viewValue: 'Public'},
      {value: 2, viewValue: 'Private'},
      {value: 3, viewValue: 'Class-Section'}
    ];
   
    // Horizontal Stepper
    horizontalStepperStep1: FormGroup;
    horizontalStepperStep2: FormGroup;
   

    horizontalStepperStep1Errors: any;
    horizontalStepperStep2Errors: any;
    surveyStatus: boolean = true;
   
    
    subject: any;
    imageLink: string;
    //AWS
    policy: string;
    s3signature : string;
    file : File;

    displayedColumns = ['enable', 'name', 'description'];
    displayedColumnsPanelHeading = ['CategoryName', 'Description', 'isChecked'];
    ClassSectionResult: any;
    notice: any;
    class_section: any=[];
    class_section_temp: any=[];

    
   
    

    constructor(private AddNoticeService: AddNoticeService, private formBuilder: FormBuilder, private router: Router, private snackBar: MatSnackBar, private appService: AppService) {

        this.ClassSectionResult=this.AddNoticeService.classes;
        this.students=this.AddNoticeService.students;
        this.horizontalStepperStep1Errors = {
            subject: {},
            information: {}
           
        };

    }
    handleResponse(response){
        this.policy = response.policy;
        this.s3signature = response.signature;
    }
    onChange(){
     console.log(this.selectedPrivacy);
    }
    onChangePrivacy(){
        this.id = null;
    }
    ngOnInit() {

        // Horizontal Stepper form steps
        this.horizontalStepperStep1 = this.formBuilder.group({

            subject: ['',Validators.required],
            PrivacySelector: [''],
            StudentSelector: [''],
            ClassSelector: [''],
            information: ['',Validators.required]
       });

        this.horizontalStepperStep2 = this.formBuilder.group({
            image : new FormControl(null, {})
            // merchantLargeLogo: ['', Validators.required],
            // merchantSmallLogo: ['', Validators.required]
        });

      

      



        this.horizontalStepperStep1.valueChanges.subscribe(() => {
            this.onFormValuesChanged(this.horizontalStepperStep1, this.horizontalStepperStep1Errors);
        });

        this.horizontalStepperStep2.valueChanges.subscribe(() => {
            this.onFormValuesChanged(this.horizontalStepperStep2, this.horizontalStepperStep2Errors);
        });

    

    }
    randomString = function (length) {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (var i = 0; i < length; i++) {
          text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return text;
      }
     uploadToS3(event){
        this.showLoadingBar = true;
        this.photoUpload = true;
        if (event.target.files){
          var reader = new FileReader();
          var blob;
          reader.readAsDataURL(event.target.files[0]);
          // tslint:disable-next-line:no-shadowed-variable
          const file = (event.target as HTMLInputElement).files[0];
          console.log('file',file);
          reader.onload = (event: any ) => {
            blob = event.target.result;
            this.imagePreview = event.target.result;
            // const file = (event.target as HTMLInputElement).files[0];
            this.horizontalStepperStep2.patchValue({image : blob});
            this.horizontalStepperStep2.get('image').updateValueAndValidity();
            let ext = 'jpg';
            let type = 'image/jpeg';
            let newName = this.randomString(6) + new Date().getTime() + '.' + ext;
            let that = this;
            // method 2

            // method 2
            this.AddNoticeService.getSignedUploadRequest(newName, ext,type).subscribe(data => {
              console.log('Success in getSignedUploadRequest',data);
              that.avatar =  data['public_url'];
              console.log('i am in public url',data['public_url']);
              console.log('b64',blob);
              console.log(this.horizontalStepperStep2);
              let form = this.horizontalStepperStep2.value;
              blob =blob.replace(/^data:image\/\w+;base64,/, '');
              console.log('blob',blob);
              blob = this.base64toBlob(blob, 'jpg');
              this.imageLink = data['public_url'];
              debugger;
              console.log('blob',blob);
              this.AddNoticeService.putFileToS3(blob, data["presigned_url"])
              .subscribe(
                 
                response => {
                this.showLoadingBar = false;
                this.photoUpload = false;
                }
                );

            }, (err) => {

              console.log('getSignedUploadRequest timeout error: ', err);
            });

          };
        }
      }
      base64toBlob(base64Data, contentType) {
        contentType = contentType || '';
        var sliceSize = 1024;
        var byteCharacters = atob(base64Data);
        var bytesLength = byteCharacters.length;
        var slicesCount = Math.ceil(bytesLength / sliceSize);
        var byteArrays = new Array(slicesCount);
    
        for (var sliceIndex = 0; sliceIndex < slicesCount; ++sliceIndex) {
          var begin = sliceIndex * sliceSize;
          var end = Math.min(begin + sliceSize, bytesLength);
    
          var bytes = new Array(end - begin);
          for (var offset = begin, i = 0; offset < end; ++i, ++offset) {
            bytes[i] = byteCharacters[offset].charCodeAt(0);
          }
          byteArrays[sliceIndex] = new Uint8Array(bytes);
        }
        return new Blob(byteArrays, { type: contentType });
      }


   
    onFormValuesChanged(horizontalStepperStep: FormGroup, horizontalStepperStepErrors: any) {
        for (const field in horizontalStepperStepErrors) {
            if (!horizontalStepperStepErrors.hasOwnProperty(field)) {
                continue;
            }
            horizontalStepperStepErrors[field] = {};

            const control = horizontalStepperStep.get(field);

            if (control && control.dirty && control.touched && !control.valid) {
                horizontalStepperStepErrors[field] = control.errors;
            }
        }
    }
    Upload(){
        
    }

    finishHorizontalStepper() {
        // alert('You have finished the horizontal stepper!');
        debugger;
        let that = this;
        that.button_disabled = true;
       if(this.selectedPrivacy==1){
        try{
            let object = {
                'subject' : that.subject,
                'notice_info' : that.info,
                'is_public' : true,
                'image_url' : that.imageLink
            }

            that.AddNoticeService.addNotice(object).then(response => {
                debugger;
                this.test = response;
                console.log("response: " + JSON.stringify(response));
                debugger;
                if(response.status !== "200"){
                    that.snackBar.open("Something went wrong", "Error", {
                        duration: 2000,
                    });
                }
                that.snackBar.open("Sucess", "Done", {
                    duration: 2000,
                });
                
                that.redirect('pages/notice/list');
            });
            }
            catch(err){
                debugger;
            }
       }
       else if(this.selectedPrivacy==2){
        try{
          let object = {
              'subject' : that.subject,
              'notice_info' : that.info,
              'is_public' : false,
              'image_url' : that.imageLink,
              'student_id': that.id
          }
          debugger;
          that.AddNoticeService.addNotice(object).then(response => {
              debugger;
              this.test = response;
              console.log("response: " + JSON.stringify(response));
              debugger;
              if(response.status !== "200"){
                  that.snackBar.open("Something went wrong", "Error", {
                      duration: 2000,
                  });
              }
              that.snackBar.open("Sucess", "Done", {
                  duration: 2000,
              });
              
              that.redirect('pages/notice/list');
          });
          }
          catch(err){
              debugger;
          }

       }
       else if(this.selectedPrivacy==3){
        try{
          let object = {
              'subject' : that.subject,
              'notice_info' : that.info,
              'is_public' : false,
              'image_url' : that.imageLink,
              'class_section_id' : that.id
          }
          debugger;
          that.AddNoticeService.addNotice(object).then(response => {
              debugger;
              this.test = response;
              console.log("response: " + JSON.stringify(response));
              debugger;
              if(response.status !== "200"){
                  that.snackBar.open("Something went wrong", "Error", {
                      duration: 2000,
                  });
              }
              that.snackBar.open("Sucess", "Done", {
                  duration: 2000,
              });
              
              that.redirect('pages/notice/list');
          });
          }
          catch(err){
              debugger;
          }

       }
       
        
    }

    redirect(pagename: string) {
        this.router.navigate([pagename]);
    }

    onUploadLargeImage(file: UploadedFile) {
        debugger;
        var response: any = {};
        response = JSON.parse(file.response)
        if (response.meta.code == 200) {
            if (this.notice.images.length >= 1) {
                for (let i = 0; i < this.notice.images.length; i++) {
                    if(this.notice.images[i].suffix=="large")
                    {
                        this.notice.images.splice(i, 1);
                    }
                }
            }
            var largeImage: any = {};
            largeImage.fileName = response.data.fileName;
            largeImage.suffix = "large";
            this.notice.images.push(largeImage);
        }
    }

    displaySelectedName(item: any) 
    {
      return item.school_class.class_name+"-"+item.section.section_name;
    }
}
