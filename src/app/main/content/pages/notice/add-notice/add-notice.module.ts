import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CdkTableModule } from '@angular/cdk/table';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import { MatFormFieldModule, MatInputModule, MatSelectModule, MatStepperModule, MatButtonModule, MatIconModule, MatRippleModule, MatSidenavModule, MatSlideToggleModule, MatTableModule, MatCheckboxModule} from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';

import { AddNoticeComponent } from './add-notice.component';
import { AddNoticeService } from './add-notice.service';
import { FormAddNoticeComponent } from './form/form-add-notice.component';
import { FancyImageUploaderModule } from 'ng2-fancy-image-uploader';
import {MatExpansionModule} from '@angular/material/expansion';
//import { QuillModule } from 'ngx-quill'
// import { FateModule } from 'fate-editor';
// import { FateMaterialModule } from 'fate-editor';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';

const routes: Routes = [
    {
        path     : '**',
        component:AddNoticeComponent,
        children : [],
        resolve  : {
            merchantById : AddNoticeService
        }
    }
];

@NgModule({
    declarations: [
        AddNoticeComponent,
        FormAddNoticeComponent,
    ],
    imports     : [
        RouterModule.forChild(routes),
        CdkTableModule,
        CKEditorModule,
        MatProgressBarModule,
        // FateModule,
        // FateMaterialModule,
         MatFormFieldModule,
        MatExpansionModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatButtonModule,
        MatIconModule,
        MatRippleModule,
        MatSlideToggleModule,
        MatSidenavModule,
        MatTableModule,
        FancyImageUploaderModule,
        MatCheckboxModule,

        FuseSharedModule
    ],
    providers   : [
        AddNoticeService
    ]
})
export class AddNoticeModule
{
}
