import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';

import { AddNoticeService } from './add-notice.service';

import { Router } from '@angular/router';

@Component({
    selector     : 'fuse-file-manager',
    templateUrl  : './add-notice.component.html',
    styleUrls    : ['./add-notice.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class AddNoticeComponent implements OnInit
{
    selected: any;
    pathArr: string[];
    

    constructor(private AddNoticeService: AddNoticeService, private router: Router)
    {
    }

    ngOnInit()
    {
        // this.AddMerchantService.onFileSelected.subscribe(selected => {
        //     this.selected = selected;
        //     this.pathArr = selected.location.split('>');
        // });
    }

    openNoticeList(){
        
        this.router.navigateByUrl("pages/notice/list/");
    }

    test=(event)=>{
        console.log(event.keyCode);
      }
    
      onSelectionChanged = (event) =>{
        if(event.oldRange == null){
          this.onFocus();
        }
        if(event.range == null){
          this.onBlur();
        }
      }
    
      onContentChanged = (event) =>{
        //console.log(event.html);
      }
    
      onFocus = () =>{
        console.log("On Focus");
      }
      onBlur = () =>{
        console.log("Blurred");
      }
}
