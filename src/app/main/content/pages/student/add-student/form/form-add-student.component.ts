/* tslint:disable:triple-equals prefer-const quotemark */
import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators, FormsModule, FormControl, RequiredValidator } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { FancyImageUploaderOptions, UploadedFile } from 'ng2-fancy-image-uploader';
import { getLocaleDateTimeFormat } from '@angular/common';
import { AppService } from '../../../../../../app.service';
import { AddStudentService } from '../add-student.service';
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';

// Depending on whether rollup is used, moment needs to be imported differently.
// Since Moment.js doesn't have a default export, we normally need to import using the `* as`
// syntax. However, rollup creates a synthetic default module and we thus need to import it using
// the `default as` syntax.
import * as _moment from 'moment';
// tslint:disable-next-line:no-duplicate-imports
import {default as _rollupMoment} from 'moment';


@Component({
    selector: 'fuse-file-list',
    templateUrl: './form-add-student.component.html',
    styleUrls: ['./form-add-student.component.scss'],
   
})

export class FormAddstudentComponent implements OnInit {

    // form: FormGroup;
    formErrors: any;
    dob : any;
    test: any;
    step: number;
    classSectionResult: any[];
    parents: any[];
    parentExist: boolean = false;
    imagePreview: string;
    showLoadingBar: boolean = false;
    photoUpload : boolean = false;
    // Horizontal Stepper
    horizontalStepperStep1: FormGroup;
    horizontalStepperStep2: FormGroup;
    horizontalStepperStep3: FormGroup;
    horizontalStepperStep4: FormGroup;
    horizontalStepperStep5: FormGroup;
    horizontalStepperStep6: FormGroup;
    horizontalStepperStep1Errors: any;
    horizontalStepperStep2Errors: any;
    horizontalStepperStep3Errors: any;
    horizontalStepperStep4Errors: any;
    horizontalStepperStep5Errors: any;
    horizontalStepperStep6Errors: any;
    button_disabled :boolean = false;
    default_option = {
        id: '-1',
        name: 'None'
    };

    default_affiliate = {
        id: 1,
        name: 'Active'
    };

    default_choice = {id: '-1', first_name: 'None' , user_name: 'none'};
     student = {
      first_name: '',
      last_name: '',
      gender: "1",
      mobile: '',
      email: '',
      enrollment_number: '',
      date_of_birth:'',
      address: '',
      country: 'Pakistan',
      class_section_id: '',
      image_url: '',
      current_monthly_fee:0 
    };
    parent = {
      first_name: '',
      last_name: '',
      gender: "1",
      mobile: '',
      email: '',
      password: 'parent123',
      role: 4,
      // address: ''
    };


    displayedColumns = ['enable', 'name', 'description'];
    displayedColumnsPanelHeading = ['CategoryName', 'Description', 'isChecked'];


    countries = [
        { 'name': 'United States', 'code': 'US' },
        { 'name': 'Pakistan', 'code': 'PK' }
    ];

    genders = [
      { 'name': 'Male', 'code': 'M' , 'value': "M"},
      { 'name': 'Female', 'code': 'F' , 'value': "F"}
    ];


    categoriesList: any[] = [];
    usersList: any[];
    barcodesList: any[] = [];
    affiliateNetworksList: any[];
    studentAffiliateStatusList: any[];
    studentTierList: any[];
    studentList: any[];
    studentName = '';
    lastBarCode: any;
    lastMasterStore: any;
    lastStudentTier: any;
    lastAccountManager: any;
    lastManager: any;
    parentId: 0;

   


    categoriesGroups: any[];
    subCategoriesGroups: any[] = [];
    catControls: any[] = [];
    avatar: any;

  // tslint:disable-next-line:no-shadowed-variable
    constructor(private AddStudentService: AddStudentService,
                private formBuilder: FormBuilder, private router: Router, private snackBar: MatSnackBar, private appService: AppService) {

      this.classSectionResult = this.AddStudentService.classes;
      this.parents = this.AddStudentService.parents;
       
        // Horizontal Stepper form error
        this.horizontalStepperStep1Errors = {
          first_name: {},
          last_name: {},
          class_section_id: {},
          gender: {},
          mobile: {},
          enrollment_number: {},
          date_of_birth: {},
          address: {},
          // searchTerms: {},
          // barcodeFormat: {}
        };

        // this.horizontalStepperStep2Errors = {
        //     merchantLargeLogo  : {},
        //     merchantSmallLogo  : {}
        // };

        this.horizontalStepperStep3Errors = {
          first_name: {},
          last_name: {},
          gender: {},
          mobile: {},
          password: {},
          parentEmail: {}
          // address: {},
        };

    }

    ngOnInit() {

        // Horizontal Stepper form steps
        this.horizontalStepperStep1 = this.formBuilder.group({

            first_name: ['', [Validators.required]],
            last_name: ['', [Validators.required]],
            class_section_id: ['', [Validators.required]],
            gender: ['', [Validators.required]],
            mobile: [''],
            enrollment_number: ['', [Validators.required]],
            date_of_birth: ['', [Validators.required]],
            address: ['', [Validators.required]],
            country: [''],
            
            email: ['',[Validators.email]],
            fees: ['',[Validators.required]],
            image_url: ['']
        });

        this.horizontalStepperStep2 = this.formBuilder.group({
            image : new FormControl(null, {})
            // merchantLargeLogo: ['', Validators.required],
            // merchantSmallLogo: ['', Validators.required]
        });

        this.horizontalStepperStep3 = this.formBuilder.group({

          first_name: ['', [Validators.required]],
          last_name: ['', [Validators.required]],
          gender: ['', [Validators.required]],
          mobile: ['', [Validators.required]],
          status: [''],
          fruitCtrl: [''],
          // address: ['', [Validators.required]],
          parentEmail: ['',[Validators.required,Validators.email]]
        });

        this.horizontalStepperStep1.controls.first_name.valueChanges.subscribe(() => {
            let normalized_name = this.student.first_name.toLowerCase() + '' + this.student.last_name.toLowerCase();
            normalized_name = normalized_name.trim();
            normalized_name = normalized_name.replace("'", "");
            let normalized_name_arr = normalized_name.split(' ');
            normalized_name = '';
            normalized_name_arr.forEach(word => {
                if (normalized_name == ''){
                    normalized_name = word;
                }
                else{
                  normalized_name = normalized_name + '-' + word;
                }
            });
            // this.student.normalizedUrlName = normalized_name;
            // this.student.ciUrl="coupons.com/coupon-codes/"+ this.student.normalizedUrlName;
            // alert(normalized_name);
        });


        this.horizontalStepperStep1.valueChanges.subscribe(() => {
            this.onFormValuesChanged(this.horizontalStepperStep1, this.horizontalStepperStep1Errors);
        });

        this.horizontalStepperStep2.valueChanges.subscribe(() => {
            this.onFormValuesChanged(this.horizontalStepperStep2, this.horizontalStepperStep2Errors);
        });

        this.horizontalStepperStep3.valueChanges.subscribe(() => {
            this.onFormValuesChanged(this.horizontalStepperStep3, this.horizontalStepperStep3Errors);
        });

        // this.horizontalStepperStep1.controls.isActive.valueChanges.subscribe(() => {
        //     if (this.horizontalStepperStep1.controls.isActive.value == false) {
        //         this.horizontalStepperStep1.controls.monetized.setValue(false);
        //         this.horizontalStepperStep1.controls.featured.setValue(false);
        //     }
        //
        // });

        // this.horizontalStepperStep1.controls.monetized.valueChanges.subscribe(() => {
        //     if (this.horizontalStepperStep1.controls.monetized.value == true) {
        //         if (this.horizontalStepperStep1.controls.isActive.value == false) {
        //             this.horizontalStepperStep1.controls.monetized.setValue(false);
        //         }
        //     }
        //
        // });

        // this.horizontalStepperStep1.controls.featured.valueChanges.subscribe(() => {
        //     if (this.horizontalStepperStep1.controls.featured.value == true) {
        //         if (this.horizontalStepperStep1.controls.isActive.value == false) {
        //             this.horizontalStepperStep1.controls.featured.setValue(false);
        //         }
        //     }
        //
        // });

    }
    onChange(){
      debugger;
      const selectedParentArray = this.parents.filter(parent => parent.id == this.parentId);
      this.parent.email = selectedParentArray[0].email;
      this.parent.first_name = selectedParentArray[0].first_name;
      this.parent.last_name = selectedParentArray[0].last_name;
      this.parent.gender = selectedParentArray[0].gender;
      this.parent.mobile = selectedParentArray[0].mobile;
    }
    randomString = function (length) {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (var i = 0; i < length; i++) {
          text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return text;
      }
     uploadToS3(event){
      this.showLoadingBar = true;
      this.photoUpload = true;
        if (event.target.files){
          var reader = new FileReader();
          var blob;
          reader.readAsDataURL(event.target.files[0]);
          // tslint:disable-next-line:no-shadowed-variable
          const file = (event.target as HTMLInputElement).files[0];
          console.log('file',file);
          reader.onload = (event: any ) => {
            blob = event.target.result;
            this.imagePreview = event.target.result;
            // const file = (event.target as HTMLInputElement).files[0];
            this.horizontalStepperStep2.patchValue({image : blob});
            this.horizontalStepperStep2.get('image').updateValueAndValidity();
            let ext = 'jpg';
            let type = 'image/jpeg';
            let newName = this.randomString(6) + new Date().getTime() + '.' + ext;
            let that = this;
            // method 2

            // method 2
            this.AddStudentService.getSignedUploadRequest(newName, ext,type).subscribe(data => {
              console.log('Success in getSignedUploadRequest',data);
              that.avatar =  data['public_url'];
              console.log('i am in public url',data['public_url']);
              console.log('b64',blob);
              console.log(this.horizontalStepperStep2);
              let form = this.horizontalStepperStep2.value;
              blob =blob.replace(/^data:image\/\w+;base64,/, '');
              console.log('blob',blob);
              blob = this.base64toBlob(blob, 'jpg');
              this.student.image_url = data['public_url'];
              debugger;
              console.log('blob',blob);
              this.AddStudentService.putFileToS3(blob, data["presigned_url"])
              .subscribe(
                 
                response => {
                this.showLoadingBar = false;
                this.photoUpload = false;
                }
                );

            }, (err) => {

              console.log('getSignedUploadRequest timeout error: ', err);
            });

          };
        }
      }
      base64toBlob(base64Data, contentType) {
        contentType = contentType || '';
        var sliceSize = 1024;
        var byteCharacters = atob(base64Data);
        var bytesLength = byteCharacters.length;
        var slicesCount = Math.ceil(bytesLength / sliceSize);
        var byteArrays = new Array(slicesCount);
    
        for (var sliceIndex = 0; sliceIndex < slicesCount; ++sliceIndex) {
          var begin = sliceIndex * sliceSize;
          var end = Math.min(begin + sliceSize, bytesLength);
    
          var bytes = new Array(end - begin);
          for (var offset = begin, i = 0; offset < end; ++i, ++offset) {
            bytes[i] = byteCharacters[offset].charCodeAt(0);
          }
          byteArrays[sliceIndex] = new Uint8Array(bytes);
        }
        return new Blob(byteArrays, { type: contentType });
      }


    onFormValuesChanged(horizontalStepperStep: FormGroup, horizontalStepperStepErrors: any) {
        for (const field in horizontalStepperStepErrors) {
            if (!horizontalStepperStepErrors.hasOwnProperty(field)) {
                continue;
            }


            horizontalStepperStepErrors[field] = {};

            const control = horizontalStepperStep.get(field);

            if (control && control.dirty && control.touched && !control.valid) {
                horizontalStepperStepErrors[field] = control.errors;
            }
        }
    }

    finishHorizontalStepper() {
       let that = this;
       console.log(this.horizontalStepperStep1);
        that.button_disabled = true;
        debugger;

        var dateStrToSend = that.horizontalStepperStep1.value.date_of_birth.getFullYear() + '-' + (that.horizontalStepperStep1.value.date_of_birth.getMonth() + 1) +  '-' + that.horizontalStepperStep1.value.date_of_birth.getDate();
       that.student.date_of_birth = dateStrToSend;

       if(this.parentExist == false){
          try{
            that.AddStudentService.addstudent(this.student, this.parent).then(response => {
                 
              this.test = response;
              let is422 = that.AddStudentService.is422;
              if(response.status != "200" && !is422){
                  this.snackBar.open("Something went wrong", "Error", {
                      duration: 2000,
                  });
                  that.button_disabled = false;
              }
              else if (is422) {
                  this.snackBar.open("Email OR Contact Number OR Enrollement No Already Taken", "Done", {
                      duration: 2000,
                  });
                  that.button_disabled = false;    
              }
              else{
                  this.snackBar.open("Success", "Done", {
                      duration: 2000,
                  });
                  this.redirect('pages/student/list');   
              }
          });
          }
          catch(err){
              debugger;
          }
        }
        else{
          try{
            let object= {
              'id' : this.parentId
            }
            debugger;
            that.AddStudentService.addstudent(this.student, object).then(response => {
                // debugger;
                this.test = response;
                let is422 = that.AddStudentService.is422;
                if(response.status != "200" && !is422){
                    this.snackBar.open("Something went wrong", "Error", {
                        duration: 2000,
                    });
                }
                else if (is422) {
                    this.snackBar.open("Email OR Contact Number OR Enrollement No Already Taken", "Done", {
                        duration: 2000,
                    });    
                }
                else{
                    this.snackBar.open("Success", "Done", {
                        duration: 2000,
                    });
                    this.redirect('pages/student/list');   
                }
          });
          }
          catch(err){
              debugger;
          }
        }
        
    }

    redirect(pagename: string) {
        this.router.navigate([pagename]);
    }

   
    

    OnSubcategoriesChangeStatus(values: any, category: any) {
        // debugger;
        if (values.checked == false) {
            for (let subcategory of category.subCategories) {
                subcategory.checked = false;
            }
        }

    }
    OnCategoryChangeStatus(values: any, i: any) {
        // debugger;
        if (values.checked == true) {
            this.categoriesList[i].checked = true;
        }

    }
    onKey(event: any) { // without type info
        // debugger
        let value = '';
        let valuesArray: any[];
        value += event.target.value + '|';
        valuesArray = value.split('|');

        for (let i = 0; i < value.length; i++) {
            if (i == valuesArray.length - 2) {
                this.studentName = valuesArray[i];
            }
        }
        // this.nameExists();

    }
    nameExists() {

        // debugger;
        // const name = this.horizontalStepperStep1.get('name');
        let nameFound;
        this.AddStudentService.findStudentByName(this.studentName);
        if (this.AddStudentService.studentFound == true) {
            // const name = this.horizontalStepperStep1.get('name');
            // console.log("merchant already exists");

            return {
                nameFound: true
            };
        }
        return;
    }

    compareObjects(obj1: any, obj2: any): boolean {
        return obj1.id === obj2.id;
    }


}
