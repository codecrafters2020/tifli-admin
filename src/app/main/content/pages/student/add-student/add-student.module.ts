import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CdkTableModule } from '@angular/cdk/table';
import {  MatDatepickerModule, MatNativeDateModule, MAT_DATE_LOCALE } from '@angular/material';
import { MatFormFieldModule, MatInputModule, MatSelectModule,
  MatStepperModule, MatButtonModule, MatIconModule, MatRippleModule,
  MatSidenavModule, MatSlideToggleModule, MatTableModule, MatCheckboxModule} from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';

import { AddstudentComponent } from './add-student.component';
import { AddStudentService } from './add-student.service';
import { FormAddstudentComponent } from './form/form-add-student.component';
import { FancyImageUploaderModule } from 'ng2-fancy-image-uploader';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatProgressBarModule} from '@angular/material/progress-bar';

const routes: Routes = [
    {
        path     : '**',
        component: AddstudentComponent,
        children : [],
        resolve  : {
            studentById : AddStudentService
        }
    }
];

@NgModule({
    declarations: [
        AddstudentComponent,
        FormAddstudentComponent,
    ],
    imports     : [
        RouterModule.forChild(routes),
        MatProgressBarModule,
        CdkTableModule,
        MatFormFieldModule,
        MatExpansionModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatButtonModule,
        MatIconModule,
        MatRippleModule,
        MatSlideToggleModule,
        MatSidenavModule,
        MatTableModule,
        FancyImageUploaderModule,
        MatCheckboxModule,
        MatDatepickerModule,
        MatNativeDateModule,
        FuseSharedModule
    ],
    providers   : [
        AddStudentService
    ]
})
export class AddStudentModule
{
}
