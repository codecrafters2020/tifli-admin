import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';

import { AddStudentService } from './add-student.service';

import { Router } from '@angular/router';

@Component({
    selector     : 'fuse-file-manager',
    templateUrl  : './add-student.component.html',
    styleUrls    : ['./add-student.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class AddstudentComponent implements OnInit
{
    selected: any;
    pathArr: string[];

    constructor(private AddstudentService: AddStudentService, private router: Router)
    {
    }

    ngOnInit()
    {
        // this.AddMerchantService.onFileSelected.subscribe(selected => {
        //     this.selected = selected;
        //     this.pathArr = selected.location.split('>');
        // });
    }

    openstudentList(){

        this.router.navigateByUrl('pages/student/list/');
    }
}
