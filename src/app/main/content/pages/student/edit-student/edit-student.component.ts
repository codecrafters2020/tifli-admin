import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';

import { EditStudentService } from './edit-student.service';

import { Router } from '@angular/router';

@Component({
    selector     : 'fuse-file-manager',
    templateUrl  : './edit-student.component.html',
    styleUrls    : ['./edit-student.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class EditStudentComponent implements OnInit
{
    selected: any;
    pathArr: string[];

    constructor(private EditStudentService: EditStudentService, private router: Router)
    {
    }

    ngOnInit()
    {
        // this.AddMerchantService.onFileSelected.subscribe(selected => {
        //     this.selected = selected;
        //     this.pathArr = selected.location.split('>');
        // });
    }

    openstudentList(){
        
        this.router.navigateByUrl("pages/student/list/");
    }
}
