import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CdkTableModule } from '@angular/cdk/table';

import {  MatDatepickerModule, MatNativeDateModule } from '@angular/material';

import { MatFormFieldModule, MatInputModule, MatSelectModule, MatStepperModule, MatButtonModule, MatIconModule, MatRippleModule, MatSidenavModule, MatSlideToggleModule, MatTableModule, MatCheckboxModule} from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';

import { EditStudentComponent } from './edit-student.component';
import { EditStudentService } from './edit-student.service';
import { FormEditStudentComponent } from './form/form-edit-student.component';
import { FancyImageUploaderModule } from 'ng2-fancy-image-uploader';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatProgressBarModule} from '@angular/material/progress-bar';

const routes: Routes = [
    {
        path     : '**',
        component:EditStudentComponent,
        children : [],
        resolve  : {
            studentById : EditStudentService
        }
    }
];

@NgModule({
    declarations: [
        EditStudentComponent,
        FormEditStudentComponent,
    ],
    imports     : [
        RouterModule.forChild(routes),
        MatProgressBarModule,
        CdkTableModule,
        MatFormFieldModule,
        MatExpansionModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatButtonModule,
        MatIconModule,
        MatRippleModule,
        MatSlideToggleModule,
        MatSidenavModule,
        MatTableModule,
        FancyImageUploaderModule,
        MatCheckboxModule,

        FuseSharedModule
    ],
    providers   : [
        EditStudentService
    ]
})
export class EditStudentModule
{
}
