/* tslint:disable:triple-equals prefer-const quotemark */
import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators, FormsModule, FormControl, RequiredValidator } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { FancyImageUploaderOptions, UploadedFile } from 'ng2-fancy-image-uploader';
import { getLocaleDateTimeFormat } from '@angular/common';
import { AppService } from '../../../../../../app.service';

import { EditStudentService } from '../edit-student.service';

@Component({
    selector: 'fuse-file-list',
    templateUrl: './form-edit-student.component.html',
    styleUrls: ['./form-edit-student.component.scss'],
})
export class FormEditStudentComponent implements OnInit {

    // form: FormGroup;
    formErrors: any;
    test: any;
    step: number;
    classSectionResult: any[];
    studentObj:any;
    imagePreview: string;
    parentId: 0;
    parentExist: boolean = true;
    // Horizontal Stepper
    horizontalStepperStep1: FormGroup;
    horizontalStepperStep2: FormGroup;
    horizontalStepperStep3: FormGroup;
    horizontalStepperStep4: FormGroup;
    horizontalStepperStep5: FormGroup;
    horizontalStepperStep6: FormGroup;
    button_disabled :boolean = false;
    horizontalStepperStep1Errors: any;
    horizontalStepperStep2Errors: any;
    horizontalStepperStep3Errors: any;
    horizontalStepperStep4Errors: any;
    horizontalStepperStep5Errors: any;
    horizontalStepperStep6Errors: any;
    student = {

      first_name: '',
      last_name: '',
      gender: "",
      mobile: 0,
      email: '',
      enrollment_number: '',
      date_of_birth: new FormControl(new Date()),
      address: '',
      country: 'Pakistan',
      class_section_id:'',
      current_monthly_fee:0,
      image_url: ''
    };
    parent = {
      first_name: '',
      last_name: '',
      gender: "",
      mobile: 0,
      email: '',
      role: 4,
      id: 0
    };


    displayedColumns = ['enable', 'name', 'description'];
    displayedColumnsPanelHeading = ['CategoryName', 'Description', 'isChecked'];


    countries = [
        { 'name': 'United States', 'code': 'US' },
        { 'name': 'Pakistan', 'code': 'PK' }
    ];

    genders = [
      { 'name': 'Male', 'code': 'M' , 'value': "M"},
      { 'name': 'Female', 'code': 'F' , 'value': "F"}
    ];


    categoriesList: any[] = [];
    usersList: any[];
    barcodesList: any[] = [];
    affiliateNetworksList: any[];
    studentAffiliateStatusList: any[];
    studentTierList: any[];
    studentList: any[];
    studentName = '';
    lastBarCode: any;
    lastMasterStore: any;
    lastStudentTier: any;
    lastAccountManager: any;
    lastManager: any;
    showLoadingBar: boolean = false;
    photoUpload: boolean = false;



    categoriesGroups: any[];
    subCategoriesGroups: any[] = [];
    catControls: any[] = [];
    avatar: any;
    parents: any;

  // tslint:disable-next-line:no-shadowed-variable
    constructor(private EditStudentService: EditStudentService,
                private formBuilder: FormBuilder, private router: Router, private snackBar: MatSnackBar, private appService: AppService) {
      
      this.classSectionResult = this.EditStudentService.classes;
      this.studentObj = EditStudentService.student.student;
      this.imagePreview = EditStudentService.student.student.image_url;
      this.parents = this.EditStudentService.parents;
      debugger;

        // Horizontal Stepper form error
        this.horizontalStepperStep1Errors = {
          first_name: {},
          last_name: {},
          class_section_id: {},
          gender: {},
          mobile: {},
          enrollment_number: {},
          date_of_birth: {},
          address: {},
          // searchTerms: {},
          // barcodeFormat: {}
        };

     
        this.horizontalStepperStep3Errors = {
          first_name: {},
          last_name: {},
          gender: {},
          mobile: {},
          fruitCtrl: {}
          // address: {},
        };

    }

    ngOnInit() {

        // Horizontal Stepper form steps
        this.horizontalStepperStep1 = this.formBuilder.group({

            first_name: ['', [Validators.required]],
            last_name: ['', [Validators.required]],
            class_section_id: ['', [Validators.required]],
            gender: ['', [Validators.required]],
            mobile: [''],
            enrollment_number: ['', [Validators.required]],
            date_of_birth: ['', [Validators.required]],
            address: ['', [Validators.required]],
            country: [''],
            email: [''],
            image_url: [''],
            fees: ['',[Validators.required]]
        });

        this.horizontalStepperStep2 = this.formBuilder.group({
          image : new FormControl(null, {})
           
        });

        this.horizontalStepperStep3 = this.formBuilder.group({

          first_name: ['', [Validators.required]],
          last_name: ['', [Validators.required]],
          gender: ['', [Validators.required]],
          mobile: ['', [Validators.required]],
          // address: ['', [Validators.required]],
          status: [''],
          fruitCtrl: [''],
          parentEmail: ['',[Validators.required,Validators.email]],
          
        });

        this.horizontalStepperStep1.controls.first_name.valueChanges.subscribe(() => {
            let normalized_name = this.student.first_name.toLowerCase() + '' + this.student.last_name.toLowerCase();
            normalized_name = normalized_name.trim();
            normalized_name = normalized_name.replace("'", "");
            let normalized_name_arr = normalized_name.split(' ');
            normalized_name = '';
            normalized_name_arr.forEach(word => {
                if (normalized_name == ''){
                    normalized_name = word;
                }
                else{
                  normalized_name = normalized_name + '-' + word;
                }
            });
            
        });


        this.horizontalStepperStep1.valueChanges.subscribe(() => {
            this.onFormValuesChanged(this.horizontalStepperStep1, this.horizontalStepperStep1Errors);
        });

        this.horizontalStepperStep2.valueChanges.subscribe(() => {
            this.onFormValuesChanged(this.horizontalStepperStep2, this.horizontalStepperStep2Errors);
        });

        this.horizontalStepperStep3.valueChanges.subscribe(() => {
            this.onFormValuesChanged(this.horizontalStepperStep3, this.horizontalStepperStep3Errors);
        });

        

    }

    onChange(){
      debugger;
      const selectedParentArray = this.parents.filter(parent => parent.id == this.studentObj.parent_details.id);
      this.studentObj.parent_details.email = selectedParentArray[0].email;
      this.studentObj.parent_details.first_name = selectedParentArray[0].first_name;
      this.studentObj.parent_details.last_name = selectedParentArray[0].last_name;
      this.studentObj.parent_details.gender = selectedParentArray[0].gender;
      this.studentObj.parent_details.mobile = selectedParentArray[0].mobile;
    }
    
    onFormValuesChanged(horizontalStepperStep: FormGroup, horizontalStepperStepErrors: any) {
        for (const field in horizontalStepperStepErrors) {
            if (!horizontalStepperStepErrors.hasOwnProperty(field)) {
                continue;
            }


            horizontalStepperStepErrors[field] = {};

            const control = horizontalStepperStep.get(field);

            if (control && control.dirty && control.touched && !control.valid) {
                horizontalStepperStepErrors[field] = control.errors;
            }
        }
    }

    finishHorizontalStepper() {
      this.button_disabled = true;
       let that = this;
        this.student.first_name= this.studentObj.first_name;
        this.student.last_name =this.studentObj.last_name;
        this.student.address=this.studentObj.address;
        if(this.studentObj.gender == "M"){
          that.studentObj.gender = 1
        }
        else if(this.studentObj.gender == "F"){
          that.studentObj.gender = 2
        }
        if(this.studentObj.parent_details.gender == "M"){
          that.studentObj.parent_details.gender = 1
        }
        else if(this.studentObj.parent_details.gender == "F"){
          that.studentObj.parent_details.gender = 2
        }
        this.student.gender = this.studentObj.gender;

        this.student.image_url = this.studentObj.image_url;
        this.student.date_of_birth = this.studentObj.date_of_birth;
        this.student.email = this.studentObj.email;
        this.student.mobile = this.studentObj.mobile;
        this.student.class_section_id = this.studentObj.class_section_id;
        this.student.current_monthly_fee = this.studentObj.current_monthly_fee;
        this.student.country = this.studentObj.country;
        this.student.enrollment_number = this.studentObj.enrollment_number;
        this.parent.first_name= this.studentObj.parent_details.first_name;
        this.parent.email= this.studentObj.parent_details.email;
        this.parent.gender= this.studentObj.parent_details.gender;
        this.parent.last_name= this.studentObj.parent_details.last_name;
        this.parent.mobile= this.studentObj.parent_details.mobile;
        this.parent.id = this.studentObj.parent_details.id;
      //  this.parent.password= this.studentObj.parent_details.password;
        this.parent.role= this.studentObj.parent_details.role;
      if(this.parentExist== false){
        try{
          
              
              this.EditStudentService.editStudent(this.student, this.parent).then(response => {
                // debugger;
                let is422 = that.EditStudentService.is422;
                if(response.status != "200" && !is422){
                    this.snackBar.open("Something went wrong", "Error", {
                        duration: 2000,
                    });
                    that.button_disabled = false;
                }
                else if (is422) {
                    this.snackBar.open("Email OR Contact Number OR Enrollement No Already Taken", "Done", {
                        duration: 2000,
                    });    
                    that.button_disabled = false;
                }
                else{
                    this.snackBar.open("Success", "Done", {
                        duration: 2000,
                    });
                    this.redirect('pages/student/list');   
                }
            });
        }
        catch (err){
            // debugger;
        }
      }
      else{
        try{
         
          let object= {
            'id' : this.studentObj.id,
            'student' : this.student,
            'parent' : this.parent
          }

         this.EditStudentService.editStudent(this.student,this.parent).then(response => {
            // debugger;
            let is422 = that.EditStudentService.is422;
                if(response.status != "200" && !is422){
                    this.snackBar.open("Something went wrong", "Error", {
                        duration: 2000,
                    });
                    that.button_disabled = false;
                }
                else if (is422) {
                    this.snackBar.open("Email OR Contact Number OR Enrollement No Already Taken", "Done", {
                        duration: 2000,
                    });    
                    that.button_disabled = false;
                }
                else{
                    this.snackBar.open("Success", "Done", {
                        duration: 2000,
                    });
                    this.redirect('pages/student/list');   
                }
        });
        }
        catch (err){
            // debugger;
        }
      }
        
    }

    redirect(pagename: string) {
        this.router.navigate([pagename]);
    }
    randomString = function (length) {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (var i = 0; i < length; i++) {
          text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return text;
      }
     uploadToS3(event){
      this.showLoadingBar = true;
      this.photoUpload = true;
        if (event.target.files){
          var reader = new FileReader();
          var blob;
          reader.readAsDataURL(event.target.files[0]);
          // tslint:disable-next-line:no-shadowed-variable
          const file = (event.target as HTMLInputElement).files[0];
          console.log('file',file);
          reader.onload = (event: any ) => {
            blob = event.target.result;
            this.imagePreview = event.target.result;
            // const file = (event.target as HTMLInputElement).files[0];
            this.horizontalStepperStep2.patchValue({image : blob});
            this.horizontalStepperStep2.get('image').updateValueAndValidity();
            let ext = 'jpg';
            let type = 'image/jpeg';
            let newName = this.randomString(6) + new Date().getTime() + '.' + ext;
            let that = this;
            // method 2

            // method 2
            this.EditStudentService.getSignedUploadRequest(newName, ext,type).subscribe(data => {
              console.log('Success in getSignedUploadRequest',data);
              that.avatar =  data['public_url'];
              console.log('i am in public url',data['public_url']);
              console.log('b64',blob);
              console.log(this.horizontalStepperStep2);
              let form = this.horizontalStepperStep2.value;
              blob =blob.replace(/^data:image\/\w+;base64,/, '');
              console.log('blob',blob);
              blob = this.base64toBlob(blob, 'jpg');
              this.student.image_url = data['public_url'];
              debugger;
              console.log('blob',blob);
              this.EditStudentService.putFileToS3(blob, data["presigned_url"])
              .subscribe(
                 
                response => {
                this.showLoadingBar = false;
                this.photoUpload = false;
                }
                );

            }, (err) => {

              console.log('getSignedUploadRequest timeout error: ', err);
            });

          };
        }
      }
      base64toBlob(base64Data, contentType) {
        contentType = contentType || '';
        var sliceSize = 1024;
        var byteCharacters = atob(base64Data);
        var bytesLength = byteCharacters.length;
        var slicesCount = Math.ceil(bytesLength / sliceSize);
        var byteArrays = new Array(slicesCount);
    
        for (var sliceIndex = 0; sliceIndex < slicesCount; ++sliceIndex) {
          var begin = sliceIndex * sliceSize;
          var end = Math.min(begin + sliceSize, bytesLength);
    
          var bytes = new Array(end - begin);
          for (var offset = begin, i = 0; offset < end; ++i, ++offset) {
            bytes[i] = byteCharacters[offset].charCodeAt(0);
          }
          byteArrays[sliceIndex] = new Uint8Array(bytes);
        }
        return new Blob(byteArrays, { type: contentType });
      }


    



       nameExists() {

        // debugger;
        // const name = this.horizontalStepperStep1.get('name');
        let nameFound;
        this.EditStudentService.findStudentByName(this.studentName);
        if (this.EditStudentService.studentFound == true) {
            // const name = this.horizontalStepperStep1.get('name');
            // console.log("merchant already exists");

            return {
                nameFound: true
            };
        }
        return;
    }

    compareObjects(obj1: any, obj2: any): boolean {
        return obj1.id === obj2.id;
    }


}
