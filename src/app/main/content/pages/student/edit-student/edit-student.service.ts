import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { AppService } from '../../../../../app.service';
import { AbstractControl } from '@angular/forms';
import { HttpHeaders, } from '@angular/common/http';
import { Headers } from '@angular/http';
import { RequestOptions } from '@angular/http';
import {AuthService} from '../../../../../auth/auth.service'
import {InterceptorSkipHeader} from '../../../../../auth/jwt.interceptor';
import { of } from 'rxjs/observable/of'
@Injectable()
export class EditStudentService implements Resolve<any>
{

    categoriesList : any[];
    usersList: any[];
    barcodesList : any[];
    affiliateNetworksList : any[];
    studentAffiliateStatusList : any[];
    studentList : any[];
    studentFound : boolean=false;
    studentTierList : any[];
    id:any;
    student:any;
    classes: any;
    studentParentObject: { student: any; parent: any; };
    headers: any;
    parents: any;
    is422 : boolean = false;
    
    constructor(private http: HttpClient, private appService: AppService, public globalVar: AuthService)
    {
    }

    /**
     * The File Manager App Main Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {

        this.id=route.params['id'];


        return new Promise((resolve, reject) => {

            Promise.all([
                this.getstudentById(),
                this.getClass(),
                this.getParents()
            ]).then(
                ([]) => {
                    resolve();
                },
                reject
            );
        });
        // return new Promise((resolve, reject) => {

        //     Promise.all([
        //         this.getCategories(),
        //         this.getUsers(),
        //         this.getBarcodes(),
        //         this.getAffiliateNetworks(),
        //         this.getAffiliateNetworkStatuses(),
        //         this.getMerchants(),
        //         this.getMerchantById(),
        //         this.getMerchantTiers()
        //     ]).then(
        //         ([categoriesList,usersList,barcodesList,affiliateNetworksList,merchantAffiliateStatusList,merchantsList,merchant,merchantTierList]) => {
        //             resolve();
        //         },
        //         reject
        //     );
        // });
    }
    getParents(): Promise<any> {
        return new Promise((resolve, reject) => {
          this.http.get(`${this.appService.adminService}students/get_all_parents`)
            .subscribe((response: any) => {
              this.parents = response.sms_users;
              resolve(response);
            }, reject);
        });
      }

    getstudent(): Promise<any>
    {

        return new Promise((resolve, reject) => {
            this.http.get(this.appService.merchantService)
                .subscribe((response: any) => {
                    console.log(response);
                    if(response.meta.code=="200")
                    {
                        this.studentList=response.data;
                    }
                    resolve(response);
                }, reject);
        });
    }

    getSignedUploadRequest(name,ext, type) {
        this.setHeader()
        // return this.http.get(`${this.apiUrl}/companies/presign_upload?filename=${name}&filetype=${type}&contentType=jpeg`, {headers: this.headers});
  
        return this.http.get(`${this.appService.uploadImageService}/signed_urls.json?filename=${name}&filetype=${type}&contentType=jpg`,
        {headers: this.headers});
  
  
      }
      putFileToS3(body: File, presignedUrl: string){
        type bodyType = 'body';
  
        // upload file to the pre-signed url
        const httpOptions = {
          headers: new HttpHeaders({
            'Content-Type': 'image/jpeg'
              }),
         observe: <bodyType>'response'
        };
        const headers = new HttpHeaders().set(InterceptorSkipHeader, '');
  
        return this.http.put(presignedUrl, body, { headers });
        }
  
        uploadFile(url, file) {
  
          return this.http.put(url, file);
      }
      setHeader(){
        this.headers = new HttpHeaders({
           Authorization: this.globalVar.user.accessToken|| '',
           "Access-Control-Allow-Origin": "*"
  
        });
      }
    findStudentByName(student_name: string ): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this.http.get(this.appService.adminService + 'students/' + student_name)
                .subscribe((response: any) => {
                    console.log(response);
                    // debugger;
                    if (response.meta.code== '200')
                    {
                        this.studentFound=true;
                    }
                    resolve(response);
                }, reject);
        });
    }

    getstudentById(): Promise<any>
    {
        let queryString = 'id=' + this.id ;

        return new Promise((resolve, reject) => {
            this.http.get(`${this.appService.adminService}students/get_student?${queryString}`)
            .subscribe((response: any) => {
                      debugger;
                    console.log(response);
                    if(response)
                    {
                        debugger;
                        this.student=response;
                    }
                    resolve(response);
                }, reject);
        });
    }

    getCategories(): Promise<any>
    {

        return new Promise((resolve, reject) => {

            this.http.get(this.appService.merchantService + 'categories/')
                .subscribe((response: any) => {
                    console.log(response);
                    if(response.meta.code=="200")
                    {
                        this.categoriesList=response.data;
                    }

                    resolve(response);
                }, reject);
        });
    }

    getUsers(): Promise<any>
    {

        return new Promise((resolve, reject) => {
            this.http.get(this.appService.userService + 'users/')
                .subscribe((response: any) => {
                    console.log(response);
                    if(response.meta.code=="200")
                    {
                        this.usersList=response.data;
                    }
                    resolve(response);
                }, reject);
        });
    }
    getClass(): Promise<any> {
        return new Promise((resolve, reject) => {
          this.http.get(`${this.appService.adminService}class_sections/show_all_classes`)
            .subscribe((response: any) => {
              this.classes = response.class_sections;
              resolve(response);
            }, reject);
        });
      }
    getBarcodes(): Promise<any>
    {

        return new Promise((resolve, reject) => {
            this.http.get(this.appService.merchantService + 'barcodes/')
                .subscribe((response: any) => {
                    console.log(response);
                    if(response.meta.code=="200")
                    {
                        this.barcodesList=response.data;
                    }
                    resolve(response);
                }, reject);
        });
    }

    getAffiliateNetworks(): Promise<any>
    {

        return new Promise((resolve, reject) => {
            this.http.get(this.appService.merchantService + 'affiliates/')
                .subscribe((response: any) => {
                    console.log(response);
                    if(response.meta.code=="200")
                    {
                        this.affiliateNetworksList=response.data;
                    }
                    resolve(response);
                }, reject);
        });
    }

    getAffiliateNetworkStatuses(): Promise<any>
    {

        return new Promise((resolve, reject) => {
            this.http.get(this.appService.merchantService + 'affiliates/statuses/')
                .subscribe((response: any) => {
                    console.log(response);
                    if(response.meta.code=="200")
                    {
                        this.studentAffiliateStatusList=response.data;
                    }
                    resolve(response);
                }, reject);
        });
    }

    getstudentTiers(): Promise<any> {

        return new Promise((resolve, reject) => {
            this.http.get(this.appService.merchantService + 'tiers/')
                .subscribe((response: any) => {
                    console.log(response);
                    if (response.meta.code == "200") {
                        this.studentTierList = response.data;
                    }
                    resolve(response);
                }, reject);
        });
    }
    editStudent(student, parent): Promise<any> {
        return new Promise((resolve, reject) => {
            // debugger;

            let Obj = {
                id: this.id,
                student: student,
                parent: parent
            };
          
            this.http.patch(`${this.appService.adminService}students`, Obj,{observe:"response"})
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        }).catch(err => this.handleError(err, () =>   this.http.patch(`${this.appService.adminService}students`,{observe:"response"}),4));
    }
    protected handleError(error, continuation: () => Observable<any> ,object) {
      let that = this;
      if (error.status == 404 || error.status == 400) {
        if(object==1){
          that.classes = [];
          return of(false);
        }
        else if(object==2){
          that.parents = [];
          return of(false);
        }
      }
      else if (error.status == 422){
        this.is422 = true;
        return of(false);
      }

   
  }
    
    updateStudent(student): Promise<any>
    {
        return new Promise((resolve, reject) => {
            debugger;
            // this.http.post('https://httpbin.org/post', merchant)
            console.log("before merchnt")
            console.log(this.appService.merchantService)
            this.http.put(this.appService.merchantService + student.id, student)
                .subscribe((response: any) => {
                    console.log("after merchnt")
                    console.log(this.appService.merchantService)
                    resolve(response);
                }, reject);
        });
    }

}
