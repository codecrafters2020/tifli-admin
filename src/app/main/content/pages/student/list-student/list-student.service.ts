import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { of } from 'rxjs/observable/of'
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/mergeMap';
import { AppService } from '../../../../../app.service';
import * as _ from 'lodash';

@Injectable()
export class ListStudentService implements Resolve<any>
{
    students: any[];
    temp: any[];
    classes: any[];
    onFilesChanged: BehaviorSubject<any> = new BehaviorSubject({});
    onFileSelected: BehaviorSubject<any> = new BehaviorSubject({});
    onStudentChanged: BehaviorSubject<any> = new BehaviorSubject({});
    constructor(private http: HttpClient, private appService: AppService)
    {
    }

    /**
     * The File Manager App Main Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getStudents(),
                this.getClasses(),
               
                //this.getDashboard3()
                
            ]).then(
                ([student]) => {
                    resolve();
                },
                reject
            );
        });
    }

  deleteStudent(student): Promise<any>
  {
    return new Promise((resolve, reject) => {

      this.http.patch(`${this.appService.adminService}students/delete_student`, student)
        .subscribe((response: any) => {
          // debugger;
          resolve(response);
        }, reject);
    });
  }
  protected handleError(error, continuation: () => Observable<any> ,object) {
    let that = this;
    if (error.status == 404 || error.status == 400) {
        if(object==1){
          that.students = [];
        }
        else if (object == 2){
          that.classes = [];
        }
      return of(false);
    }
 
}

 
  getStudents(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get(`${this.appService.adminService}students/show_all_students`)
        .subscribe((response: any) => {
          // debugger;
          this.temp = response.students;
          this.students = response.students;
          resolve(response);
        }, reject);
    }).catch(err => this.handleError(err, () =>  this.http.get(`${this.appService.adminService}students/show_all_students`),1));
  }
  getClasses(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get(`${this.appService.adminService}faculties/show_all_classes`)
        .subscribe((response: any) => {
          this.classes = response.class_sections;
          resolve(response);
        }, reject);
    }).catch(err => this.handleError(err, () =>  this.http.get(`${this.appService.adminService}faculties/show_all_classes`),2));
  }
  
  getFilteredStudent(
    class_section: number ,
    studentName: string

  ): Promise<any> {
    // debugger;
    let params = {

      class_section,
      studentName
    };

    let queryString = '';
    _.forEach(params, (val, key) => {
      if (val || typeof val === 'number') {
        queryString = queryString + key + '=' + val + '&';
      }
    });

    return new Promise((resolve, reject) => {
      // this.http.get('api/coupon-list')
      // this.http.get(`http://104.42.179.33:8765/cms/api/coupons?page=0&size=20&sort=couponCode,asc&searchTerms=gag&couponCode=123&couponType=Deal`)
      this.http.get(`${this.appService.adminService}students/get_filtered_students?${queryString}`)
        .subscribe((response: any) => {
          this.students = response.students;
          this.onFilesChanged.next(response.students);
          this.onFileSelected.next(response[0]);
          resolve(response);
        }, reject);
    });
  }
    //
    // searchstudentName(term: string): Observable<any> {
    //     return this.http.get(`${this.appService.adminService}search/?name=${term}`)
    //         .map(response => response['data'])
    //         .map(data => data instanceof Array ? data : []);
    // }
}
