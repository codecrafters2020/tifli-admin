import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';

import { ListStudentService } from './list-student.service';
import { Router } from '@angular/router';
import {FormBuilder, FormGroup} from '@angular/forms';

@Component({
    selector     : 'fuse-file-manager',
    templateUrl  : './list-student.component.html',
    styleUrls    : ['./list-student.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class ListStudentComponent implements OnInit
{
    selected: any;
    pathArr: string[];
    isSearch: boolean = false;
    form: FormGroup;
    studentSearchResult: any[];

    constructor(private ListStudentService: ListStudentService, private router: Router, private formBuilder: FormBuilder)
    {
      this.form = this.formBuilder.group({
        studentName : ['', ]});
    }

    ngOnInit()
    {
      this.ListStudentService.onFileSelected.subscribe(selected => {
        this.selected = selected;
        // this.pathArr = selected.location.split('>');
      });
    }

    openStudentAdd(){
        this.router.navigateByUrl('pages/student/add/');
    }

  resetGrid() {
    this.ListStudentService.getStudents();
  }

  searching() {

    if (this.isSearch == false){
      this.isSearch = true;
    }

    else{
      this.isSearch = false;
      console.log( 'close it' );
    }
  }
}
