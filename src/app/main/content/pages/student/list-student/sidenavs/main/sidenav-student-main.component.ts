import {Component, Input} from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatRadioModule} from '@angular/material/radio';
import { FuseAngularMaterialModule } from '../../../../../components/angular-material/angular-material.module';
import {ListStudentService} from '../../list-student.service';


@Component({
    selector   : 'fuse-file-manager-main-sidenav',
    templateUrl: './sidenav-student-main.component.html',
    styleUrls  : ['./sidenav-student-main.component.scss']
})
export class SidenavstudentMainComponent
{

  @Input() leftSideNav: any;
  selected: any;
  form: FormGroup;
  formErrors: any;

  btnDisabled = false;
  classSectionResult: any[];
  classSection: any;
  studentName: any;
  studentSearchResult: any[];
  filterVal : any;
  searchBox: any;
  filters: any[] = [
    {value: 1, viewValue: 'Student First Name'},
    {value: 2, viewValue: 'Student Number'},
    {value: 3, viewValue: 'Student Email'},
    {value: 4, viewValue: 'Student Class'},
    {value: 5, viewValue: 'Parent First Name'},
    {value: 6, viewValue: 'Roll #'}
  ];

  // tslint:disable-next-line:no-shadowed-variable
    constructor(private ListStudentService: ListStudentService,
        private router: Router,
        private formBuilder: FormBuilder) {
        // Reactive Form

        this.form = this.formBuilder.group({
          studentName: ['', ],
          class : ['', ]
          // seasonalStatus: ['',],
        });

        // Reactive form errors
        this.formErrors = {
          studentname: {},
          class : {}
            // seasonalStatus: {}
        };
        this.classSectionResult = this.ListStudentService.classes;
    }
    onChange(){
      let that = this;
      if(this.searchBox ==""){
        that.discardFilter();
      }
    }
    filter()
    {
      let that = this;
      this.btnDisabled = true;
      // debugger;
      debugger;
      if(this.filterVal == 1){
        that.ListStudentService.students = that.ListStudentService.students.filter(res =>{
          
          return res.first_name.toLocaleLowerCase().match(this.searchBox.toLocaleLowerCase());
         });

      }
      else if(this.filterVal == 2){
        that.ListStudentService.students = that.ListStudentService.students.filter(res =>{
          
          return res.mobile.toLocaleLowerCase().match(this.searchBox.toLocaleLowerCase());
         });
      }
      else if(this.filterVal == 3){
        that.ListStudentService.students = that.ListStudentService.students.filter(res =>{
          
          return res.email.toLocaleLowerCase().match(this.searchBox.toLocaleLowerCase());
         });
      }
  
      else if(this.filterVal == 4){
        that.ListStudentService.students = that.ListStudentService.students.filter(res =>{
          
          return res.class_section.school_class.class_name.toLocaleLowerCase().match(this.searchBox.toLocaleLowerCase());
         });
      }
      else if(this.filterVal == 5){
        that.ListStudentService.students = that.ListStudentService.students.filter(res =>{
          
          return res.parent_details.first_name.toLocaleLowerCase().match(this.searchBox.toLocaleLowerCase());
         });
      }
      else if(this.filterVal == 6){
        that.ListStudentService.students = that.ListStudentService.students.filter(res =>{
          
          return res.enrollment_number.toLocaleLowerCase().match(this.searchBox.toLocaleLowerCase());
         });
      }
      this.ListStudentService.onStudentChanged.next(that.ListStudentService.students);
  
     }
     discardFilter(){
      this.ListStudentService.students = this.ListStudentService.temp;
      this.ListStudentService.onStudentChanged.next(this.ListStudentService.students);
     }
    displaySelectedName(item: any) {
        return item;
    }

    ngOnInit() {
        this.form.valueChanges.subscribe(() => {
            // console.log(this.form.value);
        });
        let x: any;
      this.form.valueChanges.subscribe(() => {
        // console.log(this.form.value);
      });
    }

    callSomeFunction(){
        this.studentSearchResult.forEach(student => {
            if (student.name == this.form.controls.studentname.value){
                    this.router.navigateByUrl('pages/student/edit/' + student.id);
            }
        });
    }

}
