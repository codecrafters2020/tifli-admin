import { Component, OnInit } from '@angular/core';

import { fuseAnimations } from '@fuse/animations/index';

import { ListStudentService } from '../../list-student.service';

@Component({
    selector   : 'fuse-file-manager-details-sidenav',
    templateUrl: './sidenav-student-details.component.html',
    styleUrls  : ['./sidenav-student-details.component.scss'],
    animations : fuseAnimations
})
export class SidenavstudentDetailsComponent implements OnInit
{

    selected: any;

    constructor(private ListStudentService: ListStudentService)
    {

    }

    ngOnInit()
    {
        this.ListStudentService.onFileSelected.subscribe(selected => {
            this.selected = selected;
        });
    }

}
