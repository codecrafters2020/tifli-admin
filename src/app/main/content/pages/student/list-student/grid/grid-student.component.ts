import { Component, ElementRef, OnInit, ViewChild, AfterViewInit, OnChanges } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { DataSource } from '@angular/cdk/collections';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';

import { ListStudentService } from '../list-student.service';
import { fuseAnimations } from '@fuse/animations/index';
import { FuseUtils } from '@fuse/utils';
import * as _ from 'lodash';

@Component({
    selector: 'fuse-file-list',
    templateUrl: './grid-student.component.html',
    styleUrls: ['./grid-student.component.scss'],
    animations: fuseAnimations
})
export class GridStudentComponent implements OnInit, OnChanges, AfterViewInit {
    nothing = 'None';
    files: any;
    dataSource: GridStudentDataSource | null;
    displayedColumns = ['student_id', 'enrolment_number', 'student_name', 'gender', 'mobile', 'email', 'dob', 'class','modified'];
    selected: any;
    lastId: number;

    // pagination
    pageSize: number;
    pageSizeOptions: number[];


    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild('filter') filter: ElementRef;
    @ViewChild(MatSort) sort: MatSort;

  // tslint:disable-next-line:no-shadowed-variable
    constructor(private ListStudentService: ListStudentService, private router: Router, private snackBar: MatSnackBar) {
        this.pageSize = 25;
        this.pageSizeOptions = [this.pageSize, this.pageSize * 2, this.pageSize * 4];

    }

    myStyles(studentId): any {
        let studentIndex = this.ListStudentService.students.findIndex(student => student.id === studentId);
        if (this.ListStudentService.students[studentIndex].affiliateNetwork.name === 'Commission Junction') {
            return 'mat-purple-300-bg';
        }

        if (this.ListStudentService.students[studentIndex].affiliateNetwork.name === 'Pepper Jam') {

            return 'mat-green-300-bg';
        }

        if (this.ListStudentService.students[studentIndex].affiliateNetwork.name === 'Impact Radius') {

            return 'mat-yellow-300-bg';
        }

        if (this.ListStudentService.students[studentIndex].affiliateNetwork.name === 'LinkShare') {

            return 'mat-pink-300-bg';
        }

        if (this.ListStudentService.students[studentIndex].affiliateNetwork.name === 'Ratuken') {

            return 'mat-orange-300-bg';
        }


    }


    ngOnChanges(changes) {
    }

    ngAfterViewInit() {
    //     _.forEach(this.dataSource.filteredData, (value, key) => {
    //         if (value.hasOwnProperty('manager')){
    //             if (value.manager.id > 0){
    //               this.ListStudentService.getManager(value.manager.id, this.dataSource.filteredData, key);
    //             }
    //
    //         }
    //         else{
    //             value['manager'] = {
    //                 id: 0
    //             };
    //         }
    //
    //     });
    }

    ngOnInit() {
        // debugger;
        this.dataSource = new GridStudentDataSource(this.ListStudentService, this.paginator, this.sort);
        this.ListStudentService.onStudentChanged.subscribe(data => {
            this.dataSource = new GridStudentDataSource(this.ListStudentService, this.paginator, this.sort);
        })
        // Observable.fromEvent(this.filter.nativeElement, 'keyup')
        //           .debounceTime(150)
        //           .distinctUntilChanged()
        //           .subscribe(() => {
        //               if ( !this.dataSource )
        //               {
        //                   return;
        //               }
        //               this.dataSource.filter = this.filter.nativeElement.value;
        //           });
        // this.ListMerchantService.merchants[1].affiliate.colo
    }

    onSelect(student) {
        this.router.navigateByUrl('pages/student/edit/' + student.id);
    }

    onSelected(student) {
        // this.ListMerchantService.onFileSelected.next(selected);
        this.router.navigateByUrl('pages/student-coupons/list/' + student.id);
    }

    openstudentDashboard(event, studentid) {
        this.router.navigateByUrl('apps/dashboards/project/' + studentid);
    }

    openStudentEdit(event, student_id) {
        this.router.navigateByUrl('pages/student/edit/' + student_id);
    }

  deleteStudent(event, id){


    let queryString = {
      id: id,
    };

    try{
      this.ListStudentService.deleteStudent(queryString).then(response => {


        // debugger;
        if (response.status !== '200'){
          this.snackBar.open('Something went wrong', 'Error', {
            duration: 2000,
          });
        }
        this.snackBar.open('Sucess', 'Done', {
          duration: 2000,
        });

        this.redirect('pages/student/list');
      });
    }
    catch (err){
      debugger;
    }
  }

  openAddCoupon(event, studentid) {
        this.router.navigate(['/pages/coupon/add/'], { queryParams: {  id : studentid } });
  }

    redirect(pagename: string) {
    this.router.navigate([pagename]);
  }
}


export class GridStudentDataSource extends DataSource<any>
{
    _filterChange = new BehaviorSubject('');
    _filteredDataChange = new BehaviorSubject('');

    get filteredData(): any {
        return this._filteredDataChange.value;
    }

    set filteredData(value: any) {
        this._filteredDataChange.next(value);
    }

    get filter(): string {
        return this._filterChange.value;
    }

    set filter(filter: string) {
        this._filterChange.next(filter);
    }
  // tslint:disable-next-line:no-shadowed-variable
    constructor(private ListStudentService: ListStudentService, private _paginator: MatPaginator, private _sort: MatSort) {
        super();
        this.filteredData = this.ListStudentService.students;
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    // connect(): Observable<any[]>
    // {
    //     return this.ListMerchantService.onFilesChanged;
    // }
    connect(): Observable<any[]> {
        const displayDataChanges = [
            this.ListStudentService.onFilesChanged,
            this._paginator.page,
            this._filterChange,
            // this._sort.sortChange
        ];

        return Observable.merge(...displayDataChanges).map(() => {
            let data = this.ListStudentService.students.slice();

            data = this.filterData(data);

            this.filteredData = [...data];

            // data = this.sortData(data);

            // Grab the page's slice of data.
            const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
            return data.splice(startIndex, this._paginator.pageSize);
        });
    }

    filterData(data) {
        if (!this.filter) {
            return data;
        }
        return FuseUtils.filterArrayByString(data, this.filter);
    }

    // sortData(data): any[]
    // {
    //     if ( !this._sort.active || this._sort.direction === '' )
    //     {
    //         return data;
    //     }

    //     return data.sort((a, b) => {
    //         let propertyA: number | string = '';
    //         let propertyB: number | string = '';

    //         switch ( this._sort.active )
    //         {
    //             case 'id':
    //                 [propertyA, propertyB] = [a.id, b.id];
    //                 break;
    //             case 'name':
    //                 [propertyA, propertyB] = [a.name, b.name];
    //                 break;
    //             case 'categories':
    //                 [propertyA, propertyB] = [a.categories[0], b.categories[0]];
    //                 break;
    //             case 'price':
    //                 [propertyA, propertyB] = [a.priceTaxIncl, b.priceTaxIncl];
    //                 break;
    //             case 'quantity':
    //                 [propertyA, propertyB] = [a.quantity, b.quantity];
    //                 break;
    //             case 'active':
    //                 [propertyA, propertyB] = [a.active, b.active];
    //                 break;
    //         }

    //         const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
    //         const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

    //         return (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1);
    //     });
    // }

    disconnect() {
    }
}
