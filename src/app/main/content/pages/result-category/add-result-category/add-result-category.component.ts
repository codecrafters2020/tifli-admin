import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';

import { AddResultCategoryService } from './add-result-category.service';

import { Router } from '@angular/router';

@Component({
    selector     : 'fuse-file-manager',
    templateUrl  : './add-result-category.component.html',
    styleUrls    : ['./add-result-category.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class AddResultCategoryComponent implements OnInit
{
    selected: any;
    pathArr: string[];

    constructor(private AddResultCategoryService: AddResultCategoryService, private router: Router)
    {
    }

    ngOnInit()
    {
        
    }

    openResultCategoryList(){
        
        this.router.navigateByUrl("pages/result-category/list");
    }
}
