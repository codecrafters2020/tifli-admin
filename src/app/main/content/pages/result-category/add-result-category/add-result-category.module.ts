import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CdkTableModule } from '@angular/cdk/table';

import { MatFormFieldModule, MatInputModule, MatSelectModule, MatStepperModule, MatButtonModule, MatIconModule, MatRippleModule, MatSidenavModule, MatSlideToggleModule, MatTableModule } from '@angular/material';
import {MatChipsModule} from '@angular/material/chips';
import {MatAutocompleteModule} from '@angular/material/autocomplete';

import { FuseSharedModule } from '@fuse/shared.module';

import { AddResultCategoryComponent } from './add-result-category.component';
import { AddResultCategoryService } from './add-result-category.service';
import {FormAddResultCategoryComponent} from './form/form-add-result-category.component';

const routes: Routes = [
    {
        path     : '**',
        component: AddResultCategoryComponent,
        children : [],
        resolve  : {
            files: AddResultCategoryService
        }
    }
];

@NgModule({
    declarations: [
        AddResultCategoryComponent,
        FormAddResultCategoryComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        CdkTableModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatButtonModule,
        MatIconModule,
        MatRippleModule,
        MatSlideToggleModule,
        MatSidenavModule,
        MatTableModule,
        MatChipsModule,
        MatAutocompleteModule,

        FuseSharedModule
    ],
    providers   : [
        AddResultCategoryService
    ]
})
export class AddResultCategoryModule
{
}
