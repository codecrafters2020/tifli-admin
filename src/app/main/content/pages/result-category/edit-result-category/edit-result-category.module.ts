import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CdkTableModule } from '@angular/cdk/table';

import { MatFormFieldModule, MatInputModule, MatSelectModule, MatStepperModule, MatButtonModule, MatIconModule, MatRippleModule, MatSidenavModule, MatSlideToggleModule, MatTableModule, MatCheckboxModule} from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';

import { EditResultCategoryComponent } from './edit-result-category.component';
import { EditResultCategoryService } from './edit-result-category.service';
import { FormEditResultCategoryComponent } from './form/form-edit-result-categorycomponent';
import { FancyImageUploaderModule } from 'ng2-fancy-image-uploader';
import {MatExpansionModule} from '@angular/material/expansion';


const routes: Routes = [
    {
        path     : '**',
        component:EditResultCategoryComponent,
        children : [],
        resolve  : {
            merchantById : EditResultCategoryService
        }
    }
];

@NgModule({
    declarations: [
        EditResultCategoryComponent,
        FormEditResultCategoryComponent,
    ],
    imports     : [
        RouterModule.forChild(routes),

        CdkTableModule,
        MatFormFieldModule,
        MatExpansionModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatButtonModule,
        MatIconModule,
        MatRippleModule,
        MatSlideToggleModule,
        MatSidenavModule,
        MatTableModule,
        FancyImageUploaderModule,
        MatCheckboxModule,

        FuseSharedModule
    ],
    providers   : [
        EditResultCategoryService
    ]
})
export class EditResultCategoryModule
{
}
