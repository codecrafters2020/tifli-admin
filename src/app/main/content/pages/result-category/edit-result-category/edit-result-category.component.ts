import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';

import { EditResultCategoryService } from './edit-result-category.service';

import { Router } from '@angular/router';

@Component({
    selector     : 'fuse-file-manager',
    templateUrl  : './edit-result-category.component.html',
    styleUrls    : ['./edit-result-category.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class EditResultCategoryComponent implements OnInit
{
    selected: any;
    pathArr: string[];

    constructor(private EditResultCategoryService: EditResultCategoryService, private router: Router)
    {
    }

    ngOnInit()
    {
        // this.AddMerchantService.onFileSelected.subscribe(selected => {
        //     this.selected = selected;
        //     this.pathArr = selected.location.split('>');
        // });
    }

    openResultCategoryList(){
        
        this.router.navigateByUrl("pages/result-category/list/");
    }
}
