import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FuseAngularMaterialModule } from '../../components/angular-material/angular-material.module';

import { FuseSharedModule } from '@fuse/shared.module';

const routes = [
    {
        path        : 'list',
        loadChildren: './list-result-category/list-result-category.module#ListResultCategoryModule'
    },
    {
        path        : 'add',
        loadChildren: './add-result-category/add-result-category.module#AddResultCategoryModule'
    },
    {
        path        : 'edit/:id',
        loadChildren: './edit-result-category/edit-result-category.module#EditResultCategoryModule'
    }
];

@NgModule({
    imports     : [
        FuseSharedModule,
        RouterModule.forChild(routes),
        FuseAngularMaterialModule
    ],
    declarations: []
})
export class ResultCategoryModule
{
}
