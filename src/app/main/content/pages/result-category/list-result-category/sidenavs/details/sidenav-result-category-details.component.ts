import { Component, OnInit } from '@angular/core';

import { fuseAnimations } from '@fuse/animations/index';

import { ListResultCategoryService } from '../../list-result-category.service';

@Component({
    selector   : 'fuse-file-manager-details-sidenav',
    templateUrl: './sidenav-result-category-details.component.html',
    styleUrls  : ['./sidenav-result-category-details.component.scss'],
    animations : fuseAnimations
})
export class SidenavResultCategoryDetailsComponent implements OnInit
{

    selected: any;

    constructor(private ListResultCategoryService: ListResultCategoryService)
    {

    }

    ngOnInit()
    {
        this.ListResultCategoryService.onFileSelected.subscribe(selected => {
            this.selected = selected;
        });
    }

}
