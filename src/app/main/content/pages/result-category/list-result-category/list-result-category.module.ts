import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CdkTableModule } from '@angular/cdk/table';

import { MatAutocompleteModule, MatFormFieldModule, MatInputModule, MatSelectModule, MatStepperModule, MatButtonModule, MatIconModule, MatRippleModule, MatSidenavModule, MatSlideToggleModule, MatTableModule, MatMenuModule, MatPaginatorModule, MatSortModule } from '@angular/material';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatRadioModule} from '@angular/material/radio';
import { FuseSharedModule } from '@fuse/shared.module';

import { ListResultCategoryComponent } from './list-result-category.component';
import { ListResultCategoryService } from './list-result-category.service';
import { GridResultCategoryComponent } from './grid/grid-result-category.component';
import { SidenavResultCategoryMainComponent } from './sidenavs/main/sidenav-result-category-main.component';
import { SidenavResultCategoryDetailsComponent } from './sidenavs/details/sidenav-result-category-details.component';

const routes: Routes = [
    {
        path     : '**',
        component: ListResultCategoryComponent,
        children : [],
        resolve  : {
            files: ListResultCategoryService
        }
    }
];

@NgModule({
    declarations: [
        ListResultCategoryComponent,
        GridResultCategoryComponent,
        SidenavResultCategoryMainComponent,
        SidenavResultCategoryDetailsComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        CdkTableModule,
        MatAutocompleteModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatButtonModule,
        MatIconModule,
        MatRippleModule,
        MatSlideToggleModule,
        MatSidenavModule,
        MatTableModule,
        MatMenuModule,
        MatPaginatorModule, 
        MatSortModule,
        MatProgressSpinnerModule,

        FuseSharedModule,
        MatDatepickerModule,
        MatCheckboxModule,
        MatRadioModule
    ],
    providers   : [
        ListResultCategoryService
    ]
})
export class ListResultCategoryModule
{
}
