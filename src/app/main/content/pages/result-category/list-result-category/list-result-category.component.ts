import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, FormArray, Validators, EmailValidator } from '@angular/forms';
import { fuseAnimations } from '@fuse/animations';

import { ListResultCategoryService } from './list-result-category.service';
import { Router } from '@angular/router';

@Component({
    selector: 'fuse-file-manager',
    templateUrl: './list-result-category.component.html',
    styleUrls: ['./list-result-category.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class ListResultCategoryComponent implements OnInit {
    selected: any;
    pathArr: string[];
    isSearch: boolean = false;
    form: FormGroup;
    merchantSearchResult: any[];


    constructor(private ListResultCategoryService: ListResultCategoryService, private router: Router, private formBuilder: FormBuilder) {
      //  this.ListCouponService.getCoupons();
        this.form = this.formBuilder.group({
            merchantName : ['', ]})

    }

    ngOnInit() {
        this.ListResultCategoryService.onFileSelected.subscribe(selected => {
            this.selected = selected;
            // this.pathArr = selected.location.split('>');
        });

    }

    openCouponAdd() {
        this.router.navigateByUrl("pages/result-category/add");

    }

    resetGrid() {
        this.ListResultCategoryService.getResultCategory();
    }
    
    searching() {

        if(this.isSearch == false){
            this.isSearch = true;
        }

        else{
            this.isSearch = false;
            console.log("close it")
        }
    }
}
