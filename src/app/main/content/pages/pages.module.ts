import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { LoginModule } from './authentication/login/login.module';
import { Login2Module } from './authentication/login-2/login-2.module';
import { RegisterModule } from './authentication/register/register.module';
import { Register2Module } from './authentication/register-2/register-2.module';
import { ForgotPasswordModule } from './authentication/forgot-password/forgot-password.module';
import { ForgotPassword2Module } from './authentication/forgot-password-2/forgot-password-2.module';
import { ResetPasswordModule } from './authentication/reset-password/reset-password.module';
import { ResetPassword2Module } from './authentication/reset-password-2/reset-password-2.module';
import { LockModule } from './authentication/lock/lock.module';
import { MailConfirmModule } from './authentication/mail-confirm/mail-confirm.module';
import { Error404Module } from './errors/404/error-404.module';
import { Error500Module } from './errors/500/error-500.module';
import {MatTooltipModule} from '@angular/material/tooltip';

import { AuthGuard } from '../../../auth/auth.guard'

const routes = [
    {
        path        : 'survey',
        loadChildren: './survey/survey.module#SurveyModule',
        // canActivateChild: [AuthGuard]
    },
    {
        path        : 'notice',
        loadChildren: './notice/notice.module#NoticeModule'
        // canActivateChild: [AuthGuard]
    },
    {
        path        : 'keys',
        loadChildren: './progress-evaluation-keys/keys.module#KeysModule'
        // canActivateChild: [AuthGuard]
    },
    {
        path        : 'account/logout',
        loadChildren: './logout/logout.module#LogoutModule'
    },
    {
        path        : 'faculty',
        loadChildren: './faculty/faculty.module#FacultyModule',
        // canActivateChild: [AuthGuard]
    },
    {
        path        : 'events',
        loadChildren: './events/events.module#EventsModule',
        // canActivateChild: [AuthGuard]
    },
    {
        path        : 'fees',
        loadChildren: './fees/fees.module#FeesModule',
        // canActivateChild: [AuthGuard]
    },

    {
        path        : 'classes',
        loadChildren: './classes/classes.module#ClassesModule',
        // canActivateChild: [AuthGuard]
    },
    {
        path        : 'courses',
        loadChildren: './courses/courses.module#CoursesModule',
        // canActivateChild: [AuthGuard]
    },
   
    {
        path        : 'student',
        loadChildren: './student/student.module#StudentModule',
        // canActivateChild: [AuthGuard]
    },
    {
        path        : 'timetable',
        loadChildren: './scheduled-placement-upcoming-sales/scheduled-placement-upcoming-sales.module#ScheduledPlacementUpcomingSalesModule'
    },
   
    {
        path        : 'lists',
        loadChildren: './lists/lists.module#ListsModule'
    },
    {
        path        : 'result-category',
        loadChildren: './result-category/result-category.module#ResultCategoryModule',
        // canActivateChild: [AuthGuard]
    },
    
    {
        path        : 'progress-category',
        loadChildren: './progress-category/progress-category.module#ProgressCategoryModule',
        // canActivateChild: [AuthGuard]
    }
    
];

@NgModule({
    imports: [
        // Auth
        LoginModule,
        MatTooltipModule,
        Login2Module,
        RegisterModule,
        Register2Module,
        ForgotPasswordModule,
        ForgotPassword2Module,
        ResetPasswordModule,
        ResetPassword2Module,
        LockModule,
        MailConfirmModule,

        // Coming-soon
        

        // Errors
        Error404Module,
        Error500Module,

        // Invoices
        
        // Faq
        
        // Seasonal Events
        // SeasonalModule,

        RouterModule.forChild(routes),

        // MerchantModule,
        // AdministrationModule,
        // PaidPlacementsModule        
    ]
})
export class FusePagesModule
{

}
