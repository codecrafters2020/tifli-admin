import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { FuseCalendarEventFormDialogComponent } from './slot-form/slot-form.component';
import { CalendarEvent, CalendarEventAction, CalendarEventTimesChangedEvent, CalendarMonthViewDay, CalendarWeekViewComponent } from 'angular-calendar';

import { fuseAnimations } from '@fuse/animations';
import { MAT_DIALOG_DATA, MatDialogRef,MatDialog } from '@angular/material';
import { ListMerchantService } from './list-merchant.service';
import { Router } from '@angular/router';
import { AbstractControl, FormBuilder, FormGroup, Validators, FormsModule, FormControl } from '@angular/forms';
import {GridMerchantComponent} from './grid/grid-merchant.component';

@Component({
    selector     : 'fuse-file-manager',
    templateUrl  : './list-merchant.component.html',
    styleUrls    : ['./list-merchant.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class ListMerchantComponent implements OnInit
{

    view: string;
    viewDate: Date;
    selected: any;
    pathArr: string[];
    dialogRef:any;
    selectedPage: any
    placementShedulingForm: FormGroup;
    showLoadingBar:Boolean = false;
    pages: any[] = [];  
    classes: any;
    selectedClass: any = 0;
    @ViewChild(CalendarWeekViewComponent)
    weekView: CalendarWeekViewComponent;
    className: any = 'Class Name';
    course_section: any;
    dashboard: any;
    constructor(private ListMerchantService: ListMerchantService,public grid: GridMerchantComponent, private formBuilder: FormBuilder, private router: Router, public dialog: MatDialog)
    {
        this.classes = this.ListMerchantService.classes;
       
        // this.pages = ListMerchantService.pages;
        // this.view = 'week';
        // this.viewDate = new Date();
        // this.selectedPage = this.ListMerchantService.pages[0];
    }

    ngOnInit()
    {
        this.dashboard = [
            { cols: 1, rows: 1, y: 0, x: 0, data: {start_time:'',end_time: '',x: 0,y: 0,section_course_id:0,section_course_name:'',section_course_teacher:'',className: '',id:null}},
            { cols: 1, rows: 1, y: 0, x: 1, data: {start_time:'',end_time: '',x: 0,y: 0,section_course_id:0,section_course_name:'',section_course_teacher:'',className: '',id:null} },
            { cols: 1, rows: 1, y: 0, x: 2, data: {start_time:'',end_time: '',x: 0,y: 0,section_course_id:0,section_course_name:'',section_course_teacher:'',className: '',id:null} },
            { cols: 1, rows: 1, y: 0, x: 3, data: {start_time:'',end_time: '',x: 0,y: 0,section_course_id:0,section_course_name:'',section_course_teacher:'',className: '',id:null} },
            { cols: 1, rows: 1, y: 0, x: 4,  data: {start_time:'',end_time: '',x: 0,y: 0,section_course_id:0,section_course_name:'',section_course_teacher:'',className: '',id:null} },
            { cols: 1, rows: 1, y: 0, x: 5, data: {start_time:'',end_time: '',x: 0,y: 0,section_course_id:0,section_course_name:'',section_course_teacher:'',className: '',id:null} },
            { cols: 1, rows: 1, y: 0, x: 6, data: {start_time:'',end_time: '',x: 0,y: 0,section_course_id:0,section_course_name:'',section_course_teacher:'',className: '',id:null}},
            { cols: 1, rows: 1, y: 1, x: 0, data: {start_time:'',end_time: '',x: 0,y: 0,section_course_id:0,section_course_name:'',section_course_teacher:'',className: '',id:null}},
            { cols: 1, rows: 1, y: 1, x: 1, data: {start_time:'',end_time: '',x: 0,y: 0,section_course_id:0,section_course_name:'',section_course_teacher:'',className: '',id:null}},
            { cols: 1, rows: 1, y: 1, x: 2, data: {start_time:'',end_time: '',x: 0,y: 0,section_course_id:0,section_course_name:'',section_course_teacher:'',className: '',id:null} },
            { cols: 1, rows: 1, y: 1, x: 3, data: {start_time:'',end_time: '',x: 0,y: 0,section_course_id:0,section_course_name:'',section_course_teacher:'',className: '',id:null} },
            { cols: 1, rows: 1, y: 1, x: 4, data: {start_time:'',end_time: '',x: 0,y: 0,section_course_id:0,section_course_name:'',section_course_teacher:'',className: '',id:null} },
            { cols: 1, rows: 1, y: 1, x: 5, data: {start_time:'',end_time: '',x: 0,y: 0,section_course_id:0,section_course_name:'',section_course_teacher:'',className: '',id:null}},
            { cols: 1, rows: 1, y: 1, x: 6, data: {start_time:'',end_time: '',x: 0,y: 0,section_course_id:0,section_course_name:'',section_course_teacher:'',className: '',id:null} },
            { cols: 1, rows: 1, y: 2, x: 0, data: {start_time:'',end_time: '',x: 0,y: 0,section_course_id:0,section_course_name:'',section_course_teacher:'',className: '',id:null} },
            { cols: 1, rows: 1, y: 2, x: 1, data: {start_time:'',end_time: '',x: 0,y: 0,section_course_id:0,section_course_name:'',section_course_teacher:'',className: '',id:null} },
            { cols: 1, rows: 1, y: 2, x: 2, data: {start_time:'',end_time: '',x: 0,y: 0,section_course_id:0,section_course_name:'',section_course_teacher:'',className: '',id:null}},
            { cols: 1, rows: 1, y: 2, x: 3, data: {start_time:'',end_time: '',x: 0,y: 0,section_course_id:0,section_course_name:'',section_course_teacher:'',className: '',id:null}},
            { cols: 1, rows: 1, y: 2, x: 4, data: {start_time:'',end_time: '',x: 0,y: 0,section_course_id:0,section_course_name:'',section_course_teacher:'',className: '',id:null} },
            { cols: 1, rows: 1, y: 2, x: 5, data: {start_time:'',end_time: '',x: 0,y: 0,section_course_id:0,section_course_name:'',section_course_teacher:'',className: '',id:null} },
            { cols: 1, rows: 1, y: 2, x: 6, data: {start_time:'',end_time: '',x: 0,y: 0,section_course_id:0,section_course_name:'',section_course_teacher:'',className: '',id:null}},
            { cols: 1, rows: 1, y: 3, x: 0, data: {start_time:'',end_time: '',x: 0,y: 0,section_course_id:0,section_course_name:'',section_course_teacher:'',className: '',id:null}},
            { cols: 1, rows: 1, y: 3, x: 1, data: {start_time:'',end_time: '',x: 0,y: 0,section_course_id:0,section_course_name:'',section_course_teacher:'',className: '',id:null}},
            { cols: 1, rows: 1, y: 3, x: 2, data: {start_time:'',end_time: '',x: 0,y: 0,section_course_id:0,section_course_name:'',section_course_teacher:'',className: '',id:null}},
            { cols: 1, rows: 1, y: 3, x: 3, data: {start_time:'',end_time: '',x: 0,y: 0,section_course_id:0,section_course_name:'',section_course_teacher:'',className: '',id:null} },
            { cols: 1, rows: 1, y: 3, x: 4, data: {start_time:'',end_time: '',x: 0,y: 0,section_course_id:0,section_course_name:'',section_course_teacher:'',className: '',id:null}},
            { cols: 1, rows: 1, y: 3, x: 5, data: {start_time:'',end_time: '',x: 0,y: 0,section_course_id:0,section_course_name:'',section_course_teacher:'',className: '',id:null}},
            { cols: 1, rows: 1, y: 3, x: 6, data: {start_time:'',end_time: '',x: 0,y: 0,section_course_id:0,section_course_name:'',section_course_teacher:'',className: '',id:null} },
            { cols: 1, rows: 1, y: 4, x: 0, data: {start_time:'',end_time: '',x: 0,y: 0,section_course_id:0,section_course_name:'',section_course_teacher:'',className: '',id:null}},
            { cols: 1, rows: 1, y: 4, x: 1, data: {start_time:'',end_time: '',x: 0,y: 0,section_course_id:0,section_course_name:'',section_course_teacher:'',className: '',id:null}},
            { cols: 1, rows: 1, y: 4, x: 2, data: {start_time:'',end_time: '',x: 0,y: 0,section_course_id:0,section_course_name:'',section_course_teacher:'',className: '',id:null}},
            { cols: 1, rows: 1, y: 4, x: 3, data: {start_time:'',end_time: '',x: 0,y: 0,section_course_id:0,section_course_name:'',section_course_teacher:'',className: '',id:null}},
            { cols: 1, rows: 1, y: 4, x: 4, data: {start_time:'',end_time: '',x: 0,y: 0,section_course_id:0,section_course_name:'',section_course_teacher:'',className: '',id:null} },
            { cols: 1, rows: 1, y: 4, x: 5, data: {start_time:'',end_time: '',x: 0,y: 0,section_course_id:0,section_course_name:'',section_course_teacher:'',className: '',id:null} },
            { cols: 1, rows: 1, y: 4, x: 6, data: {start_time:'',end_time: '',x: 0,y: 0,section_course_id:0,section_course_name:'',section_course_teacher:'',className: '',id:null}},
            { cols: 1, rows: 1, y: 5, x: 0, data: {start_time:'',end_time: '',x: 0,y: 0,section_course_id:0,section_course_name:'',section_course_teacher:'',className: '',id:null} },
            { cols: 1, rows: 1, y: 5, x: 1, data: {start_time:'',end_time: '',x: 0,y: 0,section_course_id:0,section_course_name:'',section_course_teacher:'',className: '',id:null}},
            { cols: 1, rows: 1, y: 5, x: 2, data: {start_time:'',end_time: '',x: 0,y: 0,section_course_id:0,section_course_name:'',section_course_teacher:'',className: '',id:null}},
            { cols: 1, rows: 1, y: 5, x: 3, data: {start_time:'',end_time: '',x: 0,y: 0,section_course_id:0,section_course_name:'',section_course_teacher:'',className: '',id:null}},
            { cols: 1, rows: 1, y: 5, x: 4, data: {start_time:'',end_time: '',x: 0,y: 0,section_course_id:0,section_course_name:'',section_course_teacher:'',className: '',id:null}},
            { cols: 1, rows: 1, y: 5, x: 5, data: {start_time:'',end_time: '',x: 0,y: 0,section_course_id:0,section_course_name:'',section_course_teacher:'',className: '',id:null}},
            { cols: 1, rows: 1, y: 5, x: 6, data: {start_time:'',end_time: '',x: 0,y: 0,section_course_id:0,section_course_name:'',section_course_teacher:'',className: '',id:null}},
            { cols: 1, rows: 1, y: 6, x: 0, data: {start_time:'',end_time: '',x: 0,y: 0,section_course_id:0,section_course_name:'',section_course_teacher:'',className: '',id:null}},
            { cols: 1, rows: 1, y: 6, x: 1, data: {start_time:'',end_time: '',x: 0,y: 0,section_course_id:0,section_course_name:'',section_course_teacher:'',className: '',id:null}},
            { cols: 1, rows: 1, y: 6, x: 2, data: {start_time:'',end_time: '',x: 0,y: 0,section_course_id:0,section_course_name:'',section_course_teacher:'',className: '',id:null} },
            { cols: 1, rows: 1, y: 6, x: 3, data: {start_time:'',end_time: '',x: 0,y: 0,section_course_id:0,section_course_name:'',section_course_teacher:'',className: '',id:null}},
            { cols: 1, rows: 1, y: 6, x: 4, data: {start_time:'',end_time: '',x: 0,y: 0,section_course_id:0,section_course_name:'',section_course_teacher:'',className: '',id:null}},
            { cols: 1, rows: 1, y: 6, x: 5, data: {start_time:'',end_time: '',x: 0,y: 0,section_course_id:0,section_course_name:'',section_course_teacher:'',className: '',id:null}},
            { cols: 1, rows: 1, y: 6, x: 6, data: {start_time:'',end_time: '',x: 0,y: 0,section_course_id:0,section_course_name:'',section_course_teacher:'',className: '',id:null} },
            { cols: 1, rows: 1, y: 7, x: 0, data: {start_time:'',end_time: '',x: 0,y: 0,section_course_id:0,section_course_name:'',section_course_teacher:'',className: '',id:null}},
            { cols: 1, rows: 1, y: 7, x: 1, data: {start_time:'',end_time: '',x: 0,y: 0,section_course_id:0,section_course_name:'',section_course_teacher:'',className: '',id:null} },
            { cols: 1, rows: 1, y: 7, x: 2, data: {start_time:'',end_time: '',x: 0,y: 0,section_course_id:0,section_course_name:'',section_course_teacher:'',className: '',id:null} },
            { cols: 1, rows: 1, y: 7, x: 3, data: {start_time:'',end_time: '',x: 0,y: 0,section_course_id:0,section_course_name:'',section_course_teacher:'',className: '',id:null}},
            { cols: 1, rows: 1, y: 7, x: 4, data: {start_time:'',end_time: '',x: 0,y: 0,section_course_id:0,section_course_name:'',section_course_teacher:'',className: '',id:null} },
            { cols: 1, rows: 1, y: 7, x: 5, data: {start_time:'',end_time: '',x: 0,y: 0,section_course_id:0,section_course_name:'',section_course_teacher:'',className: '',id:null} },
            { cols: 1, rows: 1, y: 7, x: 6, data: {start_time:'',end_time: '',x: 0,y: 0,section_course_id:0,section_course_name:'',section_course_teacher:'',className: '',id:null} },
            { cols: 1, rows: 1, y: 8, x: 0,data: {start_time:'',end_time: '',x: 0,y: 0,section_course_id:0,section_course_name:'',section_course_teacher:'',className: '',id:null} },
            { cols: 1, rows: 1, y: 8, x: 1, data: {start_time:'',end_time: '',x: 0,y: 0,section_course_id:0,section_course_name:'',section_course_teacher:'',className: '',id:null}},
            { cols: 1, rows: 1, y: 8, x: 2, data: {start_time:'',end_time: '',x: 0,y: 0,section_course_id:0,section_course_name:'',section_course_teacher:'',className: '',id:null}},
            { cols: 1, rows: 1, y: 8, x: 3, data: {start_time:'',end_time: '',x: 0,y: 0,section_course_id:0,section_course_name:'',section_course_teacher:'',className: '',id:null}},
            { cols: 1, rows: 1, y: 8, x: 4, data: {start_time:'',end_time: '',x: 0,y: 0,section_course_id:0,section_course_name:'',section_course_teacher:'',className: '',id:null}},
            { cols: 1, rows: 1, y: 8, x: 5, data: {start_time:'',end_time: '',x: 0,y: 0,section_course_id:0,section_course_name:'',section_course_teacher:'',className: '',id:null}},
            { cols: 1, rows: 1, y: 8, x: 6, data: {start_time:'',end_time: '',x: 0,y: 0,section_course_id:0,section_course_name:'',section_course_teacher:'',className: '',id:null} }
            ];
            this.dashboard.forEach(function(value){
               
                value.data.start_time ='00:00',
                value.data.end_time = '12:59',
                value.data.section_course_name ='Course Name',
                value.data.section_course_teacher ='Course Teacher',
                value.data.className = 'Class Name'
              });   
         this.ListMerchantService.dashboard = this.dashboard;
        // this.ListMerchantService.onFileSelected.subscribe(selected => {
        //     this.selected = selected;
        //    // this.pathArr = selected.location.split('>');
        // });
        this.placementShedulingForm = this.formBuilder.group({
            selectPage: ['', Validators.required],
        });

        // this.placementShedulingForm.controls.selectPage.valueChanges
        // .subscribe(page=>{
        //     this.ListMerchantService.showLoadingBar = true;
        //     this.ListMerchantService.onShowLoadingBarChanged.next(true);
        //     this.ListMerchantService.getScheduledPlacement(page.id, null, null);

        // });

    }
    onChange(){
        
        this.className  = this.classes.filter(obj => obj.id == this.selectedClass);
         this.className = this.className[0].school_class.class_name.concat(this.className[0].section.section_name);
         this.ListMerchantService.className = this.className ;
         this.ListMerchantService.onclassNameChanged.next(this.className);

         let that = this;
         this.ListMerchantService.getClass(this.selectedClass).then(response => {
           this.course_section= response.body;
           
           if(that.course_section.time_tables){
               
               if(typeof that.course_section.time_tables != "undefined" && that.course_section.time_tables != null && that.course_section.time_tables.length > 0){
                
                that.course_section.time_tables.forEach(element => {
                    for(let i = 0; i < that.dashboard.length; i++){
                        if(element.x == that.dashboard[i].x && element.y == that.dashboard[i].y ){
                            debugger;
                            var str = element.start_time;
                            var res = str.split("T");
                            var time = res[1].split(".");
                            time[0]= time[0].substring(0,5);
                            that.dashboard[i].data.start_time = time[0];
                            var str = element.end_time;
                            var res = str.split("T");
                            var time = res[1].split(".");
                            time[0]= time[0].substring(0,5);
                            that.dashboard[i].data.end_time = time[0]; 
                            that.dashboard[i].data.section_course_id = element.section_course.id;
                            that.dashboard[i].data.section_course_name =element.section_course.course.course_name ;
                            that.dashboard[i].data.section_course_teacher= element.section_course.course_teacher_name;
                            that.dashboard[i].data.id = element.id;
                            that.dashboard[i].data.x = element.x;
                            that.dashboard[i].data.y = element.y;
                        }
                        else if(element.y == that.dashboard[i].y  ){
                            var str = element.start_time;
                            var res = str.split("T");
                            var time = res[1].split(".");
                            time[0]= time[0].substring(0,5);
                            that.dashboard[i].data.start_time = time[0];
                            var str = element.end_time;
                            var res = str.split("T");
                            var time = res[1].split(".");
                            time[0]= time[0].substring(0,5);
                            that.dashboard[i].data.end_time = time[0]; 
                            
                          }
                    }
                });
 
               }
               else{
                   
                    that.ngOnInit();}
          
           }
         else{
               
               that.ngOnInit();
           }
           this.ListMerchantService.course_section = this.course_section ;
           this.ListMerchantService.onCourseNameChanged.next(this.course_section);
           this.ListMerchantService.onDashBoardChanged.next(this.dashboard);
           this.ListMerchantService.dashboard = this.dashboard;
        // this.grid.item(this.course_section,this.className);
      });

        
    }
    

    

    nextweek(){
        this.ListMerchantService.showLoadingBar = true;
        this.ListMerchantService.onShowLoadingBarChanged.next(true);
        var pend = this.ListMerchantService.endDate;
       
        var pstart=new Date(pend);
       pstart.setDate(pstart.getDate() +1);
       
        pend =new Date(pstart);
       pend.setDate(pend.getDate() +6);

        let pageid= this.ListMerchantService.pageid;
        console.log(pstart)
        console.log(pend)

       this.ListMerchantService.getScheduledPlacement(pageid, pstart, pend);
    }
    
    openUserAdd(){
        this.router.navigateByUrl("pages/merchant/add/");
    }

    previousweek(){
        this.ListMerchantService.showLoadingBar = true;
        this.ListMerchantService.onShowLoadingBarChanged.next(true);
        var pstart = this.ListMerchantService.startDate;
        
        var pend=new Date(pstart);
       pend.setDate(pend.getDate() -1);
       
        pstart =new Date(pend);
       pstart.setDate(pstart.getDate() -6);

        let pageid= this.ListMerchantService.pageid;
        console.log(pstart)
        console.log(pend)

       this.ListMerchantService.getScheduledPlacement(pageid, pstart, pend);
    }
    
}
