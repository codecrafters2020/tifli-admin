import { Component, Inject, ViewEncapsulation, AfterViewChecked } from '@angular/core';
import { FormBuilder, FormControl, Validators, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material';

import { MatColors } from '@fuse/mat-colors';

import { CalendarEvent } from 'angular-calendar';
import { AppService } from './../../../../../../app.service';
import { ListMerchantService } from '../list-merchant.service';
import { invalid } from 'moment';
import { DISABLED } from '@angular/forms/src/model';
// import { SlotModel } from '../slot.model';
// import { ScheduledPlacementService} from '../scheduled-placement.service';
import { AmazingTimePickerService } from 'amazing-time-picker';
@Component({
    selector: 'fuse-calendar-event-form-dialog',
    templateUrl: './slot-form.component.html',
    styleUrls: ['./slot-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class FuseCalendarEventFormDialogComponent {

    btndisable: boolean = false;
    /** Meta Data for Carousel Popup */
    public selectedTime: string;
    


    /** Meta data ended */

    /** Slot Object */
    slot: any = {
        start_time:'',
        end_time: '',
        x: 0,
        y: 0,
        section_course_id:0,
        section_course_name:'',
        section_course_teacher:'',
        className: '',
        id:null
    };

    filterArray : any[];

    dialogTitle: string = "Add Slot";
    slotForm: FormGroup;
    action: string = "add";

    
    buttonAction: any = "add";
    file_access: any;

    constructor(
        public dialogRef: MatDialogRef<FuseCalendarEventFormDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private formBuilder: FormBuilder,
        private carouselPlacementService: ListMerchantService,
        private appService: AppService,
        private atp: AmazingTimePickerService
        // private scheduledPlacementService: ScheduledPlacementService
    ) {
        this.slot.x = this.data.x;
        this.slot.y = this.data.y;
        this.slot.className = this.data.class;
        this.slot.start_time = this.data.start_time;
        this.slot.end_time = this.data.end_time;
        debugger;
        this.slot.section_course_id = this.data.course_id;
        this.slot.section_course_teacher =this.data.teacher_name;
        this.slot.id = this.data.id;
        this.slot.section_course_name = this.data.course_name;
        
        
        
    }
    onChange(){
        debugger;
        this.filterArray = this.data.data.section_courses.filter(obj => obj.id == this.slot.section_course_id);
        this.slot.section_course_name = this.filterArray[0].course.course_name;
        if(!this.filterArray[0].course_teacher){
            this.slot.section_course_teacher = 'no teacher assigned';
        }
        else{
            this.slot.section_course_teacher = this.filterArray[0].course_teacher_name;
        }
      
    }
    
    ngOnInit()
    {
        // Reactive Form
        this.slotForm = this.formBuilder.group({
            class_section: ['', [Validators.required]],
            start_time: ['', [Validators.required]],
            end_time: ['', [Validators.required]],
            course_name: ['', [Validators.required]],
            course_teacher: ['', [Validators.required]]
            
           
        });

       
        
        

    }
    



    closeDialog(action) {
        debugger;
         if(action=='cancel'){
            this.dialogRef.close();
         }   
         else{
             this.slot.start_time = this.slot.start_time;
             this.slot.end_time = this.slot.end_time;     
             this.dialogRef.close(this.slot);
         }
         
    }

   

}
