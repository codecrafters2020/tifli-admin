import { Input, Component, ElementRef, ChangeDetectionStrategy, ViewEncapsulation, OnInit, ViewChild, AfterViewInit, OnChanges } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { DataSource } from '@angular/cdk/collections';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material';
import { ListMerchantService } from '../list-merchant.service';
import { fuseAnimations } from '@fuse/animations';
import { FuseUtils } from '@fuse/utils';
import { FuseCalendarEventFormDialogComponent } from '../slot-form/slot-form.component';
import { DisplayGrid, GridsterConfig, GridsterItem, GridType } from 'angular-gridster2';
import { resetState } from 'sweetalert/typings/modules/state';
import { MatSnackBar } from '@angular/material';
import { HttpClient } from '@angular/common/http';
import { AppService } from '../../../../../../app.service';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import { SnackBarComponentExample } from 'assets/angular-material-examples/snack-bar-component/snack-bar-component-example';

@Component({
  selector: 'app-swap',
  templateUrl: './grid-merchant.component.html',
  styleUrls: ['./grid-merchant.component.scss'],
  //changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None
})
export class GridMerchantComponent implements OnInit {
  static item: BehaviorSubject<any> = new BehaviorSubject({});selectedClass: any;

  slotsData: any[] = [];
  options: GridsterConfig;
  dashboard: Array<GridsterItem>;
  newdashboard: Array<GridsterItem> = [];
  dialogRef: any;
  swapCounter: any = 0;
  that:any;
  static swapChecker: any = 0;
  @Input() showLoadingBar: any;
  @Input() page: any;
  classes: any;
  course_section: any;
  className: any;
  itemArray : any=[];
  dashboardObj: any=[];
  timetable : any=[];
  //snackbar: SnackBarComponentExample

  constructor(public ListMerchantService: ListMerchantService, public dialog: MatDialog,private router: Router, public snackBar: MatSnackBar, private http: HttpClient, private appService: AppService) {

    this.classes = this.ListMerchantService.classes;
    
    debugger;
    // this.course_section = this.ListMerchantComponent.course_section;
    // this.className = this.ListMerchantComponent.className;

  }

   itemChange(item, ListMerchantService) {
    this.that.itemArray.push(item);
    GridMerchantComponent.item.next(item);
    this.that.swapCounter++;
    if(this.that.swapCounter ==2){
      this.that.timeSwap();
    }
  }
  timeSwap(){
    var temp_start = this.itemArray[0].data.start_time;
    var temp_end = this.itemArray[0].data.end_time;
    this.itemArray[0].data.start_time = this.itemArray[1].data.start_time;
    this.itemArray[0].data.end_time = this.itemArray[1].data.end_time;
    this.itemArray[1].data.start_time = temp_start;
    this.itemArray[1].data.end_time = temp_end;
    this.swapCounter = 0;
    this.itemArray = [];
  }


 

  ngOnInit() {
    this.ListMerchantService.onShowLoadingBarChanged.subscribe(data => {
      this.showLoadingBar = data;
    })

    GridMerchantComponent.item.subscribe(slotData => {
      
    
    });

    let that = this;
    this.ListMerchantService.onclassNameChanged.subscribe(data => {
      this.className = data;})
    
      this.ListMerchantService.onDashBoardChanged.subscribe(data => {
        this.dashboard = data;
       
  
              })
  
      this.ListMerchantService.onCourseNameChanged.subscribe(data => {
        this.course_section = data;
       
  
              })
  
      

    this.ListMerchantService.onSlotsDataChanged.subscribe(data => {
      this.slotsData = data;
      debugger;
      this.pagechange();
      this.ListMerchantService.showLoadingBar = false;
      this.ListMerchantService.onShowLoadingBarChanged.next(false);
    })
    debugger;
    
    this.className = this.ListMerchantService.className;
     this.that = this;
    this.options = {
    
      itemChangeCallback: this.itemChange,
      gridType: GridType.ScrollVertical,
      displayGrid: DisplayGrid.None,
      pushItems: false,
      swap: true,
      draggable: {
        enabled: true
      },
      resizable: {
        enabled: false
      },
      maxRows: 15,
      that :this
    };


   
  }
  refresh(): void {
    window.location.reload();
}
  
 
  

  pagechange() {
   //console.log(this.timetable )

    this.dashboard = this.ListMerchantService.dashboard;
    this.dashboard.forEach(function(value){
      debugger;
      value.data.start_time ='00:00',
      value.data.end_time = '12:59',
      value.data.section_course_name ='Course Name',
      value.data.section_course_teacher ='Course Teacher',
      value.data.className = 'Class Name'
    });
  }
  onSubmit(){
    let that = this;
    this.dashboard.forEach(function(value){
      if(value.data.section_course_id != 0 ){
        value.data.x = value.x;
        value.data.y = value.y;
        that.dashboardObj.push(value.data);
      }
      
    })
    try{
      let obj = {
        'dashboard' : this.dashboardObj
      }
      this.ListMerchantService.addTimetable(obj).then(response => {
          debugger;
          
          console.log("response: " + JSON.stringify(response));
          debugger;
          if(response.status !== "200"){
              this.snackBar.open("Something went wrong", "Error", {
                  duration: 2000,
              });
          }
          this.snackBar.open("Sucess", "Done", {
              duration: 2000,
          });
              this.refresh();
              
      });
      }
      catch(err){
          debugger;
      }
  

  



  }
  redirect(pagename: string) {
    this.router.navigate([pagename]);
}
    
 
 
    openDialogueBox(x,y, course_section,className,course_id,teacher_name, start_time,end_time,id,course_name) {
    debugger;
    if(!className){
      let snackBarRef = this.snackBar.open('Select Class First!');
    }
    else if(!course_section){
      let snackBarRef = this.snackBar.open('Selected Class has no Courses!');
      
    }
    else{
      this.snackBar.dismiss();
    this.dialogRef = this.dialog.open(FuseCalendarEventFormDialogComponent, {
      panelClass: 'event-form-dialog',
      data: {
        x: x,
        y: y,
        data: course_section,
        class:className,
        course_id : course_id,
        teacher_name : teacher_name,
        start_time : start_time,
        end_time : end_time,
        id : id,
        course_name: course_name
      
      }
    });
    this.dialogRef.afterClosed()
      .subscribe(response => {
        debugger;
      //  this.showLoadingBar = true;
      this.dashboard.forEach((element, index, array) => {
          if(element.x == response.x && element.y == response.y ){
            element.data = response;
          }
          if(element.y == response.y  ){
            element.data.start_time = response.start_time;
            element.data.end_time = response.end_time;
          }
         
          
      });
      //  this.dashboardObj.push(response);
      });
    }
    this.showLoadingBar = false;

  }
  dismiss(){
    setTimeout(function(){ 
      this.snackRef.dismiss();
     }, 3000);
  }


}



