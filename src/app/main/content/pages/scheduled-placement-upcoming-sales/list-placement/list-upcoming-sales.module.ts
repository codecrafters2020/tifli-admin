import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CdkTableModule } from '@angular/cdk/table';

import { MatFormFieldModule, MatInputModule, MatSelectModule, MatStepperModule, MatButtonModule, MatIconModule, MatRippleModule, MatSidenavModule, MatSlideToggleModule, MatTableModule, MatMenuModule, MatPaginatorModule, MatSortModule, MatAutocomplete, MatAutocompleteModule, MatToolbarModule, } from '@angular/material';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseConfirmDialogModule, FuseMaterialColorPickerModule } from '@fuse/components';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRadioModule } from '@angular/material/radio';
import { FuseCalendarEventFormDialogComponent } from './slot-form/slot-form.component';
import { ListMerchantComponent } from './list-merchant.component';
import { ListMerchantService } from './list-merchant.service';
import { GridMerchantComponent } from './grid/grid-merchant.component';
import { SidenavMerchantMainComponent } from './sidenavs/main/sidenav-merchant-main.component';
import { SidenavMerchantDetailsComponent } from './sidenavs/details/sidenav-merchant-details.component';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import { GridsterModule } from 'angular-gridster2';
import { AmazingTimePickerModule } from 'amazing-time-picker';


const routes: Routes = [
    {
        path: '**',
        component: ListMerchantComponent,
        children: [],
        resolve: {
            files: ListMerchantService
        }
    }
];

@NgModule({
    declarations: [
        ListMerchantComponent,
        GridMerchantComponent,
        SidenavMerchantMainComponent,
        SidenavMerchantDetailsComponent,
        FuseCalendarEventFormDialogComponent
    ],
    imports: [
        RouterModule.forChild(routes),
        MatAutocompleteModule,
        CdkTableModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatButtonModule,
        MatIconModule,
        MatRippleModule,
        MatProgressBarModule,
        MatSlideToggleModule,
        MatSidenavModule,
        MatTableModule,
        MatMenuModule,
        MatPaginatorModule,
        MatSortModule,
        FuseSharedModule,
        MatDatepickerModule,
        MatCheckboxModule,
        MatRadioModule,
        FuseSharedModule,
        MatToolbarModule,
        FuseConfirmDialogModule,
        FuseMaterialColorPickerModule,
        GridsterModule,
        MatProgressBarModule,
        MatSnackBarModule,
        AmazingTimePickerModule
    ],

    providers: [
        ListMerchantService,
        GridMerchantComponent

    ],
    entryComponents: [FuseCalendarEventFormDialogComponent]
})
export class ListUpcomingSalesModule {
}
