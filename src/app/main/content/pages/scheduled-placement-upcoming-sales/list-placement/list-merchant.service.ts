import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/mergeMap'
import { AppService } from '../../../../../app.service';
import * as moment from 'moment';
import { of } from 'rxjs/observable/of';

@Injectable()
export class ListMerchantService implements Resolve<any>
{
    merchants: any[];
    slotsData: any[];
    placements: any;
    pages: any[];
    data: any[];
    pageid: any = 1;
    categories: any;
    onSeasonalChanged: BehaviorSubject<any> = new BehaviorSubject({});
    onSlotsDataChanged: BehaviorSubject<any> = new BehaviorSubject({});
    onFilesChanged: BehaviorSubject<any> = new BehaviorSubject({});
    onFileSelected: BehaviorSubject<any> = new BehaviorSubject({});
    onSelectedMerchantsChanged: BehaviorSubject<any> = new BehaviorSubject<any>({});
    onplacementsUpdated = new Subject<any>();
    onMerchantsChanged: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);
    onCouponsChanged: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);
    onPlacementsChanged: BehaviorSubject<any> = new BehaviorSubject({});
    onclassNameChanged: BehaviorSubject<any> = new BehaviorSubject({});
    onDashBoardChanged: BehaviorSubject<any> = new BehaviorSubject({});
    onCourseNameChanged: BehaviorSubject<any> = new BehaviorSubject({});
    showLoadingBar: Boolean = false;
    onShowLoadingBarChanged: BehaviorSubject<any> = new BehaviorSubject({});
    startDate: any;
    endDate: any;
    classes: any;
    course_section: any;
    className:any;
    dashboard: any;
    constructor(private http: HttpClient, private appService: AppService) {
        debugger;

    }

    /**
     * The File Manager App Main Resolver
     * 
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getClasses()
            ]).then(
                ([files]) => {
                    resolve(Response);
                },
                reject
                );
        });


    }

    getPages() {

        // this.pages = [{ id: 1, name: "Top Stores" }, { id: 2, name: "Home Navigation Links" }, { id: 3, name: "Search Merchant Links" }, { id: 4, name: "Upcoming Sales" }, { id: 5, name: "Home Daily Specials" }];


        return new Promise((resolve, reject) => {

            let pages = [];

            this.http.get(this.appService.couponService + `seasonal/`)
                .subscribe((response: any) => {
                    debugger
                    for (let i = 0; i < response.data.length; i++) {
                        let page = {
                            name: '',

                            id: 0
                        }
                        page.name = response.data[i].name;
                        page.id = response.data[i].id;
                        pages.push(page);
                    }

                    this.pages = pages;
                    resolve(this.pages);

                }, reject);


        });
    }
    getClasses(): Promise<any>
    {
        return new Promise((resolve, reject) => {
           // this.http.get(this.appService.merchantService + 'categories/')
           this.http.get(`${this.appService.adminService}class_sections/show_all_classes`)
           .subscribe((response: any) => {
                    debugger;
                    this.classes = response.class_sections;
                   // this.onFilesChanged.next(response.data);
                  //  this.onFileSelected.next(response[0]);
                    resolve(response);
                }, reject);
        });
    }


    getCoupon(couponId): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(this.appService.couponService + 'coupons/' + couponId)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    getFiles(startDate, endDate): Promise<any> {


        return new Promise((resolve, reject) => {
            this.http.get(this.appService.placementService + 'carousel/?startDate=' + startDate + '&endDate=' + endDate)
                // this.http.get(this.appService.apiUrl + 'cms/api/placements/carousel/?startDate=2018-06-07&endDate=2018-06-10')
                .subscribe((response: any) => {

                    this.placements = response.data;
                    for (let i = 0; i < this.placements.length; i++) {
                        //this.placements[i].date = moment(this.placements[i].date).local().format("MMM DD, YYYY");
                        // this.placements[i].date = moment(this.placements[i].date).format("MMM DD, YYYY");
                        var date = this.placements[i].date.split("T")[0];
                        this.placements[i].date = moment(date).format("MMM DD, YYYY");
                        if (this.placements[i].positions[1] != null) {
                            this.placements[i].positions[1]["startDate"] = this.placements[i].positions[1].start;
                            this.placements[i].positions[1].start = moment(this.placements[i].positions[1].start).local();
                            this.placements[i].positions[1].end = moment(this.placements[i].positions[1].end).local();
                        }
                        if (this.placements[i].positions[2] != null) {
                            this.placements[i].positions[2]["startDate"] = this.placements[i].positions[2].start;
                            this.placements[i].positions[2].start = moment(this.placements[i].positions[2].start).local();
                            this.placements[i].positions[2].end = moment(this.placements[i].positions[2].end).local();
                        }
                        if (this.placements[i].positions[3] != null) {
                            this.placements[i].positions[3]["startDate"] = this.placements[i].positions[3].start;
                            this.placements[i].positions[3].start = moment(this.placements[i].positions[3].start).local();
                            this.placements[i].positions[3].end = moment(this.placements[i].positions[3].end).local();
                        }
                        if (this.placements[i].positions[4] != null) {
                            this.placements[i].positions[4]["startDate"] = this.placements[i].positions[4].start;
                            this.placements[i].positions[4].start = moment(this.placements[i].positions[4].start).local();
                            this.placements[i].positions[4].end = moment(this.placements[i].positions[4].end).local();
                        }
                        if (this.placements[i].positions[5] != null) {
                            this.placements[i].positions[5]["startDate"] = this.placements[i].positions[5].start;
                            this.placements[i].positions[5].start = moment(this.placements[i].positions[5].start).local();
                            this.placements[i].positions[5].end = moment(this.placements[i].positions[5].end).local();
                        }

                        //this.placements[i].date = new Date(this.placements[i].date);
                        // this.placements[i].date.toLocaleString();
                        // this.placements[i].date = this.getFormattedDate(this.placements[i].date);
                    }
                    this.onPlacementsChanged.next(this.placements);

                    resolve(response);
                }, reject);
        });
    }
    getClass(id): Promise<any>
    {
        let queryString = 'class_section_id=' + id ;

        return new Promise((resolve, reject) => {
            debugger;
            // this.http.get(this.appService.apiUrl + 'cms/api/merchants/categories/'+this.id)
            this.http.get(`${this.appService.adminService}time_tables/get_class_section_time_table?${queryString}`,{observe : "response"}) 
                .subscribe((response: any) => {
                    
                    resolve(response);
                }, reject);
        }).catch(err => this.handleError(err, () => this.http.get(`${this.appService.adminService}section_courses/get_section_courses?${queryString}`)));;
    }
    addTimetable(Dashboard): Promise<any> {
        return new Promise((resolve, reject) => {
            debugger;
            this.http.post(`${this.appService.adminService}time_tables`, Dashboard)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }
    protected handleError(error, continuation: () => Observable<any>) {
      if (error.status == 404 || error.status == 400) {
        return of(false);
      }
   
  };
    
    getQueryFormattedDate(date) {
        var year = date.getFullYear();

        var month = (1 + date.getMonth()).toString();
        month = month.length > 1 ? month : '0' + month;

        var day = date.getDate().toString();
        day = day.length > 1 ? day : '0' + day;

        return year + '-' + month + '-' + day;
    }

    addCarouselPlacement(slot, page): Promise<any> {
        let date = this.getQueryFormattedDate(slot.Date);
        let payload = {
            seasonalEvent: {
                id: slot.seasonalEventId
            },
            startDate: date,
            position: slot.position
        };
        return new Promise((resolve, reject) => {
            this.http.post(this.appService.placementService + 'schedule/upcoming-sales/', payload)
                .subscribe((response: any) => {
                    debugger;
                    resolve(response);
                }, reject);
        });
    }

    updateCarouselPlacement(slot, page): Promise<any> {
        let date = this.getQueryFormattedDate(slot.Date);
        let payload = {
            seasonalEvent: {
                id: slot.seasonalEventId
            },
            startDate: date,
            position: slot.position
        };
        return new Promise((resolve, reject) => {
            this.http.put(this.appService.placementService + 'schedule/upcoming-sales/' + slot.id, payload)
                .subscribe((response: any) => {

                    resolve(response);
                }, reject);
        });
    }

    getMerchant(merchantId): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(this.appService.merchantService + merchantId)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    deleteCarouselPlacement(slot): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.delete(this.appService.placementService + 'schedule/upcoming-sales/' + slot.slotId)
                .subscribe((response: any) => {

                    resolve(response);
                }, reject);
        });
    }



    getScheduledPlacement(typepage, sdate, edate): Promise<any> {
        console.log("Start dateeee")
        console.log(sdate)
        console.log("End dateeee")
        console.log(edate)
        this.pageid = typepage;
        debugger;
        var todaysdate = new Date();
        var todaysday = todaysdate.getDay();


        if (sdate == null && edate == null) {
            if (todaysday == 1) {
                this.startDate = new Date().toString();
                todaysdate.setDate(todaysdate.getDate() + 6);
                this.endDate = todaysdate.toString();

            }

            if (todaysday != 1) {
                let diff = todaysday - 1;

                todaysdate.setDate(todaysdate.getDate() - diff);
                this.startDate = new Date(todaysdate).toString();

                todaysdate.setDate(todaysdate.getDate() + 6);
                this.endDate = todaysdate.toString();
                debugger;
            }

        }
        else {
            this.startDate = sdate;
            this.endDate = edate;
        }




        var datestart = this.formatDate(this.startDate);
        var dateend = this.formatDate(this.endDate);
        return new Promise((resolve, reject) => {
            this.http.get(this.appService.placementService + 'schedule/upcoming-sales/?startDate=' + datestart + '&endDate=' + dateend)

                .subscribe((response: any) => {
                    debugger;
                    this.data = response.data;
                    this.slotsData = this.data;
                    this.onSlotsDataChanged.next(this.data);
                    resolve(response);
                }, reject);
        });
    }

    formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join('-');
    }


    searchMerchant(searchTerm) {

        this.http.get(this.appService.merchantService + 'search/?name=' + searchTerm).subscribe(response => {
            /* some operations or conditiond on response */

            this.onMerchantsChanged.next(response['data']);
        })
    }
    searchCoupon(searchTerm) {

        this.http.get(this.appService.couponService + 'coupons/search/?titleOrId=' + searchTerm).subscribe(response => {

            this.onCouponsChanged.next(response['data']);

        })
    }
    getseasonalEvent(id): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(this.appService.couponService + `seasonal/${id}`)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }


    getMerchantById(merchantId) {
        this.http.get(this.appService.merchantService + merchantId).subscribe(response => {

            this.onSelectedMerchantsChanged.next(response['data']);
        })
    }
    getManager(id, datasource, index): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(this.appService.userService + `users/${id}`)
                .subscribe((response: any) => {
                    datasource[index].manager.name = response.data.userName;
                    resolve(response);
                }, reject);
        });
    }

    searchMerchantName(term: string): Observable<any> {
        return this.http.get(`${this.appService.merchantService}search/?name=${term}`)
            .map(response => response['data'])
            .map(data => data instanceof Array ? data : [])
    }

    searchSeasonalEvents(searchTerm) {
        debugger;
        this.http.get(this.appService.couponService + 'seasonal/search/?name=' + searchTerm).subscribe(response => {
            /* some operations or conditiond on response */

            this.onSeasonalChanged.next(response['data']);
        })
    }

}
