import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CdkTableModule } from '@angular/cdk/table';

import { MatAutocompleteModule, MatFormFieldModule, MatInputModule, MatSelectModule, MatStepperModule, MatButtonModule, MatIconModule, MatRippleModule, MatSidenavModule, MatSlideToggleModule, MatTableModule, MatMenuModule, MatPaginatorModule, MatSortModule } from '@angular/material';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatRadioModule} from '@angular/material/radio';
import { FuseSharedModule } from '@fuse/shared.module';

import { ListCoursesComponent } from './list-resources.component';
import { ListCoursesService } from './list-resources.service';
import { GridCoursesComponent } from './grid/grid-resources.component';
import { SidenavCoursesMainComponent } from './sidenavs/main/sidenav-courses-main.component';
import { SidenavCoursesDetailsComponent } from './sidenavs/details/sidenav-courses-details.component';

const routes: Routes = [
    {
        path     : '**',
        component: ListCoursesComponent,
        children : [],
        resolve  : {
            files: ListCoursesService
        }
    }
];

@NgModule({
    declarations: [
        ListCoursesComponent,
        GridCoursesComponent,
        SidenavCoursesMainComponent,
        SidenavCoursesDetailsComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        CdkTableModule,
        MatAutocompleteModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatButtonModule,
        MatIconModule,
        MatRippleModule,
        MatSlideToggleModule,
        MatSidenavModule,
        MatTableModule,
        MatMenuModule,
        MatPaginatorModule, 
        MatSortModule,
        MatProgressSpinnerModule,

        FuseSharedModule,
        MatDatepickerModule,
        MatCheckboxModule,
        MatRadioModule
    ],
    providers   : [
        ListCoursesService
    ]
})
export class ListCoursesModule
{
}
