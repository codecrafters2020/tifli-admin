import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { AbstractControl, FormBuilder, FormGroup, FormArray, Validators, EmailValidator } from '@angular/forms';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRadioModule } from '@angular/material/radio';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { FormControl } from '@angular/forms';
import { Observable, BehaviorSubject} from 'rxjs';
import { of } from 'rxjs/observable/of';
import { map, startWith } from 'rxjs/operators';
import { FuseAngularMaterialModule } from '../../../../../components/angular-material/angular-material.module';

import { ListCoursesService } from '../../list-resources.service';


@Component({
    selector   : 'fuse-file-manager-main-sidenav',
    templateUrl: './sidenav-courses-main.component.html',
    styleUrls  : ['./sidenav-courses-main.component.scss']
})

export class SidenavCoursesMainComponent
{
    @Input() leftSideNav: any;

    form: FormGroup;
    formErrors: any;

    
    filterVal : any;
    searchBox: any;
    filters: any[] = [
      {value: 1, viewValue: 'Class-Section Name'},
      {value: 2, viewValue: 'Course Name'},
      {value: 3, viewValue: 'Created by'}
    ];
    btnDisabled: boolean = false;
  eventResults: any[];
  event_name: any;
  event_date: any;
  course_name : any;
    constructor(private ListCoursesService: ListCoursesService, 
                private router: Router,
                private formBuilder: FormBuilder)
    {
      // Reactive Form
      this.form = this.formBuilder.group({
          event : ['', ],
          event_date : ['', ]
      });

      // Reactive form errors
      this.formErrors = {
     
        event  : {},
        event_date : {}
      };
      debugger;
      this.eventResults=this.ListCoursesService.course;
      
      
    }

    
    onChange(){
      let that = this;
      if(this.searchBox ==""){
        that.discardFilter();
      }
    }
    filter()
    {
      let that = this;
      this.btnDisabled = true;
      // debugger;
      debugger;
      if(this.filterVal == 1){
        that.ListCoursesService.coupons = that.ListCoursesService.coupons.filter(res =>{
          
          return res.section_course.class_section.school_class.class_name.concat(' ',res.section_course.class_section.section.section_name).toLocaleLowerCase().match(this.searchBox.toLocaleLowerCase());
         });

      }
      else if(this.filterVal == 2){
        that.ListCoursesService.coupons = that.ListCoursesService.coupons.filter(res =>{
          
          return res.section_course.course.course_name.toLocaleLowerCase().match(this.searchBox.toLocaleLowerCase());
         });
      }
      else if(this.filterVal == 3){
        that.ListCoursesService.coupons = that.ListCoursesService.coupons.filter(res =>{
          
          return res.section_course.course_teacher_name.toLocaleLowerCase().match(this.searchBox.toLocaleLowerCase());
         });
      }
  
      this.ListCoursesService.onFilesChanged.next(that.ListCoursesService.coupons);
  
     }
     discardFilter(){
      this.ListCoursesService.coupons = this.ListCoursesService.course;
      this.ListCoursesService.onFilesChanged.next(this.ListCoursesService.coupons);
     }

    ngOnInit() 
    {
      this.form.valueChanges.subscribe(() => {
        // console.log(this.form.value);
      });
    
      // this.form.controls.merchantName.valueChanges
      //   .debounceTime(400)
      //   .subscribe(searchTerm => {
    	// 		this.ListFacultyService.searchMerchant(searchTerm)
    	// 			.subscribe(merchantNames => {
    	// 				this.merchantSearchResult = merchantNames;
    	// 			});
      //   });

      // this.form.controls.seasonalEvent.valueChanges
      //   .debounceTime(400)
      //   .subscribe(searchTerm => {
    	// 		this.ListFacultyService.searchSeasonalEvent(searchTerm)
    	// 			.subscribe(response => {
    	// 				this.seasonalEventSearchResult = response;
    	// 			});
        // });

    }

    displaySelectedName(item: any) 
    {
      return item.event_name;
    }
}  

    
