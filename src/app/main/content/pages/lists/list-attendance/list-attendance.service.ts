import { Injectable } from '@angular/core';
import { HttpClient,HttpParams } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of'
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { AppService } from '../../../../../app.service';

import * as moment from 'moment';
import * as _ from 'lodash';

@Injectable()
export class ListCoursesService implements Resolve<any>
{
    coupons: any=[];
    course: any[];
    couponTypes: any[];
    onFilesChanged: BehaviorSubject<any> = new BehaviorSubject({});
    onFileSelected: BehaviorSubject<any> = new BehaviorSubject({});
    classes: any;

    constructor(private http: HttpClient, private appService: AppService) {
    }

    /**
     * The File Manager App Main Resolver
     * 
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getFiles()
                //this.getclass()
            ]).then(
                ([courses]) => {
                    resolve(Response);
                },
                reject
                );
        });
    }

    
    deleteCourses(course): Promise<any>
    {
        return new Promise((resolve, reject) => {
            // this.http.get(this.appService.apiUrl + 'cms/api/users/')
            // this.http.get('http://104.42.179.33:8765/cms/api/coupons/coupons?page=0&size=20&sort=couponCode,asc&searchTerms=gag&couponCode=123&couponType=Deal')
            this.http.patch(`${this.appService.adminService}courses/delete_course`,course)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
}

    getCourses(Month,Class): Promise<any> {
        const params = new HttpParams()
                .set('month', Month)
                .set('class_section_id', Class);
            return new Promise((resolve, reject) => {
               this.http.get<any>(`${this.appService.adminService}students/get_student_attendances`,{params})
                .subscribe((response: any) => {
                    //this.coupons = response;

                    this.course = response;
                    this.coupons = response;
                    this.onFilesChanged.next(response);
                    this.onFileSelected.next(response[0]);
                   
                    debugger;
                    resolve(response);
                }, reject);
        }).catch(err => this.handleError(err, () =>  this.http.get(`${this.appService.adminService}students/get_student_attendances`,{params})));;
    }
    protected handleError(error, continuation: () => Observable<any>) {
        let that = this;
        alert("Selected Class and Month Pair has no Attendance Record");
        if (error.status == 404 || error.status == 400) {
           that.coupons= [];
           that.course= [];
           this.onFilesChanged.next(that.coupons);
            this.onFileSelected.next(that.coupons);  
          return of(false);
        }
     
    }
    getFiles(): Promise<any>
    {
        return new Promise((resolve, reject) => {
           // this.http.get(this.appService.merchantService + 'categories/')
           this.http.get(`${this.appService.adminService}class_sections/show_all_classes`)
           .subscribe((response: any) => {
                    debugger;
                    this.classes = response.class_sections;
                   // this.onFilesChanged.next(response.data);
                  //  this.onFileSelected.next(response[0]);
                    resolve(response);
                }, reject);
        });
    }

   

    getFilteredCourses(
        name: '',
        date: '' 
    
        
    ): Promise<any> {
         let queryString = 'name=' + name;
        //+ name + ' date=' + date;

        let params = {
         
            name : 'name='+name,
            date : 'date='+date
        
        };
        /*/
        let queryString = '';
        _.forEach(params, (val, key) => {
            if (val || typeof val === 'number') {
                queryString = queryString + key + '=' + val + '&';
            }
           else if (val || typeof val === 'string') {
                queryString = queryString + key + '=' + val + '&';
            }
            
        });
        */
        return new Promise((resolve, reject) => {
            // this.http.get('api/coupon-list')
            // this.http.get(`http://104.42.179.33:8765/cms/api/coupons?page=0&size=20&sort=couponCode,asc&searchTerms=gag&couponCode=123&couponType=Deal`)
            this.http.get(`${this.appService.adminService}courses/get_filtered_courses?${queryString}`)
                .subscribe((response: any) => {
                    this.coupons = response;
                    this.onFilesChanged.next(response.sms_users);
                    this.onFileSelected.next(response[0]);
                    resolve(response);
                }, reject);
        });
    }


}
