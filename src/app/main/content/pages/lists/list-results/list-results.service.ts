import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of'
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { AppService } from '../../../../../app.service';

import * as moment from 'moment';
import * as _ from 'lodash';

@Injectable()
export class ListCoursesService implements Resolve<any>
{
    coupons: any=[];
    course: any[];
    couponTypes: any[];
    onFilesChanged: BehaviorSubject<any> = new BehaviorSubject({});
    onFileSelected: BehaviorSubject<any> = new BehaviorSubject({});

    constructor(private http: HttpClient, private appService: AppService) {
    }

    /**
     * The File Manager App Main Resolver
     * 
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getCourses(),
                //this.getclass()
            ]).then(
                ([courses]) => {
                    resolve();
                },
                reject
                );
        });
    }

    
    deleteCourses(course): Promise<any>
    {
        return new Promise((resolve, reject) => {
            // this.http.get(this.appService.apiUrl + 'cms/api/users/')
            // this.http.get('http://104.42.179.33:8765/cms/api/coupons/coupons?page=0&size=20&sort=couponCode,asc&searchTerms=gag&couponCode=123&couponType=Deal')
            this.http.patch(`${this.appService.adminService}courses/delete_course`,course)
                .subscribe((response: any) => {
                    debugger;
                    resolve(response);
                }, reject);
        });
}

    getCourses(): Promise<any> {
        return new Promise((resolve, reject) => {
            // this.http.get(this.appService.apiUrl + 'cms/api/users/')
            // this.http.get('http://104.42.179.33:8765/cms/api/coupons/coupons?page=0&size=20&sort=couponCode,asc&searchTerms=gag&couponCode=123&couponType=Deal')
            this.http.get(`${this.appService.adminService}result/get_filtered_results`,{observe: "response"})
                .subscribe((response: any) => {
                    let that = this;
                    //this.coupons = response;
                    if (response.body.message == "result not found"){
                        that.course= [];
                        that.coupons= [];
                    }
                    else{
                      
                        that.course = response.body.results;
                        debugger;
                        that.course.forEach(function(value){
                            if(value.student_results.length >0){
                                that.coupons.push(value);
                            }
                        })
                        that.course = that.coupons;
                        
                    }
                    //this.coupons = response.results;
                    resolve(response);
                }, reject);
        }).catch(err => this.handleError(err, () =>  this.http.get(`${this.appService.adminService}courses/show_all_courses`)));;
    }
    protected handleError(error, continuation: () => Observable<any>) {
        let that = this;
        if (error.status == 404 || error.status == 400) {
           that.coupons= [];
           that.course= [];  
          return of(false);
        }
     
    }


   

    getFilteredCourses(
        name: '',
        date: '' 
    
        
    ): Promise<any> {
        debugger;

        let queryString = 'name=' + name;
        //+ name + ' date=' + date;

        let params = {
         
            name : 'name='+name,
            date : 'date='+date
        
        };
        /*/
        let queryString = '';
        _.forEach(params, (val, key) => {
            if (val || typeof val === 'number') {
                queryString = queryString + key + '=' + val + '&';
            }
           else if (val || typeof val === 'string') {
                queryString = queryString + key + '=' + val + '&';
            }
            
        });
        */
        return new Promise((resolve, reject) => {
            // this.http.get('api/coupon-list')
            // this.http.get(`http://104.42.179.33:8765/cms/api/coupons?page=0&size=20&sort=couponCode,asc&searchTerms=gag&couponCode=123&couponType=Deal`)
            this.http.get(`${this.appService.adminService}courses/get_filtered_courses?${queryString}`)
                .subscribe((response: any) => {
                    this.coupons = response;
                    this.onFilesChanged.next(response.sms_users);
                    this.onFileSelected.next(response[0]);
                    resolve(response);
                }, reject);
        });
    }


}
