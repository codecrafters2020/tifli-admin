import { Component, Inject, ViewEncapsulation, AfterViewChecked } from '@angular/core';
import { FormBuilder, FormControl, Validators, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material';

import { MatColors } from '@fuse/mat-colors';
import { ListCoursesService } from '../list-results.service';
import { CalendarEvent } from 'angular-calendar';
import { AppService } from './../../../../../../app.service';
import { invalid } from 'moment';
import { DISABLED } from '@angular/forms/src/model';
@Component({
    selector: 'fuse-calendar-event-form-dialog',
    templateUrl: './slot-form.component.html',
    styleUrls: ['./slot-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class FuseCalendarEventFormDialogComponent {

    btndisable: boolean = false;
    /** Meta Data for Carousel Popup */
    public selectedTime: string;
    


    /** Meta data ended */

    /** Slot Object */
    slot: any = {
        start_time:'',
        end_time: '',
        x: 0,
        y: 0,
        section_course_id:0,
        section_course_name:'',
        section_course_teacher:'',
        className: '',
    };

    filterArray : any[];

    dialogTitle: string = "Add Slot";
    slotForm: FormGroup;
    action: string = "add";

    
    buttonAction: any = "add";
    file_access: any;

    constructor(
        public dialogRef: MatDialogRef<FuseCalendarEventFormDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private formBuilder: FormBuilder,
        private appService: AppService,
       
        // private scheduledPlacementService: ScheduledPlacementService
    ) {
       this.data; 
       debugger;
 
        
        
    }
    onChange(){
       
      
    }
    
    ngOnInit()
    {
        // Reactive Form
        this.slotForm = this.formBuilder.group({
          
           
        });

       
        
        

    }
    



    closeDialog(action) {
        debugger;
         if(action=='cancel'){
            this.dialogRef.close();
         }   
         else{
             this.slot.start_time = this.slot.start_time;
             this.slot.end_time = this.slot.end_time;     
             this.dialogRef.close(this.slot);
         }
         
    }

   

}
