import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, FormArray, Validators, EmailValidator } from '@angular/forms';
import { fuseAnimations } from '@fuse/animations';

import { ListCoursesService } from './list-results.service';
import { Router } from '@angular/router';

@Component({
    selector: 'fuse-file-manager',
    templateUrl: './list-results.component.html',
    styleUrls: ['./list-results.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class ListCoursesComponent implements OnInit {
    selected: any;
    pathArr: string[];
    isSearch: boolean = false;
    form: FormGroup;
    merchantSearchResult: any[];


    constructor(private ListCoursesService: ListCoursesService, private router: Router, private formBuilder: FormBuilder) {
      //  this.ListCouponService.getCoupons();
        this.form = this.formBuilder.group({
            merchantName : ['', ]})

    }

    ngOnInit() {
        this.ListCoursesService.onFileSelected.subscribe(selected => {
            this.selected = selected;
            // this.pathArr = selected.location.split('>');
        });

    }

    openCouponAdd() {
        this.router.navigateByUrl("pages/courses/add");

    }

    resetGrid() {
        this.ListCoursesService.getCourses();
    }
    
    searching() {

        if(this.isSearch == false){
            this.isSearch = true;
        }

        else{
            this.isSearch = false;
            console.log("close it")
        }
    }
}
