import { Component, OnInit } from '@angular/core';

import { fuseAnimations } from '@fuse/animations/index';

import { ListCoursesService } from '../../list-progress.service';

@Component({
    selector   : 'fuse-file-manager-details-sidenav',
    templateUrl: './sidenav-courses-details.component.html',
    styleUrls  : ['./sidenav-courses-details.component.scss'],
    animations : fuseAnimations
})
export class SidenavCoursesDetailsComponent implements OnInit
{

    selected: any;

    constructor(private ListCoursesService: ListCoursesService)
    {

    }

    ngOnInit()
    {
        this.ListCoursesService.onFileSelected.subscribe(selected => {
            this.selected = selected;
        });
    }

}
