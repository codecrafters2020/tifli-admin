import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { AbstractControl, FormBuilder, FormGroup, FormArray, Validators, EmailValidator } from '@angular/forms';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRadioModule } from '@angular/material/radio';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { FormControl } from '@angular/forms';
import { Observable, BehaviorSubject} from 'rxjs';
import { of } from 'rxjs/observable/of';
import { map, startWith } from 'rxjs/operators';
import { FuseAngularMaterialModule } from '../../../../../components/angular-material/angular-material.module';

import { ListCoursesService } from '../../list-ptm.service';


@Component({
    selector   : 'fuse-file-manager-main-sidenav',
    templateUrl: './sidenav-courses-main.component.html',
    styleUrls  : ['./sidenav-courses-main.component.scss']
})

export class SidenavCoursesMainComponent
{
    @Input() leftSideNav: any;

    form: FormGroup;
    formErrors: any;
    filterMonth:"";
    Months = [
      { "name": "January", "value":  '1' },
      { "name": "Feburary", "value":  '2' },
      { "name": "March", "value":  '3' },
      { "name": "April", "value":  '4' },
      { "name": "May", "value":  '5' },
      { "name": "June", "value":  '6' },
      { "name": "July", "value":  '7' },
      { "name": "August", "value":  '8' },
      { "name": "September", "value":  '9' },
      { "name": "October", "value":  '10' },
      { "name": "November", "value":  '11' },
      { "name": "December", "value":  '12' },
  ];
    filterVal : any;
    searchBox: any;
    filters: any[] = [
      {value: 1, viewValue: 'Faculty Name'},
      {value: 2, viewValue: 'Student Name'},
     ];
    btnDisabled: boolean = false;
  eventResults: any[];
  event_name: any;
  event_date: any;
  course_name : any;
    constructor(private ListCoursesService: ListCoursesService, 
                private router: Router,
                private formBuilder: FormBuilder)
    {
      // Reactive Form
      this.form = this.formBuilder.group({
          event : ['', ],
          event_date : ['', ],
          MonthSelector : [''],
      });

      // Reactive form errors
      this.formErrors = {
     
        event  : {},
        event_date : {}
      };
      debugger;
      this.eventResults=this.ListCoursesService.course;
      
      
    }
    onChangeMonth(){
      console.log(this.filterMonth);
    }
    month(){
      this.btnDisabled = true;
      debugger;
     // this.event_name =  this.form.controls.event;
      //this.event_name=  this.event_name.value;
      this.ListCoursesService.getCourses(
                                                this.filterMonth
                                                                               
                                              )
        .then((response) => {
          this.btnDisabled = false;
          this.leftSideNav.toggle()
        })
    }
    onChange(){
      let that = this;
      if(this.searchBox ==""){
        that.discardFilter();
      }
    }
    filter()
    {
      let that = this;
      this.btnDisabled = true;
      // debugger;
      debugger;
      if(this.filterVal == 1){
        that.ListCoursesService.coupons = that.ListCoursesService.coupons.filter(res =>{
          
          return res.faculty_name.toLocaleLowerCase().match(this.searchBox.toLocaleLowerCase());
         });

      }
      else if(this.filterVal == 2){
        that.ListCoursesService.coupons = that.ListCoursesService.coupons.filter(res =>{
          
          return res.student_name.toLocaleLowerCase().match(this.searchBox.toLocaleLowerCase());
         });
      }
    
  
      this.ListCoursesService.onFilesChanged.next(that.ListCoursesService.coupons);
  
     }
     discardFilter(){
      this.ListCoursesService.coupons = this.ListCoursesService.course;
      this.ListCoursesService.onFilesChanged.next(this.ListCoursesService.coupons);
     }

    ngOnInit() 
    {
      this.form.valueChanges.subscribe(() => {
        // console.log(this.form.value);
      });
    
      // this.form.controls.merchantName.valueChanges
      //   .debounceTime(400)
      //   .subscribe(searchTerm => {
    	// 		this.ListFacultyService.searchMerchant(searchTerm)
    	// 			.subscribe(merchantNames => {
    	// 				this.merchantSearchResult = merchantNames;
    	// 			});
      //   });

      // this.form.controls.seasonalEvent.valueChanges
      //   .debounceTime(400)
      //   .subscribe(searchTerm => {
    	// 		this.ListFacultyService.searchSeasonalEvent(searchTerm)
    	// 			.subscribe(response => {
    	// 				this.seasonalEventSearchResult = response;
    	// 			});
        // });

    }

    displaySelectedName(item: any) 
    {
      return item.event_name;
    }
}  

    
