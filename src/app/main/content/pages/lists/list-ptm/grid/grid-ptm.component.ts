import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { DataSource } from '@angular/cdk/collections';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { ListCoursesService } from '../list-ptm.service';
import { fuseAnimations } from '@fuse/animations/index';
import { FuseUtils } from '@fuse/utils';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material';
import { FuseCalendarEventFormDialogComponent } from '../slot-form/slot-form.component';

@Component({
    selector   : 'fuse-file-list',
    templateUrl: './grid-ptm.component.html',
    styleUrls  : ['./grid-ptm.component.scss'],
    animations : fuseAnimations
})
export class GridCoursesComponent implements OnInit
{
    files: any;
    dataSource: GridUserDataSource | null;
    displayedColumns = ['Student_Name','Teacher_Name','Description','Comments','Date','Time'];
    selected: any;
    dialogRef: any;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild('filter') filter: ElementRef;
    @ViewChild(MatSort) sort: MatSort;

    constructor(private ListCoursesService: ListCoursesService,public dialog: MatDialog, private router: Router ,private snackBar: MatSnackBar)
    {
        // this.ListCouponService.onFilesChanged.subscribe(files => {
        //     this.files = files;
        // });
        // this.ListCouponService.onFileSelected.subscribe(selected => {
        //     this.selected = selected;
        // });
    }

    ngOnInit()
    {
        // debugger;
        // this.dataSource = new GridUserDataSource(this.ListCouponService, this.paginator, this.sort);
        debugger;
        this.dataSource = new GridUserDataSource(this.ListCoursesService, this.paginator, this.sort);
              //           .debounceTime(150)
        //           .distinctUntilChanged()
        //           .subscribe(() => {
        //               if ( !this.dataSource )
        //               {
        //                   return;
        //               }
        //               this.dataSource.filter = this.filter.nativeElement.value;
        //           });
    }
    openDialog(object){
        this.dialogRef = this.dialog.open(FuseCalendarEventFormDialogComponent, {
            panelClass: 'event-form-dialog',
            data: {
             obj: object
            
            }
          });
          this.dialogRef.afterClosed()
            .subscribe(response => {
               
                
            });
            //  this.dashboardObj.push(response);
           
    }
    onSelect(selected)
    {
        debugger;
        // this.ListCouponService.onFileSelected.next(selected);
        this.router.navigateByUrl("pages/courses/edit/" + selected.id);
    }

    openUserDashboard(course,userid){
        this.router.navigateByUrl("apps/dashboards/project/" + userid);
    }
    

    openCoursesEdit(course,id){
        debugger;
        this.router.navigateByUrl("pages/courses/edit/" + id);
    }
    deleteCourses(course,id){
      
           
        let queryString = {
            id: id,
                }

        try{
            this.ListCoursesService.deleteCourses(queryString).then(response => {
              
                
                debugger;
                if(response.status !== "200"){
                    this.snackBar.open("Something went wrong", "Error", {
                        duration: 2000,
                    });
                }
                this.snackBar.open("Sucess", "Done", {
                    duration: 2000,
                });
                
                    this.redirect('pages/courses/list');
            });
            }
            catch(err){
                debugger;
            }
    }

    
    redirect(pagename: string) {
        this.router.navigate([pagename]);
    }
}

export class GridUserDataSource extends DataSource<any>
{
    _filterChange = new BehaviorSubject('');
    _filteredDataChange = new BehaviorSubject('');

    get filteredData(): any
    {
        return this._filteredDataChange.value;
    }

    set filteredData(value: any)
    {
        this._filteredDataChange.next(value);
    }

    get filter(): string
    {
        return this._filterChange.value;
    }

    set filter(filter: string)
    {
        this._filterChange.next(filter);
    }
    constructor(private ListCoursesService: ListCoursesService, private _paginator: MatPaginator, private _sort: MatSort)
    {
        super();
        this.filteredData = this.ListCoursesService.coupons;
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    // connect(): Observable<any[]>
    // {
    //     return this.ListCouponService.onFilesChanged;
    // }
    connect(): Observable<any[]>
    {
        const displayDataChanges = [
            this.ListCoursesService.onFilesChanged,
            this._paginator.page,
            this._filterChange,
            // this._sort.sortChange
        ];

        return Observable.merge(...displayDataChanges).map(() => {
            let data = this.ListCoursesService.coupons.slice();

            data = this.filterData(data);

            this.filteredData = [...data];

            //data = this.sortData(data);

            // Grab the page's slice of data.
            const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
            return data.splice(startIndex, this._paginator.pageSize);
        });
    }

    filterData(data)
    {
        if ( !this.filter )
        {
            return data;
        }
        return FuseUtils.filterArrayByString(data, this.filter);
    }

    // sortData(data): any[]
    // {
    //     if ( !this._sort.active || this._sort.direction === '' )
    //     {
    //         return data;
    //     }

    //     return data.sort((a, b) => {
    //         let propertyA: number | string = '';
    //         let propertyB: number | string = '';

    //         switch ( this._sort.active )
    //         {
    //             case 'id':
    //                 [propertyA, propertyB] = [a.id, b.id];
    //                 break;
    //             case 'name':
    //                 [propertyA, propertyB] = [a.name, b.name];
    //                 break;
    //             case 'categories':
    //                 [propertyA, propertyB] = [a.categories[0], b.categories[0]];
    //                 break;
    //             case 'price':
    //                 [propertyA, propertyB] = [a.priceTaxIncl, b.priceTaxIncl];
    //                 break;
    //             case 'quantity':
    //                 [propertyA, propertyB] = [a.quantity, b.quantity];
    //                 break;
    //             case 'active':
    //                 [propertyA, propertyB] = [a.active, b.active];
    //                 break;
    //         }

    //         const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
    //         const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

    //         return (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1);
    //     });
    // }

    disconnect()
    {
    }
}
