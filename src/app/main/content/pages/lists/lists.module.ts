import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FuseAngularMaterialModule } from '../../components/angular-material/angular-material.module';

import { FuseSharedModule } from '@fuse/shared.module';

const routes = [
    {
        path        : 'progress',
        loadChildren: './list-progress/list-progress.module#ListCoursesModule'
    },
    {
        path        : 'attendances',
        loadChildren: './list-attendance/list-attendance.module#ListCoursesModule'
    },
    {
        path        : 'diaries',
        loadChildren: './list-diaries/list-diaries.module#ListCoursesModule'
    },
    {
        path        : 'leaves',
        loadChildren: './list-leaves/list-leaves.module#ListCoursesModule'
    },
    {
        path        : 'ptm',
        loadChildren: './list-ptm/list-ptm.module#ListCoursesModule'
    },
    {
        path        : 'resources',
        loadChildren: './list-resources/list-resources.module#ListCoursesModule'
    },
    {
        path        : 'results',
        loadChildren: './list-results/list-results.module#ListCoursesModule'
    }
];

@NgModule({
    imports     : [
        FuseSharedModule,
        RouterModule.forChild(routes),
        FuseAngularMaterialModule
    ],
    declarations: []
})
export class ListsModule
{
}
