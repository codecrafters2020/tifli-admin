import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CdkTableModule } from '@angular/cdk/table';

import { MatFormFieldModule, MatInputModule, MatSelectModule, MatStepperModule, MatButtonModule, MatIconModule, MatRippleModule, MatSidenavModule, MatSlideToggleModule, MatTableModule, MatCheckboxModule} from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';

import { EditFacultyComponent } from './edit-faculty.component';
import { EditFacultyService } from './edit-faculty.service';
import { FormEditFacultyComponent } from './form/form-edit-faculty.component';
import { FancyImageUploaderModule } from 'ng2-fancy-image-uploader';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatProgressBarModule} from '@angular/material/progress-bar';


const routes: Routes = [
    {
        path     : '**',
        component:EditFacultyComponent,
        children : [],
        resolve  : {
            merchantById : EditFacultyService
        }
    }
];

@NgModule({
    declarations: [
        EditFacultyComponent,
        FormEditFacultyComponent,
    ],
    imports     : [
        RouterModule.forChild(routes),
        MatProgressBarModule,
        CdkTableModule,
        MatFormFieldModule,
        MatExpansionModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatButtonModule,
        MatIconModule,
        MatRippleModule,
        MatSlideToggleModule,
        MatSidenavModule,
        MatTableModule,
        FancyImageUploaderModule,
        MatCheckboxModule,

        FuseSharedModule
    ],
    providers   : [
        EditFacultyService
    ]
})
export class EditFacultyModule
{
}
