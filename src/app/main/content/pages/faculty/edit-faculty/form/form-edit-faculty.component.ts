
import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators, FormsModule, FormControl } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { EditFacultyService } from '../edit-faculty.service';
import { Router } from '@angular/router';
import { FancyImageUploaderOptions, UploadedFile } from 'ng2-fancy-image-uploader';
import { getLocaleDateTimeFormat } from '@angular/common';
import { AppService } from '../../../../../../app.service';
import { DISABLED } from '@angular/forms/src/model';

@Component({
    selector: 'fuse-file-list',
    templateUrl: './form-edit-faculty.component.html',
    styleUrls: ['./form-edit-faculty.component.scss'],
})
export class FormEditFacultyComponent implements OnInit {
    //form: FormGroup;
   
    //form: FormGroup;
    formErrors: any;
    test: any;
    step: number;
    photoUpload : boolean = false;
    showLoadingBar : boolean = false;
    // Horizontal Stepper
    horizontalStepperStep1: FormGroup;
    horizontalStepperStep2: FormGroup;
    button_disabled :boolean = false;

    horizontalStepperStep1Errors: any;
    horizontalStepperStep2Errors: any;
  
    default_choice={id: "-1", firstName: "None" ,userName: "none"}
    faculty={
        first_name: '',
        last_name: '',
        gender: '',
        city: 'Karachi',
        information :{
            is_class_teacher : true
        },
        country:'Pakistan',
        email:'',
        mobile:'',
        classteacher: false,
        role:3,
        password:"faculty123",
        image_url :''
         
    }

    avatar: any;
    imagePreview: string;
    displayedColumns = ['enable', 'name', 'description'];
    displayedColumnsPanelHeading = ['CategoryName', 'Description', 'isChecked'];


    countries = [
        { "name": "Afghanistan", "code": "1" },
        { "name": "land Islands", "code": "2" },
        { "name": "Pakistan", "code": "PK" },
       
      
    ];

    cities = [
        { "name": "Afghanistan", "code": "AF" },
        { "name": "Karachi", "code": "KHI" }
    ];
    genderlist = [
        { "name": "Male", "code": "M" },
        { "name": "Female", "code": "F" }
    ];
    
    constructor(private EditFacultyService: EditFacultyService, private formBuilder: FormBuilder, private router: Router, private snackBar: MatSnackBar, private appService: AppService) {

        debugger;

        this.faculty = EditFacultyService.faculty;
        this.imagePreview = this.faculty.image_url;
        // this.largeImgUrl = this.appService.cdnUrl + this.faculty.id + "_large.png";
        
       
        // // Horizontal Stepper form error
        this.horizontalStepperStep1Errors = {
            first_name: {},
            last_name: {},
            country: {},
            city: {},
            email: {},
            mobile: {}
        };

    }

    ngOnInit() {
        this.horizontalStepperStep1 = this.formBuilder.group({

            first_name: ['', [Validators.required]],
            last_name: ['', [Validators.required]],
            country: ['', Validators.required],
            city: ['', Validators.required],
            gender: [''],
            mobile: ['', Validators.required],
            email: ['', Validators.required],
            classteacher: [''],
           
        });
        // this.disableMonetized = false;

        this.horizontalStepperStep2 = this.formBuilder.group({
            image : new FormControl(null, {})
        });



        this.horizontalStepperStep1.valueChanges.subscribe(() => {
            this.onFormValuesChanged(this.horizontalStepperStep1, this.horizontalStepperStep1Errors);
        });

        this.horizontalStepperStep2.valueChanges.subscribe(() => {
            this.onFormValuesChanged(this.horizontalStepperStep2, this.horizontalStepperStep2Errors);
        });

      

    }
    randomString = function (length) {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (var i = 0; i < length; i++) {
          text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return text;
      }
     uploadToS3(event){
        this.showLoadingBar = true;
        this.photoUpload = true;
        if (event.target.files){
          var reader = new FileReader();
          var blob;
          reader.readAsDataURL(event.target.files[0]);
          // tslint:disable-next-line:no-shadowed-variable
          const file = (event.target as HTMLInputElement).files[0];
          console.log('file',file);
          reader.onload = (event: any ) => {
            blob = event.target.result;
            this.imagePreview = event.target.result;
            // const file = (event.target as HTMLInputElement).files[0];
            this.horizontalStepperStep2.patchValue({image : blob});
            this.horizontalStepperStep2.get('image').updateValueAndValidity();
            let ext = 'jpg';
            let type = 'image/jpeg';
            let newName = this.randomString(6) + new Date().getTime() + '.' + ext;
            let that = this;
            // method 2

            // method 2
            this.EditFacultyService.getSignedUploadRequest(newName, ext,type).subscribe(data => {
              console.log('Success in getSignedUploadRequest',data);
              that.avatar =  data['public_url'];
              console.log('i am in public url',data['public_url']);
              console.log('b64',blob);
              console.log(this.horizontalStepperStep2);
              let form = this.horizontalStepperStep2.value;
              blob =blob.replace(/^data:image\/\w+;base64,/, '');
              console.log('blob',blob);
              blob = this.base64toBlob(blob, 'jpg');
              this.faculty.image_url = data['public_url'];
              console.log('blob',blob);
              this.EditFacultyService.putFileToS3(blob, data["presigned_url"])
              .subscribe(
                 
                response => {
                this.showLoadingBar = false;
                this.photoUpload = false;
                }
                );
                

            }, (err) => {

              console.log('getSignedUploadRequest timeout error: ', err);
            });

          };
        }
        }

    onFormValuesChanged(horizontalStepperStep: FormGroup, horizontalStepperStepErrors: any) {
        for (const field in horizontalStepperStepErrors) {
            if (!horizontalStepperStepErrors.hasOwnProperty(field)) {
                continue;
            }


            horizontalStepperStepErrors[field] = {};

            const control = horizontalStepperStep.get(field);

            if (control && control.dirty && !control.valid) {
                horizontalStepperStepErrors[field] = control.errors;
            }
        }
    }


    finishHorizontalStepper() {
       let that = this;
       this.button_disabled = true;
        try{
        this.EditFacultyService.updateFaculty(this.faculty).then(response => {
           
            this.test = response;
            let is422 = that.EditFacultyService.is422;
            if(response.status != "200" && !is422){
                this.snackBar.open("Something went wrong", "Error", {
                    duration: 2000,
                });
                that.button_disabled = false;
            }
            else if (is422) {
                this.snackBar.open("Email OR Contact Number OR Enrollement No Already Taken", "Done", {
                    duration: 2000,
                });    
                that.button_disabled = false;
            }
            else{
                this.snackBar.open("Success", "Done", {
                    duration: 2000,
                });
                this.redirect('pages/faculty/list');   
            }
        });
        }
        catch(err){
            debugger;
        }
    }

    redirect(pagename: string) {
        this.router.navigate([pagename]);
    }

    base64toBlob(base64Data, contentType) {
        contentType = contentType || '';
        var sliceSize = 1024;
        var byteCharacters = atob(base64Data);
        var bytesLength = byteCharacters.length;
        var slicesCount = Math.ceil(bytesLength / sliceSize);
        var byteArrays = new Array(slicesCount);
    
        for (var sliceIndex = 0; sliceIndex < slicesCount; ++sliceIndex) {
          var begin = sliceIndex * sliceSize;
          var end = Math.min(begin + sliceSize, bytesLength);
    
          var bytes = new Array(end - begin);
          for (var offset = begin, i = 0; offset < end; ++i, ++offset) {
            bytes[i] = byteCharacters[offset].charCodeAt(0);
          }
          byteArrays[sliceIndex] = new Uint8Array(bytes);
        }
        return new Blob(byteArrays, { type: contentType });
      }
    

}
