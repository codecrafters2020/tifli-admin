import { Component } from '@angular/core';
import { NgModule, } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CdkTableModule } from '@angular/cdk/table';

import { MatFormFieldModule, MatInputModule, MatSelectModule, MatStepperModule, MatButtonModule, MatIconModule, MatRippleModule, MatSidenavModule, MatSlideToggleModule, MatTableModule, MatCheckboxModule} from '@angular/material';
import {MatProgressBarModule} from '@angular/material/progress-bar';

import { FuseSharedModule } from '@fuse/shared.module';

import { AddFacultyComponent } from './add-faculty.component';
import { AddFacultyService } from './add-faculty.service';
import { FormAddFacultyComponent } from './form/form-add-faculty.component';
import { FancyImageUploaderModule } from 'ng2-fancy-image-uploader';
import {MatExpansionModule} from '@angular/material/expansion';


const routes: Routes = [
    {
        path     : '**',
        component:AddFacultyComponent,
        children : [],
        resolve  : {
            merchantById : AddFacultyService
        }
    }
];

@NgModule({
    declarations: [
        AddFacultyComponent,
        FormAddFacultyComponent,
    ],
    imports     : [
        RouterModule.forChild(routes),

        CdkTableModule,
        MatFormFieldModule,
        MatExpansionModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatButtonModule,
        MatIconModule,
        MatRippleModule,
        MatSlideToggleModule,
        MatSidenavModule,
        MatTableModule,
        FancyImageUploaderModule,
        MatCheckboxModule,
        MatProgressBarModule,
        FuseSharedModule
    ],
    providers   : [
        AddFacultyService
    ]
})
export class AddFacultyModule
{
}
