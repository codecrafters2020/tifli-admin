import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';

import { AddFacultyService } from './add-faculty.service';

import { Router } from '@angular/router';

@Component({
    selector     : 'fuse-file-manager',
    templateUrl  : './add-faculty.component.html',
    styleUrls    : ['./add-faculty.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class AddFacultyComponent implements OnInit
{
    selected: any;
    pathArr: string[];

    constructor(private AddFacultyService: AddFacultyService, private router: Router)
    {
    }

    ngOnInit()
    {
        // this.AddMerchantService.onFileSelected.subscribe(selected => {
        //     this.selected = selected;
        //     this.pathArr = selected.location.split('>');
        // });
    }

    openFacultyList(){
        
        this.router.navigateByUrl("pages/faculty/list/");
    }
}
