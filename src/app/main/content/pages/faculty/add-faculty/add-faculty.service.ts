import { Injectable  } from '@angular/core';
import { HttpClient,HttpHeaders, } from '@angular/common/http';
import { Headers } from '@angular/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { AppService } from '../../../../../app.service';
import { AbstractControl } from '@angular/forms';
import { RequestOptions } from '@angular/http';
import { of } from 'rxjs/observable/of'
import {AuthService} from '../../../../../auth/auth.service'
import {InterceptorSkipHeader} from '../../../../../auth/jwt.interceptor';
@Injectable()
export class AddFacultyService implements Resolve<any>
{

    categoriesList: any[];
    usersList: any[];
    barcodesList: any[];
    affiliateNetworksList: any[];
    merchantAffiliateStatusList: any[];
    merchantsList: any[];
    merchantFound: boolean = false;
    merchantTierList: any[];
    is422 : boolean = false;
    public headers : any;
    constructor(private http: HttpClient, private appService: AppService,  public globalVar: AuthService) {
    }

    /**
     * The File Manager App Main Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {


    }
    getSignedUploadRequest(name,ext, type) {
        this.setHeader()
        // return this.http.get(`${this.apiUrl}/companies/presign_upload?filename=${name}&filetype=${type}&contentType=jpeg`, {headers: this.headers});

        return this.http.get(`${this.appService.uploadImageService}/signed_urls.json?filename=${name}&filetype=${type}&contentType=jpg`,
        {headers: this.headers});


      }
      putFileToS3(body: File, presignedUrl: string){
        type bodyType = 'body';

        // upload file to the pre-signed url
        const httpOptions = {
          headers: new HttpHeaders({
            'Content-Type': 'image/jpeg'
              }),
         observe: <bodyType>'response'
        };
        const headers = new HttpHeaders().set(InterceptorSkipHeader, '');

        return this.http.put(presignedUrl, body, { headers });
        }

        uploadFile(url, file) {

          return this.http.put(url, file);
      }
      setHeader(){
        this.headers = new HttpHeaders({
           Authorization: this.globalVar.user.accessToken|| '',
           "Access-Control-Allow-Origin": "*"

        });
      }
    addfaculty(SmsUser): Promise<any> {
        return new Promise((resolve, reject) => {
            
            this.http.post(`${this.appService.adminService}faculties`, SmsUser, {observe : "response"})
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        }).catch(err => this.handleError(err, () =>  this.http.post(`${this.appService.adminService}faculties`, SmsUser, {observe : "response"})));
      }
      protected handleError(error, continuation: () => Observable<any>) {
          let that = this;
          //alert("Selected Class and Month Pair has no Attendance Record");
          if (error.status == 404 || error.status == 400) {
               
            return of(false);
          }
          else if (error.status == 422){
            this.is422 = true;
            debugger;
            return of(false);
          }
       
      }



}
