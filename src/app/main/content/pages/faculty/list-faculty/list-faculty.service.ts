import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of'
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { AppService } from '../../../../../app.service';

import * as moment from 'moment';
import * as _ from 'lodash';

@Injectable()
export class ListFacultyService implements Resolve<any>
{
    coupons: any[];
    classes: any[];
    couponTypes: any[];
    onFilesChanged: BehaviorSubject<any> = new BehaviorSubject({});
    onFileSelected: BehaviorSubject<any> = new BehaviorSubject({});

    constructor(private http: HttpClient, private appService: AppService) {
    }

    /**
     * The File Manager App Main Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getFaculty(),
                this.getClass()
            ]).then(
                ([faculty]) => {
                    resolve();
                },
                reject
                );
        });
    }
    protected handleError(error, continuation: () => Observable<any> ,object) {
        let that = this;
        if (error.status == 404 || error.status == 400) {
            if(object==1){
                that.coupons = [];
            }
            else if (object == 2){
                that.classes = [];
            }
          return of(false);
        }
     
    }


    deleteFaculty(faculty): Promise<any>
    {
        return new Promise((resolve, reject) => {
            // this.http.get(this.appService.apiUrl + 'cms/api/users/')
            // this.http.get('http://104.42.179.33:8765/cms/api/coupons/coupons?page=0&size=20&sort=couponCode,asc&searchTerms=gag&couponCode=123&couponType=Deal')
            this.http.patch(`${this.appService.adminService}faculties/delete_faculty`,faculty)
                .subscribe((response: any) => {
                    debugger;
                    resolve(response);
                }, reject);
        });
    }

    getFaculty(): Promise<any> {
        return new Promise((resolve, reject) => {
            // this.http.get(this.appService.apiUrl + 'cms/api/users/')
            // this.http.get('http://104.42.179.33:8765/cms/api/coupons/coupons?page=0&size=20&sort=couponCode,asc&searchTerms=gag&couponCode=123&couponType=Deal')
            this.http.get(`${this.appService.adminService}faculties/show_all_faculties`)
                .subscribe((response: any) => {
                    debugger;
                    this.coupons = response.sms_users;
                    resolve(response);
                }, reject);
        }).catch(err => this.handleError(err, () => this.http.get(`${this.appService.adminService}faculties/show_all_faculties`),1));
    }

    getClass(): Promise<any> {
        return new Promise((resolve, reject) => {
          this.http.get(`${this.appService.adminService}faculties/show_all_classes`)
                .subscribe((response: any) => {
                    debugger;
                    this.classes = response.class_sections;
                    resolve(response);
                }, reject);
        }).catch(err => this.handleError(err, () => this.http.get(`${this.appService.adminService}faculties/show_all_classes`),2));
    }



    getFilteredFaculty(
        class_section: number ,
        classteacher: boolean


    ): Promise<any> {
        debugger;
        let params = {

            class_section,
            classteacher
        };

        let queryString = '';
        _.forEach(params, (val, key) => {
            if (val || typeof val === 'number') {
                queryString = queryString + key + '=' + val + '&';
            }
        });

        return new Promise((resolve, reject) => {
            // this.http.get('api/coupon-list')
            // this.http.get(`http://104.42.179.33:8765/cms/api/coupons?page=0&size=20&sort=couponCode,asc&searchTerms=gag&couponCode=123&couponType=Deal`)
            this.http.get(`${this.appService.adminService}faculties/get_filtered_faculty?${queryString}`)
                .subscribe((response: any) => {
                    this.coupons = response.sms_users;
                    this.onFilesChanged.next(response.sms_users);
                    this.onFileSelected.next(response[0]);
                    resolve(response);
                }, reject);
        });
    }


}
