import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CdkTableModule } from '@angular/cdk/table';

import { MatAutocompleteModule, MatFormFieldModule, MatInputModule, MatSelectModule, MatStepperModule, MatButtonModule, MatIconModule, MatRippleModule, MatSidenavModule, MatSlideToggleModule, MatTableModule, MatMenuModule, MatPaginatorModule, MatSortModule } from '@angular/material';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatRadioModule} from '@angular/material/radio';
import { FuseSharedModule } from '@fuse/shared.module';

import { ListFacultyComponent } from './list-faculty.component';
import { ListFacultyService } from './list-faculty.service';
import { GridFacultyComponent } from './grid/grid-faculty.component';
import { SidenavFacultyMainComponent } from './sidenavs/main/sidenav-faculty-main.component';
import { SidenavFacultyDetailsComponent } from './sidenavs/details/sidenav-faculty-details.component';

const routes: Routes = [
    {
        path     : '**',
        component: ListFacultyComponent,
        children : [],
        resolve  : {
            files: ListFacultyService
        }
    }
];

@NgModule({
    declarations: [
        ListFacultyComponent,
        GridFacultyComponent,
        SidenavFacultyMainComponent,
        SidenavFacultyDetailsComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        CdkTableModule,
        MatAutocompleteModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatButtonModule,
        MatIconModule,
        MatRippleModule,
        MatSlideToggleModule,
        MatSidenavModule,
        MatTableModule,
        MatMenuModule,
        MatPaginatorModule, 
        MatSortModule,
        MatProgressSpinnerModule,

        FuseSharedModule,
        MatDatepickerModule,
        MatCheckboxModule,
        MatRadioModule
    ],
    providers   : [
        ListFacultyService
    ]
})
export class ListFacultyModule
{
}
