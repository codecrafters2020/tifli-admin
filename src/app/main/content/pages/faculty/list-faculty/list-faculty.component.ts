import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, FormArray, Validators, EmailValidator } from '@angular/forms';
import { fuseAnimations } from '@fuse/animations';

import { ListFacultyService } from './list-faculty.service';
import { Router } from '@angular/router';

@Component({
    selector: 'fuse-file-manager',
    templateUrl: './list-faculty.component.html',
    styleUrls: ['./list-faculty.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class ListFacultyComponent implements OnInit {
    selected: any;
    pathArr: string[];
    isSearch: boolean = false;
    form: FormGroup;
    merchantSearchResult: any[];


  // tslint:disable-next-line:no-shadowed-variable
    constructor(private ListFacultyService: ListFacultyService, private router: Router, private formBuilder: FormBuilder) {
      //  this.ListCouponService.getCoupons();
        this.form = this.formBuilder.group({
            merchantName : ['', ]});

    }

    ngOnInit() {
        this.ListFacultyService.onFileSelected.subscribe(selected => {
            this.selected = selected;
            // this.pathArr = selected.location.split('>');
        });

    }

    openCouponAdd() {
        this.router.navigateByUrl('pages/faculty/add');

    }

    resetGrid() {
        this.ListFacultyService.getFaculty();
    }

    searching() {

        if (this.isSearch == false){
            this.isSearch = true;
        }

        else{
            this.isSearch = false;
            console.log( 'close it' );
        }
    }
}
