import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { AbstractControl, FormBuilder, FormGroup, FormArray, Validators, EmailValidator } from '@angular/forms';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRadioModule } from '@angular/material/radio';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { FormControl } from '@angular/forms';
import { Observable, BehaviorSubject} from 'rxjs';
import { of } from 'rxjs/observable/of';
import { map, startWith } from 'rxjs/operators';
import { FuseAngularMaterialModule } from '../../../../../components/angular-material/angular-material.module';

import { ListFacultyService } from '../../list-faculty.service';


@Component({
    selector   : 'fuse-file-manager-main-sidenav',
    templateUrl: './sidenav-faculty-main.component.html',
    styleUrls  : ['./sidenav-faculty-main.component.scss']
})

export class SidenavFacultyMainComponent
{
    @Input() leftSideNav: any;

    form: FormGroup;
    formErrors: any;

    
    classteacher  : boolean;

    btnDisabled: boolean = false;
  ClassSectionResult: any[];
  classsection: any;

    constructor(private ListFacultyService: ListFacultyService, 
                private router: Router,
                private formBuilder: FormBuilder)
    {
      // Reactive Form
      this.form = this.formBuilder.group({
          class : ['', ],
          classteacher : ['', ]
      });

      // Reactive form errors
      this.formErrors = {
     
        class  : {},
        classteacher : {}
      };
      debugger;
      this.ClassSectionResult=this.ListFacultyService.classes;
      
      
    }

    
    filter() 
    {
      this.btnDisabled = true;
      debugger;
      this.classsection =  this.form.controls.class;
      this.classsection=  this.classsection.value.id;
      this.ListFacultyService.getFilteredFaculty(
                                                this.classsection,
                                                this.classteacher                                
                                              )
        .then((response) => {
          this.btnDisabled = false;
          this.leftSideNav.toggle()
        })
    }

    ngOnInit() 
    {
      this.form.valueChanges.subscribe(() => {
        // console.log(this.form.value);
      });
    
      // this.form.controls.merchantName.valueChanges
      //   .debounceTime(400)
      //   .subscribe(searchTerm => {
    	// 		this.ListFacultyService.searchMerchant(searchTerm)
    	// 			.subscribe(merchantNames => {
    	// 				this.merchantSearchResult = merchantNames;
    	// 			});
      //   });

      // this.form.controls.seasonalEvent.valueChanges
      //   .debounceTime(400)
      //   .subscribe(searchTerm => {
    	// 		this.ListFacultyService.searchSeasonalEvent(searchTerm)
    	// 			.subscribe(response => {
    	// 				this.seasonalEventSearchResult = response;
    	// 			});
        // });

    }
    
    displaySelectedName(item: any) 
    {
      return item.school_class.class_name+"-"+item.section.section_name;
    }
}  

    
