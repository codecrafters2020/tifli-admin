import { Component, OnInit, ViewEncapsulation, ViewChild, ElementRef } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DataSource } from '@angular/cdk/collections';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { ActivatedRoute } from '@angular/router';
import { Params } from '@angular/router';
import * as shape from 'd3-shape';
import { Router } from '@angular/router';

import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/observable/fromEvent';
import { fuseAnimations } from '@fuse/animations';
import { FuseUtils } from '@fuse/utils';

import { ProjectDashboardService } from './project.service';
import { AuthService} from '../../../../../auth/auth.service'

@Component({
    selector     : 'fuse-project-dashboard',
    templateUrl  : './project.component.html',
    styleUrls    : ['./project.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class FuseProjectDashboardComponent implements OnInit
{
  
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild('filter') filter: ElementRef;
    @ViewChild(MatSort) sort: MatSort;

    constructor(private projectDashboardService: ProjectDashboardService, private route: ActivatedRoute, private router: Router, private authService: AuthService,private formBuilder: FormBuilder)
    {
      

    }

    ngOnInit()
    {
        console.log("This is ts file of Dashboard");
        this.redirect('apps/dashboards/analytics');
        
    }
    redirect(pagename: string) {
        this.router.navigate([pagename]);
    }
   

   
}

    



