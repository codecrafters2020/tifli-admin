import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { ActivatedRoute } from '@angular/router';
import { Params } from '@angular/router';
import { AppService } from './../../../../../app.service';
import { AuthService } from '../../../../../auth/auth.service'

@Injectable()
export class ProjectDashboardService implements Resolve<any>
{
    id: any;
    user: any;
    projects: any[];
    widgets: any[];
    activityLog: any[];
    onActivityLogChanged: BehaviorSubject<any> = new BehaviorSubject({});
    onActivitySelected: BehaviorSubject<any> = new BehaviorSubject({});

    constructor(
        private http: HttpClient,
        private appService: AppService,
        private authService: AuthService
        , private router: Router
        //private route: ActivatedRoute
        //private route2: ActivatedRouteSnapshot
    ) {
      
    }

    /**
     * Resolve
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        // debugger;
        // route.params.subscribe(
        //     (params : Params) => {
        //         debugger;
        //    this.id = route.params["id"]; 
        //    }
        // );
       
    }
   

    getUser(): Promise<any> {
        debugger;
        let user = this.authService.getUser();
        if (user !== null) {
            this.id = user["id"];

            return new Promise((resolve, reject) => {
                this.http.get(this.appService.userService + 'users/' + this.id)
                    .subscribe((response: any) => {
                        console.log(response);
                        debugger;
                        if (response.meta.code == "200") {
                            this.user = response.data;
                        }
                        resolve(response);
                    }, reject);
            });
        }
    }

    getProjects(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get('api/project-dashboard-projects')
                .subscribe((response: any) => {
                    this.projects = response;
                    resolve(response);
                }, reject);
        });
    }

    getWidgets(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get('api/project-dashboard-widgets')
                .subscribe((response: any) => {
                    this.widgets = response;
                    resolve(response);
                }, reject);
        });
    }

    getActivityLog() {
        // return new Promise((resolve, reject) => {
        //     this.http.get('https://reqres.in/api/users?page=2')
        //         .subscribe((response: any) => {
        //             this.activityLog = response.data;
        //             this.onActivityLogChanged.next(response.data);
        //             this.onActivitySelected.next(response.data[0]);
        //             resolve(response.data);
        //         }, reject);
        // });
    }

    searchMerchantName(term: string): Observable<any> {
        return this.http.get(`${this.appService.merchantService}search/?name=${term}`)
            .map(response => response['data'])
            .map(data => data instanceof Array ? data : [])
    }
}
