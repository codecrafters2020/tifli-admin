import { Component, ViewEncapsulation } from '@angular/core';

import { AnalyticsDashboardService } from './analytics.service';
import { fuseAnimations } from '@fuse/animations';

@Component({
    selector     : 'fuse-analytics-dashboard',
    templateUrl  : './analytics.component.html',
    styleUrls    : ['./analytics.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class FuseAnalyticsDashboardComponent
{
    //Variables with data incomings
    events : any[];
    contacts: any[];
    notices: any[];
    attendance : any[];
    searchBox  : string;
    searchBoxContacts  : string;
    searchBox2 : string;
    searchBox3 : string;
    fees: any[];
    
    


    legend: boolean = true;
    showLabels: boolean = true;
    animations: boolean = true;
    xAxis: boolean = true;
    yAxis: boolean = true;
    showYAxisLabel: boolean = true;
    showXAxisLabel: boolean = true;
    xAxisLabel: string = 'Year';
    yAxisLabel: string = 'Population';
    multi = [
        {
          "name": "Paid",
          "series": [
            {
              "name": "June",
              "value": 62000000
            }
            ,
            {
              "name": "July",
              "value": 73000000
            },
            {
              "name": "AUG",
              "value": 89400000
            }
          ]
        },
      
        {
          "name": "Unpaid",
          "series": [
            {
              "name": "June",
              "value": 250000000
            },
            {
              "name": "July",
              "value": 309000000
            },
            {
              "name": "Aug",
              "value": 311000000
            }
          ]
        },
        {
          "name": "Defaulter",
          "series": [
            {
              "name": "June",
              "value": 57000000
            },
            {
              "name": "July",
              "value": 62000000
            },
            {
              "name": "Aug",
              "value": 72000000
            }
          ]
        }
      ];
      

    
    view: any[] = [1000, 200];
    paidStudents : any=[];
    unPaidStudents: any=[];
    defaulterStudents: any=[];
    selectedMonth  : any;
    totalPresent = 0;
    totalAbsent = 0;
    totalLeaves =0;
    total= 0;
    
    //Registration data

    registrationByMonths = [
        { "name": "January", "value":  '1' },
        { "name": "Feburary", "value":  '2' },
        { "name": "March", "value":  '3' },
        { "name": "April", "value":  '4' },
        { "name": "May", "value":  '5' },
        { "name": "June", "value":  '6' },
        { "name": "July", "value":  '7' },
        { "name": "August", "value":  '8' },
        { "name": "September", "value":  '9' },
        { "name": "October", "value":  '10' },
        { "name": "November", "value":  '11' },
        { "name": "December", "value":  '12' },
    ];

    //Attendance Card Data
  colorScheme = {
    domain: ['#0097dc', '#e91e63', '#89b8e2', '#3a5a98', '#f9a825']
  };
  //cardColor: string = '#f9a825'; 
  cardColor: string = '#232837';
  single = [];
  //Attendance Card Data

 //fees Stats
    
    feesPaidCount = 0;
    feesUnpaidCount = 0;
    feesDefaulterCount = 0;
    feesStats = [];
    widget7= {
        scheme : {
            domain: ['#e91e63', '#f9a825', '#0097dc'] 
        }
           
    }
    
  //fees Stats

  //registration STATS
    widgets: any;
    widget1SelectedYear = '2016';
    widget5SelectedDay = 'today';
    facultyCount =  0;
    studentCount=  0;
    staffCount=  0;
    registrationStats =[];
    test: any;
    month =[
        { "name":  "January", "value":  '01' }
    ];
    monthName : string = "Select Month";
  //registration STATS
    constructor(
        private analyticsDashboardService: AnalyticsDashboardService
    )
    {
        
        // Get the widgets from the service
        this.widgets = this.analyticsDashboardService.widgets;
        this.events = this.analyticsDashboardService.events;
        this.fees = this.analyticsDashboardService.fees;
        this.attendance = this.analyticsDashboardService.attendance;
        this.notices = this.analyticsDashboardService.notices;
        let that = this;
        debugger;
        this.selectedMonth = (new Date().getMonth() + 1).toLocaleString();
       // this.selectedMonth = "0".concat(this.selectedMonth);
        this.onChange();
         //fees Stats
         this.fees.forEach((element, index, array) => {
            if(element.payment_status == "paid"){
                that.feesPaidCount++;
              //  that.paidStudents.push(array[index]);
              
            }
            else if(element.payment_status == "pending"){
                that.feesUnpaidCount++;
              //  that.unPaidStudents.push(array[index]);
            }
            else{
                that.feesDefaulterCount++;
                //that.defaulterStudents.push(array[index]);
            }
            
        });
        that.feesPaidCount = (that.feesPaidCount / (that.fees.length+1)) * 100;
        that.feesPaidCount.toFixed(2);
        that.feesUnpaidCount = (that.feesUnpaidCount / (that.fees.length+1)) * 100;
        that.feesDefaulterCount = (that.feesDefaulterCount / (that.fees.length+1)) * 100;
        
        let paid = {
            name : 'Paid',
            value: that.feesPaidCount.toFixed(2),
            change : 0.5
        };
        let unpaid = {
            name : 'Unpaid',
            value: that.feesUnpaidCount.toFixed(2),
            change : -0.5
        };
        let defaulter = {
            name : 'Defaulter',
            value: that.feesDefaulterCount.toFixed(2),
            change : 0
        }        
        this.feesStats.push(paid);        
        this.feesStats.push(unpaid);
        this.feesStats.push(defaulter);
        //this is becuase it doesnt refresh sometimes 
        this.feesStats = [...this.feesStats];
        //fees Stats

        //Registration Stats
       
        
            
        


        // Register the custom chart.js plugin
        this.registerCustomChartJSPlugin();

        //Attendance Stats
        this.attendance.forEach((element, index, array) => {
           that.totalPresent = that.totalPresent + element.present;
           that.totalAbsent = that.totalAbsent + element.absent;
           that.totalLeaves = that.totalLeaves + element.leaves;
           that.total = that.total + element.total;  
            
        });
        let present = {
            name : 'Total Present',
            value: that.totalPresent,
            change : 0.5
        };
        let absent = {
            name : 'Total Absent',
            value: that.totalAbsent,
            change : -0.5
        };
        let leave = {
            name : 'Total Leave',
            value: that.totalLeaves,
           
        };
        let total = {
            name : 'Total Students',
            value: that.total
        
        }        
                
        this.single.push(present);        
        this.single.push(absent);
        this.single.push(leave);
        this.single.push(total);
        //this is becuase it doesnt refresh sometimes 
        this.single = [...this.single];
        //fees Stats

    }
    ngOnInit(){
        
      let that = this;
      that.contacts = this.analyticsDashboardService.contacts;
      that.fees.forEach((element, index, array) => {
        if(element.payment_status == "paid"){
            that.paidStudents.push(array[index]);
              
        }
        else if(element.payment_status == "pending"){
            that.unPaidStudents.push(array[index]);
            
        }
        else{
            that.defaulterStudents.push(array[index]);
        }
        
    });
     
       
    }
    onChange() {
        this.registrationStats = [...this.registrationStats];
        

            this.analyticsDashboardService.getRegistrations(this.selectedMonth).then(response => {
                this.test = response.registrations;
             
                this.month = this.registrationByMonths.filter(res =>{
                    //console.log(this.searchBox.toLocaleLowerCase());
                        return res.value.toLocaleLowerCase().match(this.selectedMonth.toLocaleLowerCase());
                          })
                    this.monthName = this.month[0].name.toLocaleString();
                  
                    this.test = this.analyticsDashboardService.registrations;
                    if(this.test != null)
                    {
                        let faculty = {
                            name : 'Faculty',
                            value: this.test[0].faculties
                        };
                        let student = {
                            name : 'Student',
                            value: this.test[0].students
                        };
                        let staff = {
                            name : 'Van Drivers',
                            value: this.test[0].van_driver
                        }        
                    
                        this.registrationStats[0]=faculty;
                        this.registrationStats[1]=student;
                        this.registrationStats[2]=staff;
                        //this is becuase it doesnt refresh sometimes 
                        this.registrationStats = [...this.registrationStats];
                      //  this.registrationStats = [...this.registrationStats];
                    }
                    else{
                        let faculty = {
                            name : 'Faculty',
                            value: 0
                        };
                        let student = {
                            name : 'Student',
                            value: 0
                        };
                        let staff = {
                            name : 'Van Drivers',
                            value: 0
                        }        
                        
                        this.registrationStats[0]=faculty;
                        this.registrationStats[1]=student;
                        this.registrationStats[2]=staff;
                        //this is becuase it doesnt refresh sometimes 
                        this.registrationStats = [...this.registrationStats];
            
                    }
                    
                
            });
            
                
    }
    SearchPaid(){
        if(this.searchBox != ""){
            this.paidStudents = this.paidStudents.filter(res =>{
                return res.student.first_name.toLocaleLowerCase().match(this.searchBox.toLocaleLowerCase());
            })
        }
        else if(this.searchBox == ""){
            this.ngOnInit();        }
    }

    SearchUnpaid(){
        if(this.searchBox2 != ""){
            this.unPaidStudents = this.unPaidStudents.filter(res =>{
                //console.log(this.searchBox.toLocaleLowerCase());
                return res.student.first_name.toLocaleLowerCase().match(this.searchBox2.toLocaleLowerCase());
            })
        }
        else if(this.searchBox2 == ""){
            this.ngOnInit();        }
    }
    SearchDefaulters(){
        if(this.searchBox3 != ""){
            this.defaulterStudents = this.defaulterStudents.filter(res =>{
                console.log(this.searchBox.toLocaleLowerCase());
                return res.student.first_name.toLocaleLowerCase().match(this.searchBox3.toLocaleLowerCase());
            })
        }
        else if(this.searchBox3 == ""){
            this.ngOnInit();        }
    }
    searchContacts(){
        if(this.searchBoxContacts != ""){
            this.contacts = this.contacts.filter(res =>{
                return res.name.toLocaleLowerCase().match(this.searchBoxContacts.toLocaleLowerCase());
            })
        }
        else if(this.searchBoxContacts == ""){
            this.ngOnInit();        }
    }
    
    /**
     * Register a custom plugin
     */
    registerCustomChartJSPlugin()
    {
        (<any>window).Chart.plugins.register({
            afterDatasetsDraw: function (chart, easing) {
                // Only activate the plugin if it's made available
                // in the options
                if (
                    !chart.options.plugins.xLabelsOnTop ||
                    (chart.options.plugins.xLabelsOnTop && chart.options.plugins.xLabelsOnTop.active === false)
                )
                {
                    return;
                }

                // To only draw at the end of animation, check for easing === 1
                const ctx = chart.ctx;

                chart.data.datasets.forEach(function (dataset, i) {
                    const meta = chart.getDatasetMeta(i);
                    if ( !meta.hidden )
                    {
                        meta.data.forEach(function (element, index) {

                            // Draw the text in black, with the specified font
                            ctx.fillStyle = 'rgba(255, 255, 255, 0.7)';
                            const fontSize = 13;
                            const fontStyle = 'normal';
                            const fontFamily = 'Roboto, Helvetica Neue, Arial';
                            ctx.font = (<any>window).Chart.helpers.fontString(fontSize, fontStyle, fontFamily);

                            // Just naively convert to string for now
                            const dataString = dataset.data[index].toString() + 'k';

                            // Make sure alignment settings are correct
                            ctx.textAlign = 'center';
                            ctx.textBaseline = 'middle';
                            const padding = 15;
                            const startY = 24;
                            const position = element.tooltipPosition();
                            ctx.fillText(dataString, position.x, startY);

                            ctx.save();

                            ctx.beginPath();
                            ctx.setLineDash([5, 3]);
                            ctx.moveTo(position.x, startY + padding);
                            ctx.lineTo(position.x, position.y - padding);
                            ctx.strokeStyle = 'rgba(255,255,255,0.12)';
                            ctx.stroke();

                            ctx.restore();
                        });
                    }
                });
            }
        });
    }
}

