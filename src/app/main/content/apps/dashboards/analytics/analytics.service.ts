import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { AppService } from '../../../../../app.service';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { AuthService } from '../../../../../auth/auth.service';
import { of } from 'rxjs/observable/of';
@Injectable()
export class AnalyticsDashboardService implements Resolve<any>
{
    id: any;
    user: any;
    widgets: any[];
   public  events: any[];
   public  fees: any[];
   public  notices: any[];
   public  contacts: any[];
   attendance: any;
  registrations: any;
    constructor(
        private http: HttpClient, private appService: AppService
        , private router: Router,
        private authService: AuthService
    )
    {
      
    }


    /**
     * Resolve
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {
      

        return new Promise((resolve, reject) => {
             
            Promise.all([
              
               this.getContacts(),
               this.getFeesStats(),
               this.getUpcommingEvents(),
               this.getRecentNotices(),
               this.getAttendance()

            ]).then(
                () => {
                  
                    resolve();
                   
                },
                reject
            );
        });
    }
   

    getRecentRegistrations(): Promise<any> {
    
      let queryString = 'month=02';
      let that = this;
      return new Promise((resolve, reject) => {
         
          this.http.get(`${this.appService.adminService}dashboard/get_recent_registrations?${queryString}`
          ,{observe: 'response'})
          .subscribe((response: any) => {
            console.log(response.status);
           
            if(response.status == 401){
              that.redirect('/pages/account/logout');
            }
            this.registrations = response.body.registrations;
            resolve(response);
          }, reject);
       
         
      }).catch(err => this.handleError(err, () => this.http.get(`${this.appService.adminService}dashboard/get_recent_registrations?${queryString}`),1));;;
      }
       redirect(pagename: string) {
        this.router.navigate([pagename]);
    }

    getFeesStats(): Promise<any> {
      let queryString = 'current_month=02';
      let that = this;
        return new Promise((resolve, reject) => {
          this.http.get(`${this.appService.adminService}dashboard/show_fees_statistics?${queryString}`)
            .subscribe((response: any) => {
              this.fees = response.student_fees;
              resolve(response);
            }, reject);
        }).catch(err => this.handleError(err, () => this.http.get(`${this.appService.adminService}dashboard/show_fees_statistics`),2));;
      }
    getUpcommingEvents(): Promise<any> {
        return new Promise((resolve, reject) => {
          this.http.get(`${this.appService.adminService}dashboard/get_upcoming_events`)
            .subscribe((response: any) => {
              this.events = response;
              debugger;
              resolve(response);
            }, reject);
        }).catch(err => this.handleError(err, () => this.http.get(`${this.appService.adminService}dashboard/get_upcoming_events`),3));;
      }
      getRecentNotices(): Promise<any> {
        return new Promise((resolve, reject) => {
          this.http.get(`${this.appService.adminService}dashboard/get_recent_notices`)
            .subscribe((response: any) => {
              this.notices = response.section_notices;
              resolve(response);
            }, reject);
        }).catch(err => this.handleError(err, () => this.http.get(`${this.appService.adminService}dashboard/get_recent_notices`),4));;
      }
      getContacts(): Promise<any> {
        return new Promise((resolve, reject) => {
          this.http.get(`${this.appService.adminService}dashboard/get_contacts`)
            .subscribe((response: any) => {
              this.contacts = response;
              resolve(response);
            }, reject);
        }).catch(err => this.handleError(err, () => this.http.get(`${this.appService.adminService}dashboard/get_contacts`),5));
      }
      getAttendance(): Promise<any> {
        return new Promise((resolve, reject) => {
          this.http.get(`${this.appService.adminService}dashboard/get_student_attendance_details`)
            .subscribe((response: any) => {
              this.attendance = response.student_attendance_details;
             
              resolve(response);
            }, reject);
        }
        
        ).catch(err => this.handleError(err, () => this.http.get(`${this.appService.adminService}dashboard/get_student_attendance_details`),6));
      }
     
      getRegistrations(month): Promise<any> {
        let queryString = 'month=' + month ;
        return new Promise((resolve, reject) => {
            
            this.http.get(`${this.appService.adminService}dashboard/get_recent_registrations?${queryString}`)
            .subscribe((response: any) => {
              this.registrations = response.registrations;
              
              resolve(response);
            }, reject);
         
           
        }).catch(err => this.handleError(err, () => this.http.get(`${this.appService.adminService}dashboard/get_recent_registrations?${queryString}`),7));
    }
    protected handleError(error, continuation: () => Observable<any> ,object) {
      if (error.status == 404 || error.status == 400) {
        if(object == 1){
          this.registrations =[];
        }
        else if(object == 2){
          this.fees = [];
        }
        else if(object == 3){
          this.events = [];
        }
        
        else if(object == 4){
          this.notices = [];
        }
        
        else if(object == 5){
            this.contacts = [];
        }
        
        else if(object == 6){
          this.attendance = [];
        }
        
        else if(object == 7){
          this.registrations =[];
        }
        
        object = [];  
        return of(false);
      }
   
  }

    
}
