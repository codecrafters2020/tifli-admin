// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=eastus2qa` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
		qa: true,
    production: false,
    hmr       : false,
    apiUrl    : 'https://api.pdn.netpace.net/',
    cdnUrl    : 'https://quotientmedia.blob.core.windows.net/mediacontainer/',
    instrumentationKey: '67000639-c41a-4f13-a370-c19b6da62726'
};
